(function ($, win, doc) {
'use strict';
$(function () {
    var $container = $('#container');

    // Search
    var $searchWrap = $('#search-form-wrap'),
        onCls = 'on',
        isSearchAnim = false,
        searchAnimTimer = 0,
        searchAnimDuration = 300;

    var stopSearchAnim = function (callback) {
        setTimeout(function () {
            isSearchAnim = false;
            callback && callback();
        }, searchAnimDuration);
    };

    $('#nav-search-btn').click(function () {
        if (isSearchAnim) return;

        isSearchAnim = true;
        $searchWrap.addClass(onCls);
        stopSearchAnim(function () {
            $('.search-form-input').focus();
        });
    });

    $('.search-form-input').blur(function () {
        clearTimeout(searchAnimTimer);
        searchAnimTimer = setTimeout(function () {
            isSearchAnim = true;
            $searchWrap.removeClass(onCls);
            stopSearchAnim();
        }, searchAnimDuration);
    });

    // Caption
    $('.article-entry').each(function (i, n) {
        $(n).find('img').each(function (i, n) {
            var $el = $(n);
            var $parent = $el.parent();
            var alt = this.alt;
            if ($parent.hasClass('fancybox')) return;

            if (alt) $(n).after('<p class="text-center"><span class="label label-default">' + alt + '</span></p>');

            $parent.attr('href') ?
                $parent.attr('title', alt).addClass('fancybox') :
                $el.wrap('<a href="' + this.src + '" title="' + alt + '" class="fancybox"></a>');
        }).find('.fancybox').each(function (i, n) {
            $(n).attr('rel', 'article' + i);
        });
    });

    if ($.fancybox) {
        $('.fancybox').fancybox();
    }

    // Mobile nav
    var isMobileNavAnim = false,
        mobileNavAnimDuration = 300,
        mobileOnCls = 'mobile-nav-on';

    var stopMobileNavAnim = function () {
        setTimeout(function () {
            isMobileNavAnim = false;
        }, mobileNavAnimDuration);
    }

    $('#main-nav-toggle').click(function (e) {
        if (isMobileNavAnim) return;

        isMobileNavAnim = true;
        $container.toggleClass(mobileOnCls);
        stopMobileNavAnim();
    });

    $('#wrap').click(function () {
        if (!isMobileNavAnim && $container.hasClass(mobileOnCls)) {
            $container.removeClass(mobileOnCls);
        }
    });

    // Return Top
    var $rocketTopBtn = $('#rocket-to-top'),
        $rocketTopAnim = $rocketTopBtn.find('.anim'),
        rocketAnimTime = 1000,
        fire = 'fire',
        shot = 'shot',
        inRocketAnim = false,
        clickedTop = false;

    // Listen to window scroll
    $(win).scroll(function () {

        // is mobile phone
        if (!$rocketTopBtn.height()) return;

        if ($(doc).scrollTop() == 0) {

            // after click and scroll to top
            if (clickedTop && !inRocketAnim) {
                inRocketAnim = true;

                // begin shot animation
                $rocketTopBtn.addClass(shot);
                setTimeout(function () {

                    // remove shot animation and hide
                    $rocketTopBtn.removeClass(shot).hide();

                    // remove fire animation
                    $rocketTopAnim.removeClass(fire);

                    inRocketAnim = false;
                    clickedTop = false;
                }, rocketAnimTime * 1.5);
            } else {
                $rocketTopBtn.fadeOut(rocketAnimTime);
            }
        } else {
            $rocketTopBtn.fadeIn(rocketAnimTime);
        }
    });
    $rocketTopAnim.click(function () {
        if (inRocketAnim) return;
        clickedTop = true;

        // begin fire animation
        $rocketTopAnim.addClass(fire);

        // animation during
        var scrollTime = $(doc).scrollTop() * 0.3;
        $('html,body').animate({scrollTop: 0}, scrollTime > rocketAnimTime ? scrollTime : rocketAnimTime);
    });
});
})(jQuery, window, document);
