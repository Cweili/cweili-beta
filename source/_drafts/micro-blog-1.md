title: "微博 @Cweili"
tags:
  - 博客
  - 微博
id: 379
categories:
  - 杂物
---

<div id="jiathis_style_32x32">
	<span class="jiathis_txt">

### 微博上关注我：
</span>

### <a class="jiathis_follow_tsina" rel="http://weibo.com/cweili"></a>新浪: [http://weibo.com/cweili](http://weibo.com/cweili "新浪微博关注Cweili")

### <a class="jiathis_follow_tqq" rel="http://t.qq.com/cweili"></a>腾讯: [http://t.qq.com/cweili](http://t.qq.com/cweili "腾讯微博关注Cweili")

### <a class="jiathis_follow_fanfou" rel="http://fanfou.com/chenweili"></a>饭否: [http://fanfou.com/chenweili](http://fanfou.com/chenweili "饭否上关注Cweili")

### <a class="jiathis_follow_digu" rel="http://digu.com/cweili"></a>嘀咕: [http://digu.com/cweili](http://digu.com/cweili "嘀咕上关注Cweili")

### <a class="jiathis_follow_tsohu" rel="http://cweili.t.sohu.com"></a>搜狐: [http://cweili.t.sohu.com](http://cweili.t.sohu.com "搜狐微博关注Cweili")

### <a class="jiathis_follow_t163" rel="http://t.163.com/cweili"></a>网易: [http://t.163.com/cweili](http://t.163.com/cweili "网易微博关注Cweili")

</div>
<!--more-->

<!--iframe id="export" name="export" src="http://cweb.sinaapp.com/index.php?m=output&amp;target=Cweili&amp;unit_name=%E5%BE%AE%E5%8D%9A%E7%A7%80&amp;width=0&amp;height=2200&amp;show_logo=0&amp;show_title=0&amp;show_border=0&amp;skin=1&amp;type=1&amp;_=1319000842" frameborder="0" scrolling="no" width="100%" height="2200">< -/iframe-->
<iframe src="http://widget.weibo.com/weiboshow/index.php?language=&amp;width=0&amp;height=800&amp;fansRow=1&amp;ptype=1&amp;speed=0&amp;skin=1&amp;isTitle=0&amp;noborder=0&amp;isWeibo=1&amp;isFans=0&amp;uid=1868261575&amp;verifier=aa335783" frameborder="0" scrolling="no" width="100%" height="800"></iframe>