title: 网页颜色表
comment: false
date: 2014-08-09 11:07:31
---

<span id="s"></span>

<style type="text/css">table{border-collapse:separate;border-spacing:0}td{width:20px;height:20px;font-size:12px;color:#777;text-align:center}</style>

<script type="text/javascript">function d(c){return'<td bgcolor="#'+c[0]+c[0]+c[1]+c[1]+c[2]+c[2]+'">'+c+"</td>"}function f(){var h="0123456789abcdef".split("");var o="<table>";var i,j;for(i=0;i<h.length;++i){o+="<tr>";for(j=0;j<h.length;++j){o+=d(h[i]+h[j]+"0")}for(j=1;j<h.length;++j){o+=d(h[i]+"f"+h[j])}for(j=1;j<h.length;++j){o+=d(h[i]+h[15-j]+"f")}for(j=1;j<h.length;++j){o+=d(h[i]+"0"+h[15-j])}o+="</tr>"}o+="</table><table>";for(i=0;i<h.length;++i){o+="<tr>";for(j=0;j<h.length;++j){o+=d(h[j]+h[i]+"0")}for(j=1;j<h.length;++j){o+=d("f"+h[i]+h[j])}for(j=1;j<h.length;++j){o+=d(h[15-j]+h[i]+"f")}for(j=1;j<h.length;++j){o+=d("0"+h[i]+h[15-j])}o+="</tr>"}o+="</table><table>";for(i=0;i<h.length;++i){o+="<tr>";for(j=0;j<h.length;++j){o+=d("0"+h[j]+h[i])}for(j=1;j<h.length;++j){o+=d(h[j]+"f"+h[i])}for(j=1;j<h.length;++j){o+=d("f"+h[15-j]+h[i])}for(j=1;j<h.length;++j){o+=d(h[15-j]+"0"+h[i])}o+="</tr>"}o+="</table>";document.getElementById("s").innerHTML=o};f();</script>
