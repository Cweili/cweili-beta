title: 我是?
id: 6
comment: false
date: 2011-10-06 07:02:25
---

![Cweili](http://ww4.sinaimg.cn/large/6f5b68c7gw1ds0jsyw6zqj.jpg "Cweili")

## Cweili是谁？

毕业于安徽工程大学计算机专业的码农。

热爱计算机、编程和网络，喜欢音乐、动漫、电影，标准吃货。

## 目前兴趣：

Node.js、AngularJS、Bootstrap

## 联系方式

Gmail / Gtalk: 3weili#AT#gmail.com <small>(中间那个#AT#要替换掉, 你懂的)</small>

## 微博上关注我：

<div class="bdshare-button-style1-24">
<a class="bds_tsina pull-left" href="http://weibo.com/cweili" title="新浪微博关注 Cweili" target="_blank"></a><a class="bds_tqq pull-left" href="http://t.qq.com/cweili" title="腾讯微博关注 Cweili" target="_blank"></a><a class="bds_ff pull-left" href="http://fanfou.com/chenweili" title="饭否关注 Cweili" target="_blank"></a><a class="bds_ pull-left" href="http://digu.com/cweili" title="嘀咕关注 Cweili" target="_blank"></a><a class="bds_tsohu pull-left" href="http://cweili.t.sohu.com" title="搜狐微博关注 Cweili" target="_blank"></a><a class="bds_t163 pull-left" href="http://t.163.com/cweili" title="网易微博关注 Cweili" target="_blank"></a>
</div>

<a id="weixin"></a>
## 微信

![Cweili](http://note.youdao.com/yws/public/resource/802718246b0094895b5275c892214bab/A6650E07FEBA42DD884968B9D5F39283 "Cweili")



<span style="font-size: 26pt;">爱</span><span style="font-size: 15.5pt;">网络 </span><span style="font-size: 26pt;">爱</span><span style="font-size: 15.5pt;">编程</span>

<span style="font-size: 20pt;">爱</span><span style="font-size: 12pt;">沉浸在</span><span style="font-size: 26pt;">音乐</span><span style="font-size: 12pt;">里</span>

<span style="font-size: 14pt;">爱</span><span style="font-size: 22pt;">嘀咕</span><span style="font-size: 21pt;"> </span><span style="font-size: 14pt;">爱</span><span style="font-size: 22pt;">折腾</span>

<span style="font-size: 14pt;">也爱静静得</span><span style="font-size: 26pt;">呆</span><span style="font-size: 14pt;">在</span><span style="font-size: 20pt;">角落</span>

<span style="font-size: 16pt;">我</span><span style="font-size: 26pt;">不</span><span style="font-size: 16pt;">是<span style="font-family: 微软雅黑;">Geek</span></span>

<span style="font-size: 12pt;">也不是</span><span style="font-size: 22pt;">Superman</span>

<span style="font-size: 20pt;">我</span><span style="font-size: 12pt; font-family: '宋体';">就是我</span>

<span style="font-size: 12pt;">我只是个</span><span style="font-size: 22pt;">小屁孩</span>

### 本博客由以下产品强力驱动：

[![基于代码托管服务打造的技术协作与分享平台](http://blog.gitcafe.com/wp-content/uploads/2014/10/Header-Image.jpg)](https://gitcafe.com/Cweili) [![基于 Node.js 的静态博客程序](https://hexo.io/logo.svg)](http://hexo.io/)

### 如对主人有任何意见~请尽情吐槽~不用客气~
