title: 让 Grunt Connect 支持 POST、PUT、DELETE 请求
date: 2014-11-12 12:28:19
tags:
  - Grunt
  - Node.js
  - JavaScript
categories:
  - 学习笔记
---
使用 `grunt-contrib-connect` 插件可以很方便得在 Grunt 任务中创建一个本地服务器，但默认情况下，只提供了 `GET` 请求的支持，`POST`、`PUT`、`DELETE` 请求则会返回以下错误：

    405 Method Not Allowed
    
我们可以通过自定义一个中间件来完成 `POST`、`PUT`、`DELETE` 请求。
<!-- more -->

首先，需要引入 `fs` 和 `path`

```js
var fs = require('fs');
var path = require('path');
```

接着，在配置中增加我们自己的中间件。

```js
connect: {
  server: {
    options: {
      middleware: function (connect, options, middlewares) {
        middlewares.unshift(function (req, res, next) {
          var filepath = path.join(options.base[0], req.url);
          if ('POSTPUTDELETE'.indexOf(req.method.toUpperCase()) > -1
            && fs.existsSync(filepath) && fs.statSync(filepath).isFile()) {
            return res.end(fs.readFileSync(filepath));
          }
          return next();
        });
        return middlewares;
      }
    }
  }
},
```
