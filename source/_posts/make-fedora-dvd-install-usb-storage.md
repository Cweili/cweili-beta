title: 制作 Fedora DVD 安装U盘 用U盘安装 Fedora
tags:
  - Fedora
  - Linux
  - 操作系统
  - 计算机
id: 819
categories:
  - 学习笔记
date: 2011-11-16 13:57:14
---

Fedora 15/16 或更高版本的DVD安装盘是无法使用 Live CD 的方法 liveusb-creator 来制作USB启动盘的, 我尝试了几次均失败了, 另外 Fedora 14 或之前的U盘引导安装DVD方法也已不适用于 Fedora 15/16 或更高版本的 Fedora DVD, 因此查阅[官方wiki](http://fedoraproject.org/wiki/How_to_create_and_use_Live_USB)以后终于尝试成功, 把新方法记录于此.

我的方法如下:<!--more-->

#### 如果你已经安装了 Fedora

1.  安装 livecd-tools
`yum install livecd-tools`
2.  插入你U盘, 开始制作, 可能这段时间会比较长
`livecd-iso-to-disk path-to/Fedora-16-x86_64-DVD.iso /dev/sdb1`
其中:
"path-to/Fedora-16-x86_64-DVD.iso" 为你的DVD镜像
"/dev/sdb1" 为你的U盘设备
需要注意的是, 你的U盘如果已经挂载了的话, 请先取消挂载.

#### 如果你使用其他系统, 可以使用虚拟机安装好 Fedora 再执行以上操作.