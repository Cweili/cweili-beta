title: 河塘飞鸟西溪湿地
tags:
  - 相册
  - 摄影
  - 浙江
  - 杭州
  - 西湖
categories:
  - 小生活
date: 2014-09-21 01:05:53
---
又是一年秋高气爽之时，为了工作之余调整一番心情，我与家人出发浙江游玩水乡乌镇西塘，泛舟西子湖上，一游西溪湿地。

游玩照片集：

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjcmr534qj20xc0p014u.jpg)](http://cweili.gitcafe.io/wuzhen/#more)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/small/6f5b68c7gw1ekjdeowobuj20xc0p0124.jpg)](http://cweili.gitcafe.io/xitang/#more)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/small/6f5b68c7gw1ekjd19iw0lj20xc0p04e5.jpg)](http://cweili.gitcafe.io/xizihu/#more)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjgof4su1j20xc0p0175.jpg)](http://cweili.gitcafe.io/xixishidi/#more)
<!--more-->

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgochsp6j20xc0p0wsb.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgochsp6j20xc0p0wsb.jpg)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgof4su1j20xc0p0175.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgof4su1j20xc0p0175.jpg)

[![河塘飞鸟西溪湿地](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjgoibu1yj20xc0p0ncj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjgoibu1yj20xc0p0ncj.jpg)

[![河塘飞鸟西溪湿地](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjgokk86sj20xc0p019l.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjgokk86sj20xc0p019l.jpg)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgon2cs5j20xc0p046j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgon2cs5j20xc0p046j.jpg)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgoralvpj20xc0p0h1f.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgoralvpj20xc0p0h1f.jpg)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgout8w2j20xc0p04ga.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgout8w2j20xc0p04ga.jpg)

[![河塘飞鸟西溪湿地](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjgoxdmrxj20xc0p0gy4.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjgoxdmrxj20xc0p0gy4.jpg)

[![河塘飞鸟西溪湿地](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjgp36uoaj20xc0p0k4j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjgp36uoaj20xc0p0k4j.jpg)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgp6x0mhj20p00xcasm.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgp6x0mhj20p00xcasm.jpg)

[![河塘飞鸟西溪湿地](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjgp9mm8fj20xc0p0wt6.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjgp9mm8fj20xc0p0wt6.jpg)

[![河塘飞鸟西溪湿地](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjgpcywuyj20xc0p0gwg.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjgpcywuyj20xc0p0gwg.jpg)

[![河塘飞鸟西溪湿地](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjgph2yrej20p00xc4lu.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjgph2yrej20p00xc4lu.jpg)

[![河塘飞鸟西溪湿地](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjgpnimmsj20xc0p0nb9.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjgpnimmsj20xc0p0nb9.jpg)

[![河塘飞鸟西溪湿地](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjgprw6wlj20xc0p0k58.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjgprw6wlj20xc0p0k58.jpg)

[![河塘飞鸟西溪湿地](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjgpu1g04j20xc0p0131.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjgpu1g04j20xc0p0131.jpg)

[![河塘飞鸟西溪湿地](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjgpxvziwj20xc0p0arh.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjgpxvziwj20xc0p0arh.jpg)

[![河塘飞鸟西溪湿地](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjgq1x4t8j20xc0p0dp3.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjgq1x4t8j20xc0p0dp3.jpg)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgq4r3ytj20xc0p0qch.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgq4r3ytj20xc0p0qch.jpg)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjgq64669j20xc0p07cm.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjgq64669j20xc0p07cm.jpg)

[![河塘飞鸟西溪湿地](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjgq8rqlaj20xc0p0gx4.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjgq8rqlaj20xc0p0gx4.jpg)
