title: C++ STL 简介
tags:
  - C++
  - STL
  - 容器
  - 程序设计
  - 算法
  - 编程
  - 计算机
id: 655
categories:
  - 学习笔记
date: 2011-10-29 13:50:14
---

<div>

### 一、<span style="font-family: Tahoma;">STL</span><span style="font-family: 宋体;">简介</span>

STL（Standard Template Library，标准模板库)是惠普实验室开发的一系列软件的统称。它是由Alexander Stepanov、Meng Lee和David R Musser在惠普实验室工作时所开发出来的。现在虽说它主要出现在C++中，但在被引入C++之前该技术就已经存在了很长的一段时间。

STL的代码从广义上讲分为三类：algorithm（算法）、container（容器）和iterator（迭代器），几乎所有的代码都采用了模板类和模版函数的方式，这相比于传统的由函数和类组成的库来说提供了更好的代码重用机会。<!--more-->在C++标准中，STL被组织为下面的13个头文件：&lt;algorithm&gt;、&lt;deque&gt;、&lt;functional&gt;、&lt;iterator&gt;、&lt;vector&gt;、&lt;list&gt;、&lt;map&gt;、&lt;memory&gt;、&lt;numeric&gt;、&lt;queue&gt;、&lt;set&gt;、&lt;stack&gt;和&lt;utility&gt;。以下笔者就简单介绍一下STL各个部分的主要特点。

### 二、算法

大家都能取得的一个共识是函数库对数据类型的选择对其可重用性起着至关重要的作用。举例来说，一个求方根的函数，在使用浮点数作为其参数类型的情况下的可重用性肯定比使用整型作为它的参数类性要高。而C++通过模板的机制允许推迟对某些类型的选择，直到真正想使用模板或者说对模板进行特化的时候，STL就利用了这一点提供了相当多的有用算法。它是在一个有效的框架中完成这些算法的——你可以将所有的类型划分为少数的几类，然后就可以在模版的参数中使用一种类型替换掉同一种类中的其他类型。

STL提供了大约100个实现算法的模版函数，比如算法for_each将为指定序列中的每一个元素调用指定的函数，stable_sort以你所指定的规则对序列进行稳定性排序等等。这样一来，只要我们熟悉了STL之后，许多代码可以被大大的化简，只需要通过调用一两个算法模板，就可以完成所需要的功能并大大地提升效率。

算法部分主要由头文件&lt;algorithm&gt;，&lt;numeric&gt;和&lt;functional&gt;组成。&lt;algorithm&gt;是所有STL头文件中最大的一个（尽管它很好理解），它是由一大堆模版函数组成的，可以认为每个函数在很大程度上都是独立的，其中常用到的功能范围涉及到比较、交换、查找、遍历操作、复制、修改、移除、反转、排序、合并等等。&lt;numeric&gt;体积很小，只包括几个在序列上面进行简单数学运算的模板函数，包括加法和乘法在序列上的一些操作。&lt;functional&gt;中则定义了一些模板类，用以声明函数对象。

### 三、容器

在实际的开发过程中，数据结构本身的重要性不会逊于操作于数据结构的算法的重要性，当程序中存在着对时间要求很高的部分时，数据结构的选择就显得更加重要。

经典的数据结构数量有限，但是我们常常重复着一些为了实现向量、链表等结构而编写的代码，这些代码都十分相似，只是为了适应不同数据的变化而在细节上有所出入。STL容器就为我们提供了这样的方便，它允许我们重复利用已有的实现构造自己的特定类型下的数据结构，通过设置一些模版类，STL容器对最常用的数据结构提供了支持，这些模板的参数允许我们指定容器中元素的数据类型，可以将我们许多重复而乏味的工作简化。

容器部分主要由头文件&lt;vector&gt;,&lt;list&gt;,&lt;deque&gt;,&lt;set&gt;,&lt;map&gt;,&lt;stack&gt;和&lt;queue&gt;组成。对于常用的一些容器和容器适配器（可以看作由其它容器实现的容器），可以通过下表总结一下它们和相应头文件的对应关系。
<table>
<tbody>
<tr>
<td valign="center" width="140">数据结构</td>
<td valign="center" width="332">描述</td>
<td valign="center" width="90">实现头文件</td>
</tr>
<tr>
<td valign="center" width="140">向量<span style="font-family: Tahoma;">(vector)</span></td>
<td valign="center" width="332">连续存储的元素</td>
<td valign="center" width="90">&lt;vector&gt;</td>
</tr>
<tr>
<td valign="center" width="140">列表<span style="font-family: Tahoma;">(list)</span></td>
<td valign="center" width="332">由节点组成的双向链表，每个结点包含着一个元素</td>
<td valign="center" width="90">&lt;list&gt;</td>
</tr>
<tr>
<td valign="center" width="140">双队列<span style="font-family: Tahoma;">(deque)</span></td>
<td valign="center" width="332">连续存储的指向不同元素的指针所组成的数组</td>
<td valign="center" width="90">&lt;deque&gt;</td>
</tr>
<tr>
<td valign="center" width="140">集合<span style="font-family: Tahoma;">(set)</span></td>
<td valign="center" width="332">由节点组成的红黑树，每个节点都包含着一个元素，节点之间以某种作用于元素对的谓词排列，没有两个不同的元素能够拥有相同的次序</td>
<td valign="center" width="90">&lt;set&gt;</td>
</tr>
<tr>
<td valign="center" width="140">多重集合<span style="font-family: Tahoma;">(multiset)</span></td>
<td valign="center" width="332">允许存在两个次序相等的元素的集合</td>
<td valign="center" width="90">&lt;set&gt;</td>
</tr>
<tr>
<td valign="center" width="140">栈<span style="font-family: Tahoma;">(stack)</span></td>
<td valign="center" width="332">后进先出的值的排列</td>
<td valign="center" width="90">&lt;stack&gt;</td>
</tr>
<tr>
<td valign="center" width="140">队列<span style="font-family: Tahoma;">(queue)</span></td>
<td valign="center" width="332">先进先出的执的排列</td>
<td valign="center" width="90">&lt;queue&gt;</td>
</tr>
<tr>
<td valign="center" width="140">优先队列<span style="font-family: Tahoma;">(priority_queue)</span></td>
<td valign="center" width="332">元素的次序是由作用于所存储的值对上的某种谓词决定的的一种队列</td>
<td valign="center" width="90">&lt;queue&gt;</td>
</tr>
<tr>
<td valign="center" width="140">映射<span style="font-family: Tahoma;">(map)</span></td>
<td valign="center" width="332">由<span style="font-family: Tahoma;">{</span><span style="font-family: 宋体;">键，值</span><span style="font-family: Tahoma;">}</span><span style="font-family: 宋体;">对组成的集合，以某种作用于键对上的谓词排列</span></td>
<td valign="center" width="90">&lt;map&gt;</td>
</tr>
<tr>
<td valign="center" width="140">多重映射<span style="font-family: Tahoma;">(multimap)</span></td>
<td valign="center" width="332">允许键对有相等的次序的映射</td>
<td valign="center" width="90">&lt;map&gt;</td>
</tr>
</tbody>
</table>

### 四、迭代器

下面要说的迭代器从作用上来说是最基本的部分，可是理解起来比前两者都要费力一些（至少笔者是这样）。软件设计有一个基本原则，所有的问题都可以通过引进一个间接层来简化，这种简化在STL中就是用迭代器来完成的。概括来说，迭代器在STL中用来将算法和容器联系起来，起着一种黏和剂的作用。几乎STL提供的所有算法都是通过迭代器存取元素序列进行工作的，每一个容器都定义了其本身所专有的迭代器，用以存取容器中的元素。

迭代器部分主要由头文件&lt;utility&gt;,&lt;iterator&gt;和&lt;memory&gt;组成。&lt;utility&gt;是一个很小的头文件，它包括了贯穿使用在STL中的几个模板的声明，&lt;iterator&gt;中提供了迭代器使用的许多方法，而对于&lt;memory&gt;的描述则十分的困难，它以不同寻常的方式为容器中的元素分配存储空间，同时也为某些算法执行期间产生的临时对象提供机制,&lt;memory&gt;中的主要部分是模板类allocator，它负责产生所有容器中的默认分配器。

### 五、对初学者学习STL的一点建议

对于之前不太了解STL的读者来说，上面的文字只是十分概括地描述了一下STL的框架，对您理解STL的机制乃至使用STL所起到的帮助微乎甚微，这不光是因为深入STL需要对C++的高级应用有比较全面的了解，更因为STL的三个部分算法、容器和迭代器三部分是互相牵制或者说是紧密结合的。从概念上讲最基础的部分是迭代器，可是直接学习迭代器会遇到许多抽象枯燥和繁琐的细节，然而不真正理解迭代器又是无法直接进入另两部分的学习的（至少对剖析源码来说是这样）。可以说，适应STL处理问题的方法是需要花费一定的时间的，但是以此为代价，STL取得了一种十分可贵的独立性，它通过迭代器能在尽可能少地知道某种数据结构的情况下完成对这一结构的运算，所以下决心钻研STL的朋友们千万不要被一时的困难击倒。其实STL运用的模式相对统一，只要适应了它，从一个STL工具到另一个工具，都不会有什么大的变化。

对于STL的使用，也普遍存在着两种观点。第一种认为STL的最大作用在于充当经典的数据结构和算法教材，因为它的源代码涉及了许多具体实现方面的问题。第二种则认为STL的初衷乃是为了简化设计，避免重复劳动，提高编程效率，因此应该是“应用至上”的，对于源代码则不必深究。笔者则认为分析源代码和应用并不矛盾，通过分析源代码也能提高我们对其应用的理解，当然根据具体的目的也可以有不同的侧重。

最后要说的是，STL是ANSI/ISO C++标准的一部分，所以对于一个可以有多种C++实现的过程，首先考虑的应该是STL提供的模板（高效且可移植性好），其次才是各个厂商各自相应的库（高效但可移植性不好）以及自己去编写代码（可移植性好但低效）。

&nbsp;

</div>