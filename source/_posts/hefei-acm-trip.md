title: 合肥ACM之旅
tags:
  - 相册
  - ACM
  - 程序设计
  - 竞赛
  - 编程
id: 61
categories:
  - 小生活
date: 2011-05-25 16:57:02
---

某年某月某一天, 偶神奇滴到合肥参加了某个比赛, 于是有了下面这些照片..

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qceyoivj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qceyoivj.jpg)
<!--more-->

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qcj302ij.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qcj302ij.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qco01y6j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qco01y6j.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qcs46llj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qcs46llj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qcwt7znj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qcwt7znj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qd191bpj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qd191bpj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qd93z7yj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qd93z7yj.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qdjujd5j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qdjujd5j.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qdnz7sxj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qdnz7sxj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qdqxnlej.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qdqxnlej.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qdy7hvwj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qdy7hvwj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qe723uaj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qe723uaj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qebb19uj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qebb19uj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qey43r9j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qey43r9j.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qf2lhm3j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qf2lhm3j.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qf6javlj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qf6javlj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qfax49xj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qfax49xj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qffht87j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qffht87j.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qg8quq6j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qg8quq6j.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qgd4i1aj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qgd4i1aj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qgioe38j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qgioe38j.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qgnwodjj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qgnwodjj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qgu2rqtj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qgu2rqtj.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qh3s9rdj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qh3s9rdj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qomqe8hj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qomqe8hj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qoq1cv8j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qoq1cv8j.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qoyewsoj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qoyewsoj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qwm6mbtj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qwm6mbtj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qwqg9clj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qwqg9clj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qwub7qpj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qwub7qpj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qwxcoodj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qwxcoodj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qx1wa25j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qx1wa25j.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qx63azkj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qx63azkj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qxbj44fj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qxbj44fj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qxfhrt1j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qxfhrt1j.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qxnkj3qj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qxnkj3qj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qye85ffj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qye85ffj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qyhe1maj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qyhe1maj.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qylhmgxj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qylhmgxj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qyosecsj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qyosecsj.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qytl9ilj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qytl9ilj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qyx6dsvj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qyx6dsvj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qz12ze9j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qz12ze9j.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qz62u1pj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qz62u1pj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qzbhzkpj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qzbhzkpj.jpg)

[![合肥ACM之旅](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3qzgeiyuj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3qzgeiyuj.jpg)

[![合肥ACM之旅](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3qzktjjdj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3qzktjjdj.jpg)

[![合肥ACM之旅](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3qzpnfzjj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3qzpnfzjj.jpg)

[![合肥ACM之旅](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3qzvcke8j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3qzvcke8j.jpg)