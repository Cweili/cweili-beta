title: 数据库实验五 实验七 SQL命令
tags:
  - SQL
  - 实验
  - 数据库
  - 计算机
id: 754
categories:
  - 学习笔记
date: 2011-10-31 23:08:11
---

数据库实验五 和 实验七 的 SQL命令记录
<!--more-->
```sql
--------------------------------------------------------------------------------
--实验五
--2.1
CREATE VIEW S00311 AS SELECT * FROM Student WHERE Clno = '00311' WITH CHECK OPTION
--命令已成功完成。

--2.2
CREATE VIEW Grades(Sno, Sname, Cname, Grade, Clno) AS SELECT s.Sno, s.Sname, c.Cname, Cj.Grade, s.Clno FROM Student s LEFT JOIN Cj ON (Cj.Sno = s.Sno) LEFT JOIN Course c On (Cj.Cno = c.Cno)
--命令已成功完成。

--2.3
CREATE VIEW C_Student AS SELECT * FROM Student WHERE Clno = '00311' AND Sno IN (SELECT Sno FROM Cj WHERE Cno = '2')
--命令已成功完成。

--3.1
SELECT * FROM S00311 WHERE Ssex = '男';
--（所影响的行数为 1 行）
--2000101, 李勇, 男, 20, 00311

--3.2
INSERT INTO S00311(Sno, Sname, Ssex, Clno) VALUES ('2001105', '刘辉', '男', '00311');
--（所影响的行数为 1 行）

--3.3
UPDATE S00311 SET Sname = '刘辉' WHERE Sno = '2001105';
--（所影响的行数为 1 行）

--3.4
DELETE FROM S00311 WHERE Sno = '2001105';
--（所影响的行数为 1 行）

--------------------------------------------------------------------------------
--实验七
--2
--创建登录user3, 密码passwd, 默认数据库Student
sp_addlogin user3, passwd, Student
go
--使用Student数据库添加用户user3
USE Student
go
sp_adduser user3
go
--已创建新登录。
--已向 'user3' 授予数据库访问权。

--3.a
GRANT SELECT ON Student TO PUBLIC;
--命令已成功完成。

--3.b
GRANT CREATE TABLE, CREATE VIEW TO user1, user2;
--命令已成功完成。
CREATE TABLE student1 (test int);
--命令已成功完成。

--3.c
GRANT CREATE TABLE TO user3;
--授权者无 GRANT 权限。

--4
REVOKE SELECT ON Student FROM PUBLIC;
--命令已成功完成。

--5
REVOKE CREATE TABLE FROM user2;
--命令已成功完成。
```