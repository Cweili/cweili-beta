title: 网络中常用传输介质及其性能对比列表
tags:
  - 计算机
  - 计算机网络
id: 856
categories:
  - 学习笔记
date: 2011-10-17 22:19:15
---

<span style="font-family: Verdana;">　　**<span style="font-size: medium;">1  有线通信</span>**</span>

<span style="font-family: Verdana;">　　有线通信介质包括架空明线，双绞线，同轴电缆，光缆等。</span>

<span style="font-family: Verdana;">　　**<span style="font-size: small;">1.1 架空明线</span>**</span>

<span style="font-family: Verdana;">　　架空明线是一种最早发展和使用的传输介质，它的通信容量较小而且很容易受外界干扰，线路损耗也大，但是设备技术简单，价格便宜，因此目前在通信线路中仍占有一定比例，早期使用的长途电话线就是架空明线。<!--more--></span>

<span style="font-family: Verdana;">　　**<span style="font-size: small;">1.2 双绞线</span>**</span>

<span style="font-family: Verdana;">　　双绞线也称为双扭线，是最古老但又最常用的传输媒体。把两根互相绝缘的铜导线并排放在一起，然后用规则的方法绞合起来（这样做是为了减少相邻的导线的电磁干扰）而构成双绞线，局域网中的双绞线是将四对双绞线封装在绝缘外套中的一种传输介质。双绞线电缆分为非屏蔽双绞线（UTP: Unshielded TwiSTed Pair）和屏蔽双绞线（STP：Shielded Twisted Pair）两大类。其中非屏蔽双绞线易弯曲、易安装，具有阻燃性，布线灵活，而屏蔽双绞线价格高，安装困难，需连结器，抗干扰性好。按传输质量双绞线分为 1类到5类，局域网中常用的为3类，4类和5类双绞线。3类线用于语音传输及最高传输速率为10Mbps的数据传输；4类线用于语音传输和最高传输速率为 16Mbps的数据传输；5类线用于语音传输和最高传输速率为100mbps的数据传输。为适应网络速度的不断提高，近来又出现了超5类和6类双绞线，其中6类双绞线可满足最新的千兆以太网的高速应用，可望在不久的将来被国际电气工业协会（EIA）采纳为国际标准。</span>

<span style="font-family: Verdana;">　　在用双绞线联起来的网络中，由于存在信号衰减，因此每网段最多不能超过100米，接4个中继器后最长可达到500米，因而也限制了它较大范围的使用。</span>

<span style="font-family: Verdana;">　　在现代家庭通信网络中，双绞线又是必不可少的一部分，在这里介绍一下双绞线及其接头的制作：</span>

<span style="font-family: Verdana;">　　由于网卡使用的是RJ-45接头方式，所以要用双绞线来进行连接，双绞线共有8根线头，如果是多台微机通过集线器进行连接，其线头按颜色进行排列为：橙白，橙，绿白，蓝，蓝白，绿，棕白，棕（如果只有两台微机，只需用网线直接连接两网卡即可，但其接线方法则有所变化：要把线头的1、3交换，2、6交换，两头依次为橙白，橙，绿白，蓝，蓝白，绿，棕白，棕，另一头是绿白，绿，橙白，蓝，蓝白，橙，棕白，棕）。然后两头分别用专用钳子把RJ-45卡子头夹好，一头插在微机的网卡上，另一头插在集线器的任意接口上。</span>

<span style="font-family: Verdana;">　　**<span style="font-size: small;">1.3 同轴电缆</span>**</span>

<span style="font-family: Verdana;">　　同轴电缆由内导体铜质芯线，绝缘层，网状编制的外导体屏蔽层及保护塑料外层组成，内导体和外导体构成一组线对。由于外导体屏蔽层的作用，同轴电缆具有很好的抗干扰性。此外，由于它比双绞线有优越的频率特性，现已被广泛用于较高速率和较高频率的数字数据传输中。</span>

<span style="font-family: Verdana;">　　通常按特性阻抗值的不同，将同轴电缆分为两大类：基带同轴电缆和宽带同轴电缆。基带同轴电缆用于传输离散的基带数字信号，用这种同轴电缆可以将10Mb/S的基带数字信号传送1千米到1.2千米，因此被广泛用于局域网中。宽带同轴电缆常用于传输模拟信号，是公用天线电视系统CATV中的标准传输电缆，利用频分多路复用技术，一条同轴电缆可以同时发送一万多个相互独立的话音信道上的信息。但在传送数字信号时，必须将数字信号用调制器转换成模拟信号，在宽带同轴电缆上传送。</span>

<span style="font-family: Verdana;">　　同轴电缆又可分为细缆和粗缆两大类，细缆使用较普遍，主要用于总线形网络布线。细缆两端装BNC头， 可连接在网卡的T形头上。细缆每段干线最大长度为185m，每段干线可接30台计算机。若要拓宽网络范围则需加中继器，最多加4个，最大传输距离可达 925m。细缆安装容易，造价低，但维护麻烦，中大型网一般不使用。粗缆每段干线最大长度为500米，每段干线可接100台计算机，最长网络范围可达到 2500米，收发器间最小2.5米，收发器电缆最长50米。</span>

<span style="font-family: Verdana;">　　同轴电缆同双绞线相比，价格较贵，但频带宽，传输数据速率高，传输距离大，抗干扰能力强，是用途广泛的传输媒体。目前普遍用于长距离的电话或电报传输，有线电视，局域网络和短距离系统连的通信线路（如主机和外设，终端的高速I/O通道的连接等）。</span>

<span style="font-family: Verdana;">　　**<span style="font-size: small;">1.4 光纤通信</span>**</span>

<span style="font-family: Verdana;">　　光纤通信就是利用光导纤维传递光脉冲来进行通信，而光导纤维是光纤通信的媒体。</span>

<span style="font-family: Verdana;">　　光导纤维（光纤）是一种能够传导光信号的极细（50μm～100μm）而柔软的介质。常用的光纤材 料有：超纯二氧化硅、多成份玻璃纤维、塑料纤维。光纤的模截面为圆形，由纤芯、包层两部分构成，二者由两种光学性能不同的介质构成。其中，纤芯为光通路； 包层由多层反射玻璃纤维构成，用来将光线反射到纤芯上。实用的光缆外部还须有一个保护层。每一纤芯及包层或紧或松地被外壳包裹着。在紧型结构中，光纤被外 层塑料壳完全包住；在松型结构中，光纤与保护壳之间有一层胶体或其他材料。无论哪一种结构，外壳都是起着提供必要的光缆强度的作用，以防止光纤受外界温 度、弯曲、外拉等影响而折断。</span>

<span style="font-family: Verdana;">　　从传输点模数来分，光纤可以分为单模和多模两种传输方式，单模提供单条光通路；多模光纤，即发散为多路光波，每一路光波走一条通路。单模光纤因为衰减小而具有更大的容量，但是它的生产要比多模光纤昂贵。</span>

<span style="font-family: Verdana;">　　光纤在任何时间都只能单向传输，因此，要实行双向通信，它必须成对出现，一个用于输入，一个用于输出，光纤两端接到光学接口上。</span>

<span style="font-family: Verdana;">　　**1.4.1 光纤与同轴电缆通信系统性能的比较**</span>

<span style="font-family: Verdana;">　　A.光纤的传输系统比同轴电缆大的多，一般小同轴电缆的最大传输带宽为20MHz左右，中同轴电缆的最大传输带宽为60MHz左右。而目前一般工程实用的梯度多模光纤和单模光纤的带宽都比同轴电缆的带宽大得多。表一为同轴电缆与目前国际上发达国家已工程实用的较先进的光纤带宽比较。</span>
<table width="553" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td rowspan="2" colspan="2" valign="top" width="189"></td>
<td rowspan="2" valign="top" width="142">色散Ps/Km•nm</td>
</tr>
</tbody>
</table>
单模光纤最大带宽

（MHz,Km）最大带宽（MHz）梯度多模光纤小同轴电缆中同轴电缆   2060工作波长（微米）0.85

1.3 1000

2000  零色散波长（微米）1.33.0(1.3微米)   17（1.55微米）1.552.5/1.55微米

<span style="font-family: Verdana;">　　从表中可知，从现在已工程实用化的光纤来说，已不但能满足目前电话、数据、文字和图像等带宽综合业 务信息的传输要求，而且还可以适应预见的将来信息业务日益发展的需求，可以这样说，光纤是目前有线通信传输介质中最良好的传输介质，一但当光缆敷设以后， 通过频分，时分和波分复用，传输容量可以管用几十年，具有良好的技术经济性能。</span>

<span style="font-family: Verdana;">　　B.光纤的传输衰耗要比同轴电缆小的多，而且光纤的传输衰耗不像同轴电缆那样随频率和温度而变。所以光纤通信不需要同轴电缆通信那样复杂的频率和温度均衡。这样，光纤通信设备就可做得比较简单。表二为国际上先进的，有代表性的光纤传输衰耗。</span>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td rowspan="2" colspan="2" valign="top" width="192"></td>
<td colspan="3" valign="top" width="369">最大传输衰耗（dB/Km）</td>
</tr>
<tr>
<td valign="top" width="132">梯度多模光纤</td>
<td valign="top" width="116">常规单模光纤</td>
<td valign="top" width="121">色散移位单模光纤</td>
</tr>
<tr>
<td valign="top" width="129">工作波长</td>
</tr>
</tbody>
</table>
（微米）

1.  85
1.3

2.2

&nbsp;

0.5  零色散波长（微米）1.3

1.  4（1.3微米）
0.25（1.55微米）

1.55  0.2

&nbsp;

<span style="font-family: Verdana;">　　光纤通信的中继间距比同轴电缆长的多，长途通信中中继器数量的减少就使系统的可靠性得到较大的提高，这对于海底光缆通信和国防长途通信具有特别重要的意义。</span>

<span style="font-family: Verdana;">　　C.光纤的抗电磁干扰能力比同轴电缆强的多，由于光纤是绝缘材料，只能导光而不能导电，所以，光纤不受电磁干扰。光纤的抗电磁干扰能力对现代通信网十分重要，既可以防止外部干扰信号的影响，又可以防止电磁波辐射而受到窃听，这样就可大大的提高现代通信网的完全性和保密性。</span>

<span style="font-family: Verdana;">　　D.光纤还有一项同轴电缆所没有的独特性能，即光纤可波分复用。目前光纤已有三个波长区（0.85微米短波长区，1.3微米和1.55微米两个长波长区）。由于上述三个波区中的每一个波长都有几百兆赫以上的带宽，所以一根光纤通过波分复用就可得到非常巨大的传输容量。</span>

<span style="font-family: Verdana;">　　从上面的这些光纤特性来看，光纤是现代有线通信最理想的传输介质。</span>

<span style="font-family: Verdana;">　　以上介绍的几种传输介质都是有线传输介质，但有线介质不可能在任何时候都满足要求。例如，当通信线路要通过某些建筑物、一座高山或一个岛屿时、施工挖掘、铺设电缆往往是费时又费钱，因而需要自由空间做通信介质，进行数据的通信。这就是下面要介绍的无线通信和卫星通信。</span>

<span style="font-family: Verdana;">　　**<span style="font-size: medium;">2  无线通信</span>**</span>

<span style="font-family: Verdana;">　　无线通信包括红外通信，激光通信和微波通信。由于它们都是沿直线传播的，有时也称它们为视线媒体， 因为这三种技术都需要在发送方面和接受方面有一条视线通路。红外通信和激光通信将要传输的信号分别转换成红外光信号和激光信号，直接在空间传播。微波的频 率范围为300MHz~300GHz，但主要是使用2~40GHz的频率范围，在自由空间主要是直线传播。</span>

<span style="font-family: Verdana;">　　下面主要介绍微波通信的特点：</span>

<span style="font-family: Verdana;">　　由于微波会穿透电离层而进入宇宙空间，因此微波通信分为两种主要方式：地面微波接力通信和卫星通信。</span>

<span style="font-family: Verdana;">　　**<span style="font-size: small;">2.1 地面微波接力通信</span>**</span>

<span style="font-family: Verdana;">　　由于微波在空间是以直线传播的，而地球表面是曲面，微波在地面的传播距离受到限制。为了增大直接传 播距离，而增大天线塔的高度，塔越高传播距离。例如一般传播距离为50km，但当天线塔为100m时，距离可增大到100km。当超过一定距离后，则需在 一个无线通信信道的两个终端之间建立若干个中继站。中继站把前一端送来的信号经过放大后再发送到下一站，故称为微波接力通信。大多数长途电话业务使用 4~6GHz的频率范围，在这些频率上越来越挤，所以其他较高的微波频率也在使用。</span>

<span style="font-family: Verdana;">　　地面微波通信主要有以下特点：</span>

<span style="font-family: Verdana;">　　波段频率高、传输频带宽、通信容量大、传输距离远、抗干扰能力强、可靠性较高，与同容量的光纤和长 度的电缆载波相比，建设投资少，见效快。但微波接力通信业也存在着如下的一些缺点：相邻站点之间必须直视，不能有障碍物。有时一个天线发射出的信号也会分 成几条略有差别的路径到达天线，因而造成失真。微波的传播有时也会受到恶劣天气的影响，与电缆通信系统比较，微波通信的隐蔽性和保密性较差，对大量中继的 使用和维护要耗费一定的人力和物力。</span>

<span style="font-family: Verdana;">　　**<span style="font-size: small;">2.2 卫星通信</span>**</span>

<span style="font-family: Verdana;">　　卫星通信实质上是在利用地球站之间利用位于36000km高空的人造同步地球卫星作为中继的一种微 波接力通信。而通信卫星就是在太空的无人职守的微波通信的中继站。采用三个适当配置的卫星，即可覆盖两极音区以外的整个地球。和微波接力通信相似，卫星通 信的频带较宽，通信容量较大，信号改变干扰小，通信比较稳定，适合于卫星通信的频段是1GHz-10GHz。目前常用的频段是：</span>

<span style="font-family: Verdana;">　　3700-4200MHz（下行，卫星--地球）</span>

<span style="font-family: Verdana;">　　5925-6425MHz（上行，地球--卫星）</span>

<span style="font-family: Verdana;">　　7250-7750MHz（下行，卫星--地球）</span>

<span style="font-family: Verdana;">　　7900-8400MHz（上行，地球--卫星）</span>

<span style="font-family: Verdana;">　　每一段的带宽都是500MHz，可同时传输几千至一万路模拟话音信号。随着业务两的不断增加，这一频段已经非常拥挤，因此现在也使用频率更高的14/12Ghz的频段。</span>

<span style="font-family: Verdana;">　　卫星通信最大的优点是：通信是“面覆盖”式的，同步卫星发射出的电磁波能辐射到地球的1/3的区 域，因而便于实现多址和移动通信，也便于组成通信网。因此广泛用于传输多路长距离电话，电报，电视业务，数据等。它的缺点是，具有较大的传播时延，无论地 面上两站的距离有多远，从发送站通过卫星载发到接收站的传播延迟时间要花270s这对于微波接力通信的3ns/km与同轴电缆的5ns/km相比，显然要 慢的多，从安全方面考虑，卫星通信系统的保密性也较差。</span>

<span style="font-family: Verdana;">　　**<span style="font-size: medium;">3 结语</span>**</span>

<span style="font-family: Verdana;">　　总之，随着时间的推移，各种传输介质将会朝着传播速度快，传输容量大，传输距离远，传输安全性高，抗干扰能力强，投资少，见效快等这些方面发展，传输介质将会为各种信息的及时传输起着重要的作用。</span>