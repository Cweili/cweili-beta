title: PPP协议 点对点协议 配置安装
tags:
  - 网络协议
  - 计算机
  - 计算机网络
id: 863
categories:
  - 学习笔记
date: 2011-10-24 22:59:17
---

**PPP：点对点协议 **（PPP：Point to Point Protocol） 　　点对点协议（PPP）为在点对点连接上传输多协议数据包提供了一个标准方法。PPP 最初设计是为两个对等节点之间的 IP 流量传输提供一种封装协议。在 TCP-IP 协议集中它是一种用来同步调制连接的数据链路层协议（OSI 模式中的第二层），替代了原来非标准的第二层协议，即 SLIP。除了 IP 以外 PPP 还可以携带其它协议，包括 DECnet 和 Novell 的 Internet 网包交换（IPX）。<!--more-->

### <a name="6_2"></a>PPP 主要由以下几部分组成

封装：一种封装多协议数据报的方法。PPP 封装提供了不同网络层协议同时在同一链路传输的多路复用技术。PPP 封装精心设计，能保持对大多数常用硬件的兼容性，克服了SLIP不足之处的一种多用途、点到点协议，它提供的WAN数据链接封装服务类似于LAN所提供的 封闭服务。所以，PPP不仅仅提供帧定界，而且提供协议标识和位级完整性检查服务。 　　链路控制协议：一种扩展链路控制协议，用于建立、配置、测试和管理数据链路连接。 　　网络控制协议：协商该链路上所传输的数据包格式与类型，建立、配置不同的网络层协议；　 　　配置：使用链路控制协议的简单和自制机制。该机制也应用于其它控制协议，例如：网络控制协议（NCP）。 　　为了建立点对点链路通信，PPP 链路的每一端，必须首先发送 LCP 包以便设定和测试数据链路。在链路建立，LCP 所需的可选功能被选定之后，PPP 必须发送 NCP 包以便选择和设定一个或更多的网络层协议。一旦每个被选择的网络层协议都被设定好了，来自每个网络层协议的数据报就能在链路上发送了。 　　链路将保持通信设定不变，直到有 LCP 和 NCP 数据包关闭链路，或者是发生一些外部事件的时候（如，休止状态的定时器期满或者网络管理员干涉）。 　　应 用：假设同样是在Windows 98，并且已经创建好“拨号连接”。那么可以通过下面的方法来设置PPP协议：首先，打开“拨号连接”属性，同样选择“服务器类型”选项卡；然后，选择默 认的“PPP：Internet，Windows NT Server，Windows 98”，在高级选项中可以设置该协议其它功能选项；最后，单击“确定”按钮即可。

### <a name="6_3"></a>PPP工作流程

当用户拨号接入 ISP 时，路由器的调制解调器对拨号做出确认，并建立一条物理连接（底层up）。

PC 机向路由器发送一系列的 LCP 分组（封装成多个 PPP 帧）。 　　这些分组及其响应选择一些 PPP 参数，和进行网络层配置（此前如有PAP或CHAP验证先要通过验证），NCP 给新接入的 PC机分配一个临时的 IP 地址，使 PC 机成为因特网上的一个主机。 　　通信完毕时，NCP 释放网络层连接，收回原来分配出去的 IP 地址。接着，LCP 释放数据链路层连接。最后释放的是物理层的连接。

### <a name="6_4"></a>PPP和HDLC之间最主要的区别

PPP是面向字节的，HDLC是面向位的。 　　③PPP在GPS应用领域代表着“精密单点定位(Precise Point Positioning)”，精密单点定位是利用国际GPS服务机构IGS提供的或自己计算的GPS精密星历和精密钟差文件，以无电离层影响的载波相位和 伪距组合观测值为观测资料，对测站的位置、接收机钟差、对流层天顶延迟以及组合后的相位模糊度等参数进行估计。用户通过一台含双频双码GPS接收机就可以 实现在数千平方公里乃至全球范围内的高精度定位。它的特点在于各站的解算相互独立，计算量远远小于一般的相对定位。

### <a name="6_5"></a>PPP的特点

PPP协议是一种点——点串行通信协议。PPP具有处理错误检测、支持多个协议、允许在连接时刻协商IP地址、允许身份认证等功能，还有其他。PPP提供了3类功能：成帧；链路控制协议LCP；网络控制协议NCP。PPP是面向字符类型的协议。

**PPP协议的帧格式**
<table>
<tbody>
<tr>
<td>标志字段</td>
<td>地址字段</td>
<td>控制字段</td>
<td>协议</td>
<td>信息部 分</td>
<td>FCS</td>
<td>标志字段</td>
</tr>
</tbody>
</table>

### <a name="6_6"></a>PPP应用范围

PPP是一种多协议成帧机制，它适合于调制解调器、HDLC位序列线路、SONET和其它的物理层上使用。它支持错误检测、选项协商、头部压缩以及使用HDLC类型帧格式（可选）的可靠传输。 　　PPP提供了三类功能： 　　1 成帧：他可以毫无歧义的分割出一帧的起始和结束。 　　2 链路控制：有一个称为LCP的链路控制协议，支持同步和异步线路，也支持面向字节的和面向位的编码方式，可用于启动路线、测试线路、协商参数、以及关闭线路。 　　3 网络控制：具有协商网络层选项的方法，并且协商方法与使用的网络层协议独立。

### <a name="6_7"></a>PPP的两种认证方式

一种是PAP，一种是CHAP。相对来说PAP的认证方式安全性没有CHAP高。PAP在传输password是明文的，而CHAP在传输过程中不传输 密码，取代密码的是hash（哈希值）。PAP认证是通过两次握手实现的，而CHAP则是通过3次握手实现的。PAP认证是被叫提出连接请求，主叫响应。 而CHAP则是主叫发出请求，被叫回复一个数据包，这个包里面有主叫发送的随机的哈希值，主叫在数据库中确认无误后发送一个连接成功的数据包连接

### <a name="6_8"></a>PPP 常见问题

**1）什么是LCP？** 　　链路控制协议(LCP) LCP 建立点对点链路，是 PPP 中实际工作的部分。LCP 位于物理层的上方，负责建立、配置和测试数据链路连接。LCP 还负责协商和设置 WAN 数据链路上的控制选项，这些选项由 NCP 处理。 　　**2）NCP是什么？**　　PPP允许多个网络协议共用一个链路，网络控制协议 (NCP) 负责连接PPP（第二层）和网络协议 (第三层)。对于所使用的每个网络层协议，PPP 都分别使用独立的 NCP来连接。例如，IP 使用 IP 控制协议 (IPCP)，IPX 使用 Novell IPX 控制协议 (IPXCP)。

### <a name="6_9"></a>PPP配置方法

PPP基本配置
1， 启用ppp
RouterTest#config terminal
Enter configuration commands, one per line. End with CNTL/Z.
RouterTest(config)#interface serial 0/0
RouterTest(config-if)#encapsulation ppp
RouterTest(config-if)#
2， 地址配置命令
RouterTest(config-if)#ip address 10.1.1.1 255.255.255.0
PAP配置实例
Router(config)#hostname RouterA
RouterA(config)#RouterB password itsasecret
RouterA(config)#interface Async 0
RouterA(config-if)#encapsulation ppp
RouterA(config-if)#ip address 10.0.0.1 255.255.255.0
RouterA(config-if)#dialer-map ip 10.0.0.2 name RouterB 5551234
RouterA(config-if)#username RouterA password itsasecret2
Router(config)#hostname RouterB
RouterB (config)#RouterA password itsasecret
RouterB (config)#interface Async 0
RouterB (config-if)#encapsulation ppp
RouterB (config-if)#ip address 10.0.0.2 255.255.255.0
RouterB (config-if)#dialer-map ip 10.0.0.1 name RouterA 5551234
RouterB (config-if)#username RouterB password itsasecret2
CHAP配置实例
Router(config)#hostname RouterA
RouterA(config)#RouterB password itsasecret
RouterA(config)#interface Async 0
RouterA(config-if)dialer in-band
RouterA(config-if)#encapsulation ppp
RouterA(config-if)#ppp authentication chap
RouterA(config-if)#ip address 10.0.0.1 255.255.255.0
RouterA(config-if)#dialer-map ip 10.0.0.2 name RouterB 5551234
RouterA(config-if)#username RouterA password itsasecret2
Router(config)#hostname RouterB
RouterB (config)#RouterA password itsasecret
RouterB (config)#interface Async 0
RouterB(config-if)dialer in-band
RouterB (config-if)#encapsulation ppp
RouterB (config-if)#ppp authentication chap
RouterB (config-if)#ip address 10.0.0.2 255.255.255.0
RouterB (config-if)#dialer-map ip 10.0.0.1 name RouterA 5551234
RouterB (config-if)#username RouterB password itsasecret2
同时启用CHAP和PAP
Router(config-if)#ppp authentication chap pap
配置PPP回拨
使用压缩
cisco支持的压缩方法：
Predictor:先判断数据是否已经被压缩过。如果数据被压缩过，则立即将其发送出去，而不浪费时间对已经压缩过的数据进行压缩。
Stacker:一种基于Lempel-Ziv(LZ)的压缩算法，对每种数据类型，只发送一次有关其在数据流中的位置。接收方根据这些信息重新组织数据流。
MPPC:MPPC协议（RFC2118）让cisco路由器器能够与microsoft客户端交换压缩后的数据，它使用一种基于LZ的压缩算法
TCP报头压缩：也叫Van Jacobson压缩，只用于压缩tcp报头。
配置压缩
Router(config)#interface serial2
Router(config-if)#compress {predictor|stac|mppc}
Or
Router(config)#interface async
Router(config-if)#ip tcp header-compression
Or
Router(config)#interface async
Router(config-if)#ip tcp header-compression passive
该命令告诉路由器，仅当从对方那里收到压缩后的报头后，才使用tcp报头压缩。
多链路PPP
通过使用多链路PPP,可以将多条连接捆绑成一条虚拟连接。
Router(config-if)#ppp multilink
Router(config-if)#dialer load-threshold load [outbound | inbound | either]
命令dialer load-threshol load指定在什么情况下将更多的B信道加入到MLP链路束中。当所有B信道的总负载超过指定的阀值后，拨号接口（BRI或PRI）将信道加入到多链路束中。
同样，如果总负载低于阀值，将拆除B信道。
参数load是接口的平均负载，其取值为1（没有负载）到255（满载）。
参数outbound（默认值）指定计算负载时只考虑出站数据流；参数inbound指定只考虑入站数据流；either指定计算负载时，选择出站负载和入站负载中较大的那个。

### <a name="6_10"></a>PPP故障排查命令

debug ppp negotiation -确定客户端是否可以通过PPP协商; 这是您检查地址协商的时候。
debug ppp authentication -确定客户端是否可以通过验证。 如果您在使用Cisco IOS软件版本11.2之前的一个版本，请发出debug ppp chap命令。
debug ppp error - 显示和PPP连接协商与操作相关的协议错误以及统计错误。
debug aaa authentication -要确定在使用哪个方法进行验证(应该是RADIUS，除非RADIUS服务器发生故障)，以及用户是否通过验证。
debug aaa authorization -要确定在使用哪个方法进行验证，并且用户是否通过验证。
debug aaa accounting -查看发送的记录。
debug radius -查看用户和服务器交换的属性。