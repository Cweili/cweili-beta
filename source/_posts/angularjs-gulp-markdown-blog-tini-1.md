title: AngularJS+Gulp开发极速静态博客系统（一）
tags:
  - AngularJS
  - Gulp
  - Markdown
  - Node.JS
  - Tini
categories:
  - 学习笔记
date: 2014-11-27 15:01:48
---
**AngularJS** 是 Google 开发的纯客户端 JavaScript 的 MVVM (Model View View-Model) WEB 框架，用于扩展、增强 HTML 功能，它专为构建强大的 WEB 应用而设计。

**Gulp** 是一种基于流的，代码优于配置的新一代构建工具。Gulp 和 Grunt 类似。但相比于 Grunt 的频繁的 IO 操作，Gulp 的流操作，能更快地完成构建。

**Markdown** 的语法简洁明了、学习容易，而且功能比纯文本更强，因此有很多人用它写博客。世界上最流行的博客平台 WordPress 和大型 CMS 如 joomla、drupal 都能很好的支持 Markdown。

现在不少同学将静态博客托管在 GitHub、GitCafe 或者七牛上，其中使用的程序大多是将文章由 Markdown 编译为静态页面，完成博客的部署。

同样，我们可以使用 Markdown 来撰写博客文章，由 Gulp 将文章编译为 HTML，并进行索引，前端完全由 AngularJS 进行路由及渲染。

这样就可以达到单页面无刷新的博客浏览体验，打造一个访问极速的博客，我把它叫做 [**Tini**](http://git.oschina.net/Cweili/tini)。
<!--more-->

接下来，我们开始一步一步打造自己的急速博客。

项目仓库：[**Tini**](http://git.oschina.net/Cweili/tini)。

## 项目结构

首先，我们需要组织我们的项目。

* `config.coffee` 博客配置
* `posts` 目录用于放置文章源文件
* `public` 目录用于放置编译完成的页面
  * `assets` 资源目录
  * `posts` 编译完成的文章
  * `index.html` 页面入口
* `src` 源代码
* `themes` 主题包目录
  * `tini` 默认的主题包
    * `assets` 资源目录
    * `styles` 样式
    * `views` 视图模板
      * `partial` 模板片段
      * `widget` 小挂件
      * `index.html` 页面入口源文件

## 项目依赖

我们的博客使用 Gulp 构建，因此，需要安装 Node.JS，之后配置我们的 `package.json`。

```json
{
  "name": "tini",
  "version": "1.0.0",
  "description": "Rapid static blog in markdown",
  "keywords": [
    "blog",
    "markdown",
    "angularjs",
    "gulp"
  ],
  "author": {
    "name": "Cweili"
  },
  "license": "MIT",
  "engines": {
    "node": ">=0.8.0"
  },
  "homepage": "http://git.oschina.net/Cweili/tini",
  "bugs": {
    "url": "http://git.oschina.net/Cweili/tini/issues"
  },
  "repository": {
    "type": "git",
    "url": "git://git.oschina.net/Cweili/tini.git"
  },
  "dependencies": {
    "coffee-script": "^1.8.0",
    "gulp": "^3.8.10",
    "gulp-autoprefixer": "^1.0.1",
    "gulp-clean": "^0.3.1",
    "gulp-coffee": "^2.2.0",
    "gulp-connect": "^2.2.0",
    "gulp-html2js": "^0.2.0",
    "gulp-less": "^1.3.6",
    "gulp-marked": "^1.0.0",
    "gulp-minify-css": "^0.3.11",
    "gulp-minify-html": "^0.1.6",
    "gulp-ng-annotate": "^0.3.4",
    "gulp-rename": "^1.2.0",
    "gulp-replace": "^0.5.0",
    "gulp-rev": "^2.0.1",
    "gulp-tap": "^0.1.3",
    "gulp-uglify": "^1.0.1",
    "gulp-usemin": "^0.3.8",
    "highlight.js": "^8.3.0",
    "js-yaml": "^3.2.3"
  }
}
```

## 博客配置

我们在配置文件 `config.coffee` 中可以添加如下配置：

```coffeescript
# Tini 配置
module.exports =

  # 博客标题
  title: 'Tini'

  # 博客子标题
  subtitle: 'A Tiny, Fast Blog'

  # 博客说明
  description: 'A Tiny, Fast Blog'

  # 首页地址，可以为一个 url
  home: '/'

  # 主题包
  theme: 'tini'

  # 每页文章数量
  postPerPage: 8

  # 侧边栏小挂件
  widgets: [

    # 分类
    {
      name: 'category'
    }

    # 最新文章
    {
      name: 'recent'
      limit: 10
    }

    # 标签云
    {
      name: 'tagcloud'
      limit: 40
    }

    # 存档
    {
      name: 'archive'
    }

    # 链接
    {
      name: 'link'
      links:
        'Cweili Beta': 'http://cweili.gitcafe.io/'
    }
  ]
```

> 现在，我们构建好了项目的结构。

> 下次，我们将开始创建 Gulp 的各个任务。
