title: 秒速5厘米
tags:
  - 动漫
  - 电影
  - 秒速5厘米
id: 684
categories:
  - 杂物
date: 2011-10-29 16:34:49
---

《秒速5厘米》是日本导演新海诚于2007年发布的动画电影。在2007年亚洲太平洋电影奖中获得最佳长篇动画电影奖。
《秒速5厘米》由三个连续短篇组成，片长63分钟。故事讲述的是远野贵树和篠原明里，小学毕业就分开继续走上属于自己的人生道路。两人刻意回避了存在于他们之间的那些特别想法，而时间就这么静静的流淌过去。某个下着大雪的冬日，贵树终于决定去找寻明里……

[![秒速5厘米](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps-cover.jpg "秒速5厘米")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps-cover.jpg)

> #### Cweili推荐理由: 《秒速5厘米》随时定格都是一副唯美的画面。它是那么唯美，在唯美中又带有一丝无奈和忧伤。因此强烈推荐!
<!--more-->
新海诚个人网站上公开的访谈中说到：《秒速5厘米》是把一个少年做为轴线来描绘的独立的三部连续短篇动画。时代从1990年代后半到现代的日本，故事发生的地点随着少年的人生的旅程而沿着东京和其它几个地方变迁。作品中没有时下大多数动画中都出现的时髦的SF元素，也鲜有跌宕起伏的情节（言下之意新海诚将抛弃《星之声》中的SF元素，以及《云之彼端，约定的地方》中扣人心弦的叙事风格）。我们只是尽量坚持实地取景，真实的表现生活。尽管缺少了波澜和戏剧性场面，但作品中同样装满了世间的各种滋味以及生活的美丽。用胶片记录下这些实实在在的场景，使影片看起来就像日常生活般贴近现实。

## 在线观看(网速给力的话可以选择 原画 高清画质或 480P 高清)

<object width="640" height="400" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="src" value="http://www.tudou.com/v/E_v2kZPu_Bk/v.swf" /><param name="allowscriptaccess" value="always" /><param name="allowfullscreen" value="true" /><param name="wmode" value="opaque" /><embed width="640" height="400" type="application/x-shockwave-flash" src="http://www.tudou.com/v/E_v2kZPu_Bk/v.swf" allowscriptaccess="always" allowfullscreen="true" wmode="opaque" /></object>

## 故事梗概

### 樱花抄（おうかしょう）（约26分钟）

[![秒速5厘米1](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps10.jpg "秒速5厘米1")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps10.jpg)

东京的小学生，远野贵树和篠原明里互相抱着特别的思念。毕业之后明里转校到栃木，虽然两人在那之后再也未见过面，但借着夏季某一天的书信而再次有了联络。那年冬天，决定转校到鹿儿岛的贵树，在某天下大雪的日子前往去和明里见面。

这是一个有关他与她之间的距离的故事——

春天，落樱缤纷，阳光明媚。她撑着一把樱花色的伞欢乐地奔跑，和他兴致勃勃地讨论是喜欢怪诞虫抑或欧巴宾海蝎。即使被同学们将两人的名字写到相思伞下，他们也坚信着他们两人会上同一个中学，在那之后也永远都会在一起。

然而片中那只也叫巧比的猫已然形单影只，那只也叫咪咪的猫没有呆在它的身边。熟悉新海诚的观众们虽然猜到了这可能暗示着什么，却不大愿意往那方面去想。

那一年，他们十岁。

[![秒速5厘米2](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps2.jpg "秒速5厘米2")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps2.jpg)

青梅竹马的美好时光自然无法永远持续，两人想要继续在一起的约定在生活的变迁面前显得那么不堪一击。东京和栃木之间的距离对少年少女们是那么的遥远，遥远得让他们不安。不安到明里在分别了半年之后才写来了第一封信。

“呐，贵树。你，还记得我吗？”

时隔一年后的相会，让少年费尽心机的去筹划，兜兜转转地搭乘自己从未乘坐的线路，花了几个星期写的，想要亲手交给她的信，见面时要说什么……只是上天似乎也要捉弄他，已算是早春的三月下起了大雪，计划中的列车一部接一部地晚点，甚至是那封包含了他所要倾诉的心意的信，也失落在风雪之中。少年甚至怀疑，是不是时间也对他抱着恶意。对此，他也只能咬紧牙关让自己不至于放声哭泣。

[![秒速5厘米3](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps8.jpg "秒速5厘米3")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps8.jpg)

眼泪始终没有忍住，不过，那是他走下晚点四个多小时的列车，看到候车室里依然等待在那里的明里的时候，两人的喜极而泣。

站在樱花树下的两人，仿佛又回到了那青梅竹马的美好时光。

“你觉不觉得，这很像是飘落的樱花？”

对再会的两人来说，相触的双唇胜过千言万语。

[![秒速5厘米4](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps3.jpg "秒速5厘米4")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps3.jpg)

“在这个瞬间。我似乎明白了‘永远’、‘心’和‘灵魂’的意义之所在，强烈的情感让我想将这十三年所经历的全部都与她分享。然后在下一个瞬间——却又悲伤得无法抑制。那是因为，我不知该如何珍藏明里的这份温暖，也不知该将她的灵魂带往何处去。我清楚地明白，我们无法保证将来能永远在一起。横亘在我们面前的是那沉重的人生与漫长的时间，让人不由得产生一种无力感。”

离开的列车上，贵树看着自己的右手，那是他刚才隔着车玻璃与明里的最后的告别

[![秒速5厘米5](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps11.jpg "秒速5厘米5")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps11.jpg)

“但是，这束缚着我的不安，最终还是慢慢地消失。剩下的，只有明里那柔软的双唇传来的触感。”

那一年，他们十三岁。

### 宇航员（COSMONAUT）（约22分钟）

鹿儿岛的高中生，澄田花苗喜欢上了中学的时候从东京转校过来的同学远野贵树，却一直无法说出口。花苗知道了贵树要前往东京的大学后心里打算将心意诉说出来——

[![秒速5厘米6](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps4.jpg "秒速5厘米6")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps4.jpg)

夏天，艳阳高照，热风扑面。

没有明里的日子依然在持续着，而他搬到了离她更远的鹿儿岛。一切似乎都没什么改变，就连那个叫花苗的女孩子也没能在他的生活中泛起多少涟漪。

只是，不知从何时开始，他就一直在做着同一个梦，写着同样没有收件人的手机短信。他所看到的不是身边的人，而是注视着遥远的她。那腾空而起的外太空探索飞行器，给他的第一印象，不是壮观，而是一种……亲切感。

“那真的是让人无法想象的孤独旅程——在那幽深的黑暗之中，只朝着一个方向一直地前进。哪怕是一个小小的氢原子都难得一见。怀着探寻世界之秘密的心，深信不疑地潜入那无尽的深渊——而我们，又将到达何方？又能去往何处呢？”

[![秒速5厘米7](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps5.jpg "秒速5厘米7")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps5.jpg)

宇航员，不知道自己会遇到什么，甚至不知道自己能不能见到所追求的东西，只是带着那一份追逐，望着前方一直走下去。

那一年，他十七岁。

### 秒速5厘米（约12分钟）

虽然远野贵树想要以高处为目标迈进，但却不明白这是因为什么冲动驱使的。

冬天，寒风呼啸，白雪飘飘。

[![秒速5厘米8](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps6.jpg "秒速5厘米8")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps6.jpg)

贵树已经从鹿儿岛的高中考上了东京的大学，毕业后留在了这个熙熙攘攘的大都市。他的目标曾经很明确，然而渐渐变得模糊，又渐渐变得迷茫。
如果说以前的他追寻的是一个比较遥远的目标，那现在的他追寻的就是一个似有若无的影子，明知道已然机会渺茫，却仍然不舍得放弃。只顾仰望着星空的人注定不会留意到自己下一步是否会跨入深渊。

“我依旧还是喜欢着你。我们即使发了一千次的短信......但心与心之间 估计只能接近一厘米。”

他终于发现，他比真正关心他的人，还不了解自己。

“在这几年里，我光顾着低头前行，只想着得到那无法得到的东西，但是又不知道那究竟是什么。而这个不知从何而来的想法逐渐地变成一种压迫，让我只能靠不停工作来解脱。等我惊觉之时，逐渐僵硬的心只能感觉到痛苦。然后在一天早上，我发现曾经那刻骨铭心的感情——已然完全失却。”

“于是在那一天，我辞去了工作。”

[![秒速5厘米9](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps1.jpg "秒速5厘米9")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps1.jpg)

又是一年的春天，依然是落樱缤纷，阳光明媚。

年年岁岁花相似。

他与她在列车道口擦肩而过。蓦然回首，列车驶过后，对面空无一人。

岁岁年年人不同。

于是他笑了，笑得很是轻松惬意。

这一年，他们二十七岁。

[![秒速5厘米10](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps9.jpg "秒速5厘米10")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps9.jpg)

### 关于《秒速五厘米》最后两分钟

在秒速五厘米间，他们发现了彼此。也在秒速五厘米之间，他们错过了彼此。

末时，他最后的一笑，不仅是如释重负，还是一种坦然：即使相见，时光不能倒流、过去亦不能再来。不如留下昔日的记忆、错过的瞬间，藏于心底，细细回味纯纯的爱。开始各人的新生活，这不更比一次颤痛的相见更来的唯美？

我个人觉得，女主角恐怕也是想等火车过完，但是她知道，即使相会，她左手无名指的戒指也会毫不留情地阐述：他们错过的已不仅仅是那秒速五厘米的距离。再说了，与其好心地安排一次重逢，不如就这样错过。ONE MORE TIME ONE MORE CHANCE,这句话也许就是两人之间等待无果的一句悲伤的缩影。更符合一种情境，归根到底是一个男孩和一个女孩相遇的故事，发生在秒速五厘米，短暂而恒久的瞬间。

[![秒速5厘米 壁纸](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps0.jpg "秒速5厘米 壁纸")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps0.jpg)

### 关于五厘米的解释

每秒5厘米似乎这个速度并不是很快，甚至可以说是很慢。人的步行速度都比它快。

可这个速度如果保持了13年呢。

通过这个公式可以计算出来。

5CM/S * 13年 * 365天 * 24小时 * 60分钟 * 60秒 = 20498.4公里.

20498.4公里。这个距离正好是绕行地球半圈的距离，也就是南极和北极的距离。

当然也许这只是个巧合，经过一些专业达人的分析，贵树和明里最后一次见面到岔道口的相遇，正好是13年。

如果这一切都不是巧合的话，只能赞叹 新海诚的大纲写的是如此的精细。

两个曾经相溶的心，经过了13年的时间，彼此达到了地球上最远的距离。

[![秒速5厘米 海报](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps.jpg "秒速5厘米 海报")](http://cweili-wordpress.stor.sinaapp.com/uploads/speed5cmps.jpg)