title: Java使用POI创建Excel图表
tags:
  - Java
  - POI
  - 编程
  - 计算机
id: 1112
categories:
  - 学习笔记
date: 2012-09-04 16:46:46
---

POI(http://poi.apache.org) 提供了Java生成MS Office文档的API, 但是POI目前无法生成Excel文档中的图表.

要使用POI生成带图表的Excel文档, 只能采用修改图表模板的方法, 通过修改Excel图表引用单元格的数据, 来改变Excel的图表.<!--more-->

这样做, 把生成图表的复杂工作, 简化成了简单的修改单元格数据.

话不多说, 其实代码很简单.

主要代码如下:

#### Excel 97-2003 格式:

```java
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * 
 * @author cweili
 * @version 2012-8-22 上午11:30:13
 * 
 */
public class Chart {

	/**
	 * 修改模板数据并保存
	 * 
	 * @author cweili
	 * 
	 * @param titles
	 *            标题行
	 * @param values
	 *            数值
	 * @param inFile
	 *            模板文件
	 * @param outFile
	 *            输出文件
	 */
	public static void createChart(String[] titles, double[] values, String inFile,
			String outFile) {
		try {
			// 读取模板
			FileInputStream is = new FileInputStream(inFile);
			HSSFWorkbook wbs = new HSSFWorkbook(is);
			// 读取工作表0
			HSSFSheet sheet0 = wbs.getSheetAt(0);
			// System.out.println(sheet0.getPhysicalNumberOfRows());
			System.out.println("行数: " + (sheet0.getLastRowNum() + 1));
			// 标题项目
			HSSFRow titleRow = sheet0.getRow(0);
			for (int i = 0; i < titleRow.getLastCellNum(); ++i) {
				HSSFCell cell = titleRow.getCell(i);
				cell.setCellValue(titles[i]);
			}
			// 数据项目
			HSSFRow row = sheet0.getRow(1);
			// System.out.println(row.getPhysicalNumberOfCells());
			System.out.println("列数: " + row.getLastCellNum());
			for (int i = 0; i < row.getLastCellNum(); ++i) {
				HSSFCell cell = row.getCell(i);
				cell.setCellValue(values[i]);
			}
			// 输出文件
			FileOutputStream os = new FileOutputStream(outFile);
			wbs.write(os);
			is.close();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
```

#### Excel 2007 格式:

```java
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 
 * @author cweili
 * @version 2012-8-22 下午1:01:13
 * 
 */
public class OOXMLChart {

	/**
	 * 修改模板数据并保存
	 * 
	 * @author cweili
	 * 
	 * @param titles
	 *            标题行
	 * @param values
	 *            数值
	 * @param inFile
	 *            模板文件
	 * @param outFile
	 *            输出文件
	 */
	public static void createChart(String[] titles, double[] values, String inFile,
			String outFile) {
		try {
			// 读取模板
			FileInputStream is = new FileInputStream(inFile);
			Workbook wb = WorkbookFactory.create(is);
			// 读取工作表0
			Sheet sheet0 = wb.getSheetAt(0);
			System.out.println("行数: " + (sheet0.getLastRowNum() + 1));
			Row titleRow = sheet0.getRow(0);
			for (int i = 0; i < titleRow.getLastCellNum(); ++i) {
				Cell cell = titleRow.getCell(i);
				cell.setCellValue(titles[i]);
			}
			// 数据项目
			Row row = sheet0.getRow(1);
			System.out.println("列数: " + row.getLastCellNum());
			for (int i = 0; i < row.getLastCellNum(); ++i) {
				Cell cell = row.getCell(i);
				cell.setCellValue(values[i]);
			}
			// 输出文件
			FileOutputStream os = new FileOutputStream(outFile);
			wb.write(os);
			is.close();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
```

模板和完整代码请见: [https://github.com/Cweili/PoiExcelChart](https://github.com/Cweili/PoiExcelChart)