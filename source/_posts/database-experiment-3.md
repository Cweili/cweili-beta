title: 数据库实验 实验三 SQL命令
tags:
  - SQL
  - 实验
  - 数据库
  - 计算机
id: 296
categories:
  - 学习笔记
date: 2011-10-17 12:46:52
---

实验三的代码记录
<!--more-->
```sql
--------------------------------------------------------------------------------
--实验三
--1

INSERT INTO Student VALUES ('2001105', '刘辉', '男', 20, '00311');
--（所影响的行数为 1 行）

--2

UPDATE Student SET Clno = '01311' WHERE Sno = '2001105';
--（所影响的行数为 1 行）

--3

DELETE FROM Student WHERE Sno = '2001105';
--（所影响的行数为 1 行）

--4

INSERT INTO Student (Sname, Ssex, Sage, Clno) VALUES ('刘辉', '男', 20, '00311');
--服务器: 消息 515，级别 16，状态 2，行 1
--无法将 NULL 值插入列 'Sno'，表 'Student.dbo.Student'；该列不允许空值。INSERT 失败。
--语句已终止。
--分析原因：Sno为主码，不允许空值
--改正：INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2001105', '刘辉', '男', 20, '00311');

INSERT INTO Student VALUES ('2001104', '刘辉', '男', 20, '00311');
--（所影响的行数为 1 行）

INSERT INTO Student VALUES ('2001105', '刘辉', '男', 13, '00311');
--服务器: 消息 547，级别 16，状态 1，行 1
--INSERT 语句与 COLUMN CHECK 约束 'CK__Student__Sage__79A81403' 冲突。该冲突发生于数据库 'Student'，表 'Student', column 'Sage'。
--语句已终止。
--分析原因：Sage限制大于14，不能插入小于14的值
--改正：INSERT INTO Student VALUES ('2001105', '刘辉', '男', 20, '00311');

INSERT INTO Student VALUES ('2001105', '刘辉', '男', 20, '00310');
--服务器: 消息 547，级别 16，状态 1，行 1
--INSERT 语句与 COLUMN FOREIGN KEY 约束 'FK__Student__Clno__07020F21' 冲突。该冲突发生于数据库 'Student'，表 'Class', column 'Clno'。
--语句已终止。
--分析原因：Class表中不存在Clno'00310'，与外码冲突。
--改正：INSERT INTO Student VALUES ('2001105', '刘辉', '男', 20, '00311');
```