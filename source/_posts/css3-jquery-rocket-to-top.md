title: CSS3动画与jQuery实现返回顶部小火箭
date: 2014-08-11 22:44:12
tags:
  - CSS
  - JavaScript
  - jQuery
  - 动画
  - 博客
categories:
  - 学习笔记
---
博客玩着玩着，总觉得不太方便，博客主题里缺少了返回顶部的按钮，没法一键返回页面顶部啊！

先去找找看有没比较好玩的实现方案，于是在其他网站看到了用火箭动画的方式返回页面顶部。

[![火箭动画](http://ww2.sinaimg.cn/mw690/9b71eb63gw1ej91qbojxlj20os06x0t2.jpg)](http://ww2.sinaimg.cn/large/9b71eb63gw1ej91qbojxlj20os06x0t2.jpg)

扒扒源代码吧~发现原先的实现完全是用jQuery动画完成，CSS3动画横行的今天，怎么说也得跟得上潮流，你说是吧。

考虑CSS3动画比jQuery动画性能好得多，一不做二不休，借来图片，代码开工自己写。
<!--more-->

首先可以看看[完成效果](http://sandbox.runjs.cn/show/cf5uf8yo)，或者你可以直接看本博客的右边...

火箭是在页面往下滚动以后，就会渐渐显现出来，点击后页面滚动到顶部，火箭继续飞出。

CSS3动画其实是将火箭动画分为两部分，**火箭点火**与**火箭飞出**。

## 火箭点火

使用CSS3的 `@keyframes` 添加关键帧，指定背景图开始变换的开始位置与结束位置。

```css
@keyframes rocket-fire {
    0% {
        background-position: -298px -18px;
    }
    100% {
        background-position: -892px -18px;
    }
}
```

并且在 `animation` 中定义 `steps(4, end) infinite`，完成4个关键帧背景的不断切换。

`steps` 方式，让关键帧的切换按步完成，没有了过渡效果，可以实现连续关键帧图片的动画效果。

`infinite` 让动画无限循环。

```css
#rocket-to-top .anim.fire {
    animation: rocket-fire 0.2s steps(4, end) infinite;
}
```

## 火箭飞出

这个就很简单了，添加一个位移 `transform: translate3d(0, -2000px, 0);` ，并指定过渡 `transition: transform 1.5s ease-in;` 即可。

```css
#rocket-to-top.shot {
    transition: transform 1.5s ease-in;
    transform: translate3d(0, -2000px, 0);
}
```

完整代码如下：

<script type="text/javascript" src="http://runjs.cn/gist/cf5uf8yo"></script>