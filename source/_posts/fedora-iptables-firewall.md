title: Fedora中Iptables防火墙的配置
tags:
  - Fedora
  - Linux
  - 计算机
  - 计算机网络
id: 926
categories:
  - 学习笔记
date: 2011-12-01 12:49:57
---

防火墙是指设置在不同网络或网络安全域之间的一系列部件的组合，它能增强机构内部网络的安全性。它通过访问控制机制，确定哪些内部服务允许外部访问，以及允许哪些外部请求可以访问内部服务。它可以根据网络传输的类型决定P包是否可以传进或传出内部网。
防火墙通过审查经过的每一个数据包，判断它是否有相匹配的过滤规则，根据规则的先后顺序进行一一比较，直到满足其中的一条规则为止，然后依据控制机制做出相应的动作。如果都不满足，则将数据包丢弃，从而保护网络的安全。<!--more-->
*********************************************************************
iptables的命令格式较为复杂，一般的格式如下：
**************************************
iptables [-t表] -命令 匹配 操作
*********************************************************************
1．表选项
***********
表选项用于指定命令应用于哪个iptables内置
表，iptables内置包括filter表、nat表和mangle表
*********************************************************************
2．命令选项
***********
命令 说明
-P或--policy &lt;链名&gt; 定义默认策略
-L或--list &lt;链名&gt; 查看iptables规则列表
-A或—append &lt;链名&gt; 在规则列表的最后增加1条规则
-I或--insert &lt;链名&gt; 在指定的位置插入1条规则
-D或--delete &lt;链名&gt; 从规则列表中删除1条规则
-R或--replace &lt;链名&gt; 替换规则列表中的某条规则
-F或--flush &lt;链名&gt; 删除表中所有规则
-Z或--zero &lt;链名&gt; 将表中数据包计数器和流量计数器归零
*********************************************************************
3．匹配选项
**********
匹配 说明
-i或--in-interface &lt;网络接口名&gt; 指定数据包从哪络接口进入，如ppp0、eth0和eth1等
-o或--out-interface &lt;网络接口名&gt; 指定数据包从哪块网络接口输出，如ppp0、eth0和eth1等
-p或---proto协议类型 &lt; 指定数据包匹配的协议，如TCP、UDP和ICMP等
-s或--source &lt;源地址或子网&gt; 指定数据包匹配的源地址
--sport &lt;源端口号&gt; 指定数据包匹配的源端口号，可以使用“起始端口号:结束端口号”的格式指定一个范围的端口
-d或--destination &lt;目标地址或子网&gt; 指定数据包匹配的目标地址
--dport目标端口号 指定数据包匹配的目标端口号，可以使用“起始端口号:结束端口号”的格式指定一个范围的端口
*********************************************************************4．动作选项
**********
动作 说明
ACCEPT 接受数据包
DROP 丢弃数据包
REDIRECT 将数据包重新转向到本机或另一台主机的某个端口，通常用功能实现透明代理或对外开放内网某些服务
SNAT 源地址转换，即改变数据包的源地址
DNAT 目标地址转换，即改变数据包的目的地址
MASQUERADE IP伪装，即是常说的NAT技术，MASQUERADE只能用于ADSL等拨号上网的IP伪装，也就是主机的IP是由ISP分配动态的；如果主机的IP地址是静态固定的，就要使用SNATLOG 日志功能，将符合规则的数据包的相关信息记录在日志中，以便管理员的分析和排错
一、 安装
[root@localhost ~]# rpm -qa iptables
iptables-1.3.8-5.fc8
二、 定义默认策略
*************
定义默认策略
**************
当数据包不符合链中任一条规则时，iptables将根据该链预先定义的
默认策略来处理数据包，默认策略的定义格式如下。
iptables [-t表名] &lt;-p&gt; &lt;链名&gt; &lt;动作&gt;
参数说明如下
 [-t表名]：指默认策略将应用于哪个表，可以使用filter、nat和mangle，如果没有指定使用哪个表，iptables就默认使用filter表。
 &lt;-p&gt;：定义默认策略。
 &lt;链名&gt;：指默认策略将应用于哪个链，可以使用INPUT、OUTPUT、FORWARD、PREROUTING、OUTPUT和POSTROUTING。
 &lt;动作&gt;：处理数据包的动作，可以使用ACCEPT（接受数据包）和DROP（丢弃数据包）。
*************************************************
例1\. 将filte表INPUT链的默认策略定义为接受数据包
[root@localhost ~]# iptables -P INPUT ACCEPT
例2\. 将nat表OUTPUT链的默认策略定义为丢弃数据包
[root@localhost ~]# iptables -t nat -P OUTPUT DROP
*********************************************************************
三、 查看iptables规则
*****************
查看iptables规则
*****************
查看iptables规则的命令格式为：
iptables [-t表名] &lt;-l&gt; [链名]
参数说明如下。
 [-t表名]：指查看哪个表的规则列表，表名用可以使用filter、nat和mangle，如果没有指定使用哪个表，iptables就默认查看filter表的规则列表。
 &lt;-l&gt;：查看指定表和指定链的规则列表。
 [链名]：指查看指定表中哪个链的规则列表，可以使用INPUT、OUTPUT、FORWARD、PREROUTING、OUTPUT和POSTROUTING，如果不指明哪个链，则将查看某个表中所有链的规则列表。
************************************************
例3\. 查看filter表所有链的规则表
[root@localhost ~]# iptables -L
Chain INPUT (policy ACCEPT)
target prot opt source destination
RH-Firewall-1-INPUT all -- anywhere anywhere
Chain FORWARD (policy ACCEPT)
target prot opt source destination
REJECT all -- anywhere anywhere reject-with icmp-host-prohibited
Chain OUTPUT (policy ACCEPT)
target prot opt source destination
Chain RH-Firewall-1-INPUT (1 references)
target prot opt source destination
ACCEPT all -- anywhere anywhere
ACCEPT icmp -- anywhere anywhere icmp any
ACCEPT esp -- anywhere anywhere
ACCEPT ah -- anywhere anywhere
ACCEPT udp -- anywhere 224.0.0.251 udp dpt:mdns
ACCEPT udp -- anywhere anywhere udp dpt:ipp
ACCEPT tcp -- anywhere anywhere tcp dpt:ipp
ACCEPT all -- anywhere anywhere state RELATED,ESTABLISHED
ACCEPT tcp -- anywhere anywhere state NEW tcp dpt:ssh
REJECT all -- anywhere anywhere reject-with icmp-host-prohibited
例4\. 查看nat表所有链的规则表
[root@localhost ~]# iptables -t nat -L
Chain PREROUTING (policy ACCEPT)
target prot opt source destination
Chain POSTROUTING (policy ACCEPT)
target prot opt source destination
Chain OUTPUT (policy ACCEPT)
target prot opt source destination
例5\. 查看mangle表所有链的规则列表
[root@localhost ~]# iptables -t mangle -L
Chain PREROUTING (policy ACCEPT)
target prot opt source destination
Chain INPUT (policy ACCEPT)
target prot opt source destination
Chain FORWARD (policy ACCEPT)
target prot opt source destination
Chain OUTPUT (policy ACCEPT)
target prot opt source destination
Chain POSTROUTING (policy ACCEPT)
target prot opt source destination
*********************************************************************
四、 增加，插入，删除和替换规则
***********************
相关规则定义的格式为：
***********************
iptables [-t表名] &lt;-a | I | D | R&gt; 链名 [规则编号] [-i | o 网卡名称] [-p 协议类型] [-s 源IP地址 | 源子网] [--sport 源端口号] [-d目标IP地址 | 目标子网] [--dport目标端口号] &lt;-j动作&gt;
参数说明如下。
 [-t表名]：定义默认策略将应用于哪个表，可以使用filter、nat和mangle，如果没有指定使用哪个表，iptables就默认使用filter表。
 -A：新增加一条规则，该规则将会增加到规则列表的最后一行，该参数不能使用规则编号。
 -I：插入一条规则，原本该位置上的规则将会往后顺序移动，如果没有指定规则编号，则在第一条规则前插入。
 -D：从规则列表中删除一条规则，可以输入完整规则，或直接指定规则编号加以删除。
 -R：替换某条规则，规则被替换并不会改变顺序，必须要指定替换的规则编号。
 &lt;链名&gt;：指定查看指定表中哪个链的规则列表，可以使用INPUT、OUTPUT、FORWARD、PREROUTING、OUTPUT和POSTROUTING。
 [规则编号]：规则编号用于插入、删除和替换规则时用，编号是按照规则列表的顺序排列，规则列表中第一条规则的编号为1。
 [-i | o 网卡名称]：i是指定数据包从哪块网卡进入，o是指定数据包从哪块网卡输出。网卡名称可以使用ppp0、eth0和eth1等。
 [-p 协议类型]：可以指定规则应用的协议，包含TCP、UDP和ICMP等。
 [-s 源IP地址 | 源子网]：源主机的IP地址或子网地址。
 [--sport 源端口号]：数据包的IP的源端口号。
 [-d目标IP地址 | 目标子网]：目标主机的IP地址或子网地址。
 [--dport目标端口号]：数据包的IP的目标端口号。
 &lt;-j动作&gt;：处理数据包的动作
*****************************************************
例6\. 为filter表的INPUT链添加一条规则，规则的内容是将来自IP地址为192.168.1.200这台主机的数据包都予以丢弃，然后查看filter表的INPUT链规则列表
[root@localhost ~]# iptables -t filter -A INPUT -s 192.168.1.200 -j DROP
[root@localhost ~]# iptables -t filter -L INPUT
Chain INPUT (policy ACCEPT)
target prot opt source destination
RH-Firewall-1-INPUT all -- anywhere anywhere
DROP all -- 192.168.1.200 anywhere
例7\. 为filter表的INPUT链添加一条规则，规则的内容是接受来自192.168.1.200这台主机的数据包，然后查看filter表的INPUT链规则列表
[root@localhost ~]# iptables -t filter -A INPUT -s 192.168.1.200 -j ACCEPT
[root@localhost ~]# iptables -t filter -L INPUT
Chain INPUT (policy ACCEPT)
target prot opt source destination
RH-Firewall-1-INPUT all -- anywhere anywhere
DROP all -- 192.168.1.200 anywhere
ACCEPT all -- 192.168.1.200 anywhere
例8\. 在filter表的INPUT链规则列表中的第2条规则前插入一条规则，规则的内容是禁止192.168.2.0这个子网里所有的主机访问TCP协议的80端口，然后查看filter表的INPUT链规则列表
[root@localhost ~]# iptables -t filter -I INPUT -s 192.168.2.0/24 -p tcp --dport 80 -j DROP
[root@localhost ~]# iptables -t filter -L INPUT
Chain INPUT (policy ACCEPT)
target prot opt source destination
DROP tcp -- 192.168.2.0/24 anywhere tcp dpt:http
RH-Firewall-1-INPUT all -- anywhere anywhere
DROP all -- 192.168.1.200 anywhere
ACCEPT all -- 192.168.1.200 anywhere
例9\. 删除filter表的INPUT链规则列表中第3条规则，然后查看filter表的INPUT链规则列表
[root@localhost ~]# iptables -t filter -D INPUT 3
[root@localhost ~]# iptables -t filter -L INPUT
Chain INPUT (policy ACCEPT)
target prot opt source destination
DROP tcp -- 192.168.2.0/24 anywhere tcp dpt:http
RH-Firewall-1-INPUT all -- anywhere anywhere
ACCEPT all -- 192.168.1.200 anywhere
例10\. 替换filter表的INPUT链规则列表中的第2条规则为禁止192.168.3.0这个子网里所有的主机访问TCP协议的80端口，然后查看filter表的INPUT链规则列表
[root@localhost ~]# iptables -t filter -R INPUT 2 -s 192.168.3.0/24 -p tcp --dport 80 -j DROP
[root@localhost ~]# iptables -t filter -L INPUT
Chain INPUT (policy ACCEPT)
target prot opt source destination
DROP tcp -- 192.168.2.0/24 anywhere tcp dpt:http
DROP tcp -- 192.168.3.0/24 anywhere tcp dpt:http
ACCEPT all -- 192.168.1.200 anywhere
*********************************************************************
五、 清除规则和计数器
****************
清除规则和计数器
****************
在新建规则时，往往需要清除原有的、旧的规则，以免它们影
响新设定的规则。如果规则比较多，一条条删除就会十分麻烦，
这时可以使用iptables提供的清除规则参数达到快速删除所有的规
则的目的。定义参数的格式为：
iptables [-t表名] &lt;-f | Z&gt;
参数说明如下。
 [-t表名]：指定默认策略将应用于哪个表，可以使用filter、nat和mangle，如果没有指定使用哪个表，iptables就默认使用filter表。
 -F：删除指定表中所有规则。
 -Z：将指定表中的数据包计数器和流量计数器归零。
*********************************************
例11\. 删除filter表中所有规则
[root@localhost ~]# iptables -F
例12\. 将filter表中数据包计数器和流量计数器规零
[root@localhost ~]# iptables -Z
例13\. 删除nat表中所有规则
[root@localhost ~]# iptables -t nat -F