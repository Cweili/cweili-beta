title: 20120310南京二日行(一)
tags:
  - 相册
  - 南京
  - 旅行
id: 964
categories:
  - 小生活
date: 2012-03-17 20:59:57
---

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsa3c2r08j.jpg)](http://cweili.gitcafe.io/nanjing-trip/ "20120310南京二日行")<!--more-->

趁着学校的最后一个小长假, 咱与同学5人一起前往南京一游.

出发前制定了大致的计划如下:

第一天
06:30 出发
07:35 K222/K223 车票20元
09:16 到达南京住布丁酒店/南京火车站-&gt;安怀村东站/南洲线/64 2元
10:30 新街口/安怀村东站-&gt;新街口/30 2元
13:30 总统府/步行 门票20元
16:30 雨花台/总统府-&gt;雨花台/游2 2元
18:30 夫子庙秦淮河/雨花台-&gt;夫子庙/游2/49 2元
18:30 夫子庙秦淮河/总统府-&gt;夫子庙/游2/304/44 2元
21:30 布丁酒店/夫子庙-&gt;安怀村东站/30 2元

第二天
06:30 退房往中山陵/安怀村东站-&gt;迈皋桥/308/30/8/72/54/76/迈皋桥-&gt;中山陵/游3 2元+门票8音乐台/28灵谷寺
11:00 明孝陵/步行 门票35元
16:00 玄武湖/明孝陵-&gt;鸡鸣寺/20 2元
19:39 K568/K569 车票20元

然而计划总赶不上变化, 网上查询到的公交线路有些已经改变, 浪费了我们不少时间...

另外两天的行程太紧凑, 结果第二天下午已经腿脚酸痛无比, 但我们仍然坚持不懈爬山越岭并用相机记录如下:

### 20120310南京二日行 照片目录:

*   [20120310南京二日行(一)](http://cweili.gitcafe.io/nanjing-trip/ "20120310南京二日行(一)")
*   [20120310南京二日行(二)](http://cweili.gitcafe.io/nanjing-trip-2/ "20120310南京二日行(二)")
*   [20120310南京二日行(三)](http://cweili.gitcafe.io/nanjing-trip-3/ "20120310南京二日行(三)")
*   [20120310南京二日行(四)](http://cweili.gitcafe.io/nanjing-trip-4/ "20120310南京二日行(四)")

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2z79ywffj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2z79ywffj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2z7hjt1gj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2z7hjt1gj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2z7om56uj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2z7om56uj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2z7txjvjj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2z7txjvjj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2z80gnbaj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2z80gnbaj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2z85tap6j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2z85tap6j.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2z8c3x6cj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2z8c3x6cj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2z8j1vayj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2z8j1vayj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2z8ostlbj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2z8ostlbj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2z8tp047j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2z8tp047j.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2z8yh765j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2z8yh765j.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2z94s9y8j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2z94s9y8j.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2z9ab2muj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2z9ab2muj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2z9iwwbrj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2z9iwwbrj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2z9oxldij.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2z9oxldij.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2z9vui9wj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2z9vui9wj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2za3wde0j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2za3wde0j.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zac11haj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zac11haj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zctzek5j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zctzek5j.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zd03yhlj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zd03yhlj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zd8sl3qj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zd8sl3qj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zdgs0wlj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zdgs0wlj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zdogiwbj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zdogiwbj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zdvfnloj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zdvfnloj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2ze30rtjj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2ze30rtjj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2ze8ms19j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2ze8ms19j.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zefuu3oj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zefuu3oj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zep7uklj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zep7uklj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zezgfb6j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zezgfb6j.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zf7sbksj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zf7sbksj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zfdavwtj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zfdavwtj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zfko4ddj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zfko4ddj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zfqaejij.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zfqaejij.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zfwpyxbj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zfwpyxbj.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zg2d23fj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zg2d23fj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zgadp7cj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zgadp7cj.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zhd6lqej.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zhd6lqej.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zhjxeaqj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zhjxeaqj.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zhqrzmgj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zhqrzmgj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zi1s5i3j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zi1s5i3j.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zib34suj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zib34suj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zikik8sj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zikik8sj.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zisbpibj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zisbpibj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zj1es7kj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zj1es7kj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zjbflidj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zjbflidj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zjjf0brj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zjjf0brj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zjrj494j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zjrj494j.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zjxhh3bj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zjxhh3bj.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zk551kcj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zk551kcj.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zkg20z3j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zkg20z3j.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zkp1u4nj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zkp1u4nj.jpg)

[![20120310南京二日行](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr2zkyu93zj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr2zkyu93zj.jpg)

[![20120310南京二日行](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr2zl6povoj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr2zl6povoj.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zlfjuk8j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zlfjuk8j.jpg)

[![20120310南京二日行](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr2zmn2mxqj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr2zmn2mxqj.jpg)

[![20120310南京二日行](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr2zmv6fd5j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr2zmv6fd5j.jpg)

### 20120310南京二日行 照片目录:

*   [20120310南京二日行(一)](http://cweili.gitcafe.io/nanjing-trip/ "20120310南京二日行(一)")
*   [20120310南京二日行(二)](http://cweili.gitcafe.io/nanjing-trip-2/ "20120310南京二日行(二)")
*   [20120310南京二日行(三)](http://cweili.gitcafe.io/nanjing-trip-3/ "20120310南京二日行(三)")
*   [20120310南京二日行(四)](http://cweili.gitcafe.io/nanjing-trip-4/ "20120310南京二日行(四)")
