title: 灯火阑珊水映乌镇
tags:
  - 相册
  - 摄影
  - 浙江
  - 乌镇
categories:
  - 小生活
date: 2014-09-21 00:11:09
---
又是一年秋高气爽之时，为了工作之余调整一番心情，我与家人出发浙江游玩水乡乌镇西塘，泛舟西子湖上，一游西溪湿地。

游玩照片集：

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjcmr534qj20xc0p014u.jpg)](http://cweili.gitcafe.io/wuzhen/#more)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/small/6f5b68c7gw1ekjdeowobuj20xc0p0124.jpg)](http://cweili.gitcafe.io/xitang/#more)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/small/6f5b68c7gw1ekjd19iw0lj20xc0p04e5.jpg)](http://cweili.gitcafe.io/xizihu/#more)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjgof4su1j20xc0p0175.jpg)](http://cweili.gitcafe.io/xixishidi/#more)
<!--more-->

[![灯火阑珊水映乌镇](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjclhdz6uj20xc0p049o.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjclhdz6uj20xc0p049o.jpg)

[![灯火阑珊水映乌镇](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjcljjtf5j20xc0p07dm.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjcljjtf5j20xc0p07dm.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcllmexqj20xc0p047b.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcllmexqj20xc0p047b.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjclocbskj20xc0p0k5s.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjclocbskj20xc0p0k5s.jpg)

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjclqh5u2j20p00xcdoa.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjclqh5u2j20p00xcdoa.jpg)

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjclsok7ej20xc0p04c7.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjclsok7ej20xc0p04c7.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjclv0lvfj20xc0p0wos.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjclv0lvfj20xc0p0wos.jpg)

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjclx8i4jj20xc0p0alk.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjclx8i4jj20xc0p0alk.jpg)

[![灯火阑珊水映乌镇](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjcm0em0vj20xc0p0tpl.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjcm0em0vj20xc0p0tpl.jpg)

[![灯火阑珊水映乌镇](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjcm3g6dsj20xc0p0tpr.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjcm3g6dsj20xc0p0tpr.jpg)

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjcm64yfkj20xc0p0h40.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjcm64yfkj20xc0p0h40.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcm9ehcdj20xc0p0h1q.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcm9ehcdj20xc0p0h1q.jpg)

[![灯火阑珊水映乌镇](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjcmbr3omj20xc0p0h1c.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjcmbr3omj20xc0p0h1c.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcmdlrg4j20xc0p0wn5.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcmdlrg4j20xc0p0wn5.jpg)

[![灯火阑珊水映乌镇](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjcmfgignj20xc0p0thi.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjcmfgignj20xc0p0thi.jpg)

[![灯火阑珊水映乌镇](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjcmhek8wj20xc0p012b.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjcmhek8wj20xc0p012b.jpg)

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjcmjnlpij20xc0p0won.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjcmjnlpij20xc0p0won.jpg)

[![灯火阑珊水映乌镇](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjcmmdglpj20xc0p04dc.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjcmmdglpj20xc0p04dc.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcmpcnu8j20xc0p0dvz.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcmpcnu8j20xc0p0dvz.jpg)

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjcmr534qj20xc0p014u.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjcmr534qj20xc0p014u.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcmt9yq6j20xc0p0144.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcmt9yq6j20xc0p0144.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcmvoogvj20xc0p0tkg.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcmvoogvj20xc0p0tkg.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcmy1uqoj20xc0p0wpp.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcmy1uqoj20xc0p0wpp.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcn0nyq1j20xc0p0gwu.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcn0nyq1j20xc0p0gwu.jpg)

[![灯火阑珊水映乌镇](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjcn3smd8j20p00xcqk6.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjcn3smd8j20p00xcqk6.jpg)

[![灯火阑珊水映乌镇](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjcn512zjj20xc0p07af.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjcn512zjj20xc0p07af.jpg)

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjcn6pb5dj20xc0p0tgq.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjcn6pb5dj20xc0p0tgq.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcn8y2toj20xc0p0k20.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcn8y2toj20xc0p0k20.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcnablwwj20xc0p0q9u.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcnablwwj20xc0p0q9u.jpg)

[![灯火阑珊水映乌镇](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjcnbn9fuj20xc0p0443.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjcnbn9fuj20xc0p0443.jpg)

[![灯火阑珊水映乌镇](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjcne7dhjj20xc0p0qci.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjcne7dhjj20xc0p0qci.jpg)

[![灯火阑珊水映乌镇](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjcng6q5cj20xc0p0wmz.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjcng6q5cj20xc0p0wmz.jpg)

[![灯火阑珊水映乌镇](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjcnhr3d1j20xc0p0ahm.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjcnhr3d1j20xc0p0ahm.jpg)
