title: 数据库实验的SQL语句
tags:
  - SQL
  - 实验
  - 数据库
  - 计算机
id: 159
categories:
  - 学习笔记
date: 2011-10-10 23:36:44
---

这是第一次的实验的最终结果, 就记录在这, 我还不会写触发器, 所以没法级联插入数据, 只好先插入数据再添加外码级联...
中间的注释是题号和执行后的提示...
第一题的命令即P52页实验样例数据表的结构和数据..以后实验只需执行即可...
应该还是有很多错误的...如果有错误欢迎大家提出来~俺会不断修正的~~
<!--more-->
```sql
--1
--命令已成功完成。

CREATE TABLE Student (
	Sno char(7) PRIMARY KEY,
	Sname char(10) NOT NULL,
	Ssex char(2) NOT NULL DEFAULT '男' CHECK (Ssex IN ('男', '女')),
	Sage smallint CHECK (Sage BETWEEN 14 AND 65),
	Clno char(5) NOT NULL
);

CREATE TABLE Course (
	Cno char(1) PRIMARY KEY,
	Cname char(20) UNIQUE,
	Credit smallint CHECK (Credit IN (1, 2, 3, 4, 5, 6))
);

CREATE TABLE Class (
	Clno char(5) PRIMARY KEY,
	Speciality char(20) NOT NULL,
	Inyear datetime NOT NULL,
	Number smallint CHECK (Number BETWEEN 1 AND 60),
	Monitor char(7),
	FOREIGN KEY (Monitor) REFERENCES Student(Sno)
);

CREATE TABLE Cj (
	Sno char(7) NOT NULL,
	Cno char(1) NOT NULL,
	Grade decimal(4,1) CHECK (Grade BETWEEN 1 AND 100),
	PRIMARY KEY (Sno, Cno),
	FOREIGN KEY (Sno) REFERENCES Student(Sno) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (Cno) REFERENCES Course(Cno) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO Course (Cno, Cname, Credit) VALUES ('1', '数据库', 4);
INSERT INTO Course (Cno, Cname, Credit) VALUES ('2', '离散数学', 3);
INSERT INTO Course (Cno, Cname, Credit) VALUES ('3', '管理信息系统', 2);
INSERT INTO Course (Cno, Cname, Credit) VALUES ('4', '操作系统', 4);
INSERT INTO Course (Cno, Cname, Credit) VALUES ('5', '数据结构', 4);
INSERT INTO Course (Cno, Cname, Credit) VALUES ('6', '数据处理', 2);
INSERT INTO Course (Cno, Cname, Credit) VALUES ('7', 'C语言', 4);

INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2000101', '李勇', '男', 20, '00311');
INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2000102', '刘诗晨', '女', 19, '00311');
INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2000103', '王一鸣', '男', 20, '00312');
INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2000104', '张婷婷', '女', 21, '00312');
INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2001101', '李勇敏', '女', 19, '01311');
INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2001102', '贾向东', '男', 22, '01311');
INSERT INTO Student (Sno, Sname, Ssex, Sage, Clno) VALUES ('2001103', '陈宝玉', '男', 20, '01311');

INSERT INTO Class (Clno, Speciality, Inyear, Number, Monitor) VALUES ('00311', '计算机软件', '2000-1-1', 45, '2000101');
INSERT INTO Class (Clno, Speciality, Inyear, Number, Monitor) VALUES ('00312', '计算机应用', '2000-1-1', 42, '2000103');
INSERT INTO Class (Clno, Speciality, Inyear, Number, Monitor) VALUES ('01311', '计算机软件', '2001-1-1', 39, '2001103');

INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2000101', '1', 92);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2000101', '3', 88);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2000101', '5', 86);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2000102', '1', 78);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2000102', '6', 55);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2001101', '2', 70);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2001101', '4', 65);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2001102', '2', 80);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2001102', '4', 90);
INSERT INTO Cj (Sno, Cno, Grade) VALUES ('2001102', '6', 83);

ALTER TABLE Student ADD FOREIGN KEY (Clno) REFERENCES Class(Clno) ON DELETE CASCADE ON UPDATE CASCADE;
```
最后由 Cweili 编辑于 2011 年 10 月 17 日下午 12:15
```sql
--2
--命令已成功完成。

ALTER TABLE Student ADD Class char(5);
```
```sql
--3
--（所影响的行数为 0 行）

ALTER TABLE Student ALTER COLUMN Class char(10);

ALTER TABLE Student DROP COLUMN Class;

ALTER TABLE Student DROP COLUMN Birthday;
```
```sql
--4
--命令已成功完成。

CREATE UNIQUE INDEX UQ_Cno ON Course(Cno ASC);

CREATE UNIQUE INDEX UQ_Sno ON Cj(Sno ASC, Cno DESC);
```
```sql
--5
--*对于 DROP INDEX，必须以 tablename.indexname 的形式同时给出表名和索引名。
--命令已成功完成。

DROP INDEX Course.UQ_Cno;

DROP INDEX Cj.UQ_Sno;
```
```sql
--6
--*未能除去对象 'Course'，因为该对象正由一个 FOREIGN KEY 约束引用。
--命令已成功完成。

ALTER TABLE Cj DROP CONSTRAINT FK__Cj__Cno__0425A276;

DROP TABLE Course;
```