title: Fedora 15/16 安装 VirtualBox-OSE 出现找不到 VBoxXPCOMBase.xpt 的解决办法
tags:
  - Fedora
  - Linux
  - VirtualBox
  - 操作系统
  - 计算机
id: 869
categories:
  - 学习笔记
date: 2011-12-03 22:20:33
---

在 Fedora 15/16 安装 VirtualBox-OSE 后启动时会报如下错误：

32位：

```
$ virtualbox
VirtualBox: supR3HardenedVerifyFileInternal: Failed to open "/usr/lib/virtualbox/components/VBoxXPCOMBase.xpt": No such file or directory
```

64位：

```
$ virtualbox
VirtualBox: supR3HardenedVerifyFileInternal: Failed to open "/usr/lib64/virtualbox/components/VBoxXPCOMBase.xpt": No such file or directory
```
<!--more-->

在 bugzilla.rpmfusion.org 上已经有人提交此问题并得到确认。

> &lt;pre&gt;Add missing *.xpt files installation in VirtualBox-OSE.spec
> 
> When trying to launch VirtualBox, I get the following message error:
>    $ virtualbox
>    VirtualBox: supR3HardenedVerifyFileInternal: Failed to open
> "/usr/lib64/virtualbox/components/VBoxXPCOMBase.xpt": No such file or directory
> (2)
> 
> It seems xpt files are missing from the package:
>    $ rpm -ql VirtualBox-OSE | grep xpt
>    (nothing)
> 
> Version of VirtualBox-OSE installed:
>    VirtualBox-OSE-4.1.2-1.fc16.x86_64
> 
> The attached patch fixes this issue.&lt;/pre&gt;

目前已有临时解决办法：

请尝试以下命令：

```
yum --enablerepo=rpmfusion-free-updates-testing update VirtualBox*
```

或

```
yum install --enablerepo=rpmfusion-free-updates-testing akmod-VirtualBox-OSE
```

或

```
yum --enablerepo=rpmfusion-free-updates-testing update VirtualBox\*
```

跟踪此Bug详情或动态请查看： https://bugzilla.rpmfusion.org/show_bug.cgi?id=1979
