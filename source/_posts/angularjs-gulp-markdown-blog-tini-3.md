title: AngularJS+Gulp开发极速静态博客系统（三）
tags:
  - AngularJS
  - Gulp
  - Markdown
  - Node.JS
  - Tini
categories:
  - 学习笔记
date: 2014-11-29 14:07:05
---
## 创建 AngularJS ngApp

我们依赖于 `angular.min.js` 及 `angular-route.min.js`，在 `index.html` 中添加。

新增 `src/app.coffee` 并在 `index.html` 中引入。

### 配置 ngApp

增加 `ngRoute` 的依赖，并且对 `$routeProvider` 进行配置。

将对首页、分类、标签和存档的访问路由到相应控制器，指定模板为 `partial/posts.html`。

将对博客文章的访问路由到文章控制器，指定模板为 `partial/post.html`。

配置 `$httpProvider.defaults.cache = true` 以默认开启对 http 请求的缓存。
<!--more-->

配置如下：

```coffeescript
angular.module 'tini', [
  'ngRoute'
]
.config ($routeProvider, $httpProvider) ->

  #　对存档类页面的路由
  tplPostList = 'partial/posts.html'
  for type in [
    'category'
    'tag'
    'archive'
  ]
    $routeProvider
    .when "/#{type}/:#{type}",
      controller: type
      templateUrl: tplPostList
    .when "/#{type}/:#{type}/page/:page",
      controller: type
      templateUrl: tplPostList

  $routeProvider

  # 首页
  .when '/',
    controller: 'home'
    templateUrl: tplPostList

  # 首页分页
  .when '/page/:page',
    controller: 'home'
    templateUrl: tplPostList

  # 文章
  .otherwise
      controller: 'post'
      templateUrl: 'partial/post.html'

  # 默认开启 http 请求缓存
  $httpProvider.defaults.cache = true
```

### 请求服务

我们需要封装一个自己的请求服务，方便对索引和博客内容进行获取。

代码如下：

```coffeescript
angular.module 'tini'

  .factory 'req', ($rootScope, $http) ->
    broadcast = (msg) ->
      $rootScope.$broadcast msg

    # 增加查询串以提供对缓存的更新
    hash = '?' + (new Date).getTime().toString(36).substr 0, 3

    hash: hash

    # 请求文章索引
    index: (cb) ->
      $http.get 'assets/index.json' + hash
      .success (data) ->
        cb? data
      .error ->
        broadcast 'errIndexNotFound'

    # 请求文章
    post: (post, cb) ->
      if cb && post?.path
        $http.get 'posts/' + post.path + hash
        .success (data) ->
          cb data
        .error ->
          broadcast 'errPostNotFound'
      else
        broadcast 'errPostNotFound'
```

> 现在，我们已经开始搭建我们的 AngularJS ngApp。

> 下次，我们开始增加更多的服务。
