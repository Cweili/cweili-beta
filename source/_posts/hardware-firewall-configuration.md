title: 硬件防火墙的配置
tags:
  - 计算机
  - 计算机网络
id: 918
categories:
  - 学习笔记
date: 2011-11-30 12:08:47
---

本篇要为大家介绍一些实用的知识，那就是如何配置防火墙中的安全策略。但要注意的是，防火墙的具体配置方法也不是千篇一律的，不要说不同品牌，就是 同一品牌的不同型号也不完全一样，所以在此也只能对一些通用防火墙配置方法作一基本介绍。同时，具体的防火墙策略配置会因具体的应用环境不同而有较大区 别。首先介绍一些基本的配置原则。<!--more-->

一.防火墙的基本配置原则

默认情况下，所有的防火墙都是按以下两种情况配置的：

●拒绝所有的流量，这需要在你的网络中特殊指定能够进入和出去的流量的一些类型。
●允许所有的流量，这种情况需要你特殊指定要拒绝的流量的 类型。可论证地，大多数防火墙默认都是拒绝所有的流量作为安全选项。一旦你安装防火墙后，你需要打开一些必要的端口来使防火墙内的用户在通过验证之后可以 访问系统。换句话说，如果你想让你的员工们能够发送和接收Email，你必须在防火墙上设置相应的规则或开启允许POP3和SMTP的进程。

在防火墙的配置中，我们首先要遵循的原则就是安全实用，从这个角度考虑，在防火墙的配置过程中需坚持以下三个基本原则：

（1）.简单实用：对防火墙环境设计来讲，首要的就是越简单越好。其实这也是任何事物的基本原则。越简单的实现方式，越容易理解和使用。而且是设计越简单，越不容易出错，防火墙的安全功能越容易得到保证，管理也越可靠和简便。

每种产品在开发前都会有其主要功能定位，比如防火墙产品的初衷就是实现网络之 间的安全控制，入侵检测产品主要针对网络非法行为进行监控。但是随着技术的成熟和发展，这些产品在原来的主要功能之外或多或少地增加了一些增值功能，比如 在防火墙上增加了查杀病毒、入侵检测等功能，在入侵检测上增加了病毒查杀功能。但是这些增值功能并不是所有应用环境都需要，在配置时我们也可针对具体应用 环境进行配置，不必要对每一功能都详细配置，这样一则会大大增强配置难度，同时还可能因各方面配置不协调，引起新的安全漏洞，得不偿失。

（2）.全面深入：单一的防御措施是难以保障系统的安全的，只有采用全面的、多层次的深层防御战略体系才能实现系统的真正安全。在防火墙配置中，我 们不要停留在几个表面的防火墙语句上，而应系统地看等整个网络的安全防护体系，尽量使各方面的配置相互加强，从深层次上防护整个系统。这方面可以体现在两 个方面：一方面体现在防火墙系统的部署上，多层次的防火墙部署体系，即采用集互联网边界防火墙、部门边界防火墙和主机防火墙于一体的层次防御；另一方面将 入侵检测、网络加密、病毒查杀等多种安全措施结合在一起的多层安全体系。

（3）.内外兼顾：防火墙的一个特点是防外不防内，其实在现实的网络环境中，80%以上的威胁都来自内部，所以我们要树立防内的观念，从根本上改变 过去那种防外不防内的传统观念。对内部威胁可以采取其它安全措施，比如入侵检测、主机防护、漏洞扫描、病毒查杀。这方面体现在防火墙配置方面就是要引入全 面防护的观念，最好能部署与上述内部防护手段一起联动的机制。目前来说，要做到这一点比较困难。

二、防火墙的初始配置

像路由器一样，在使用之前，防火墙也需要经过基本的初始配置。但因各种防火墙的初始配置基本类似，所以在此仅以Cisco PIX防火墙为例进行介绍。

防火墙的初始配置也是通过控制端口（Console）与PC机（通常是便于移动的笔记本电脑）的串口连接，再通过Windows系统自带的超级终端 （HyperTerminal）程序进行选项配置。防火墙的初始配置物理连接与前面介绍的交换机初始配置连接方法一样，参见图1所示。

![](http://img.bianceng.cn/upimg/allimg/070602/315.jpg)

图1

防火墙除了以上所说的通过控制端口（Console）进行初始配置外，也可以通过telnet和Tffp配置方式进行高级配置，但Telnet配置方式都是在命令方式中配置，难度较大，而Tffp方式需要专用的Tffp服务器软件，但配置界面比较友好。

防火墙与路由器一样也有四种用户配置模式，即：普通模式（Unprivileged mode）、特权模式（Privileged Mode）、配置模式（Configuration Mode）和端口模式（Interface Mode），进入这四种用户模式的命令也与路由器一样：

普通用户模式无需特别命令，启动后即进入；

进入特权用户模式的命令为"enable"；进入配置模式的命令为"config terminal"；而进入端口模式的命令为"interface ethernet（）"。不过因为防火墙的端口没有路由器那么复杂，所以通常把端口模式归为配置模式，统称为"全局配置模式"。

防火墙的具体配置步骤如下：

1.将防火墙的Console端口用一条防火墙自带的串行电缆连接到笔记本电脑的一个空余串口上，参见图1。

2.打开PIX防火电源，让系统加电初始化，然后开启与防火墙连接的主机。

3.运行笔记本电脑Windows系统中的超级终端（HyperTerminal）程序（通常在"附件"程序组中）。对超级终端的配置与交换机或路由器的配置一样，参见本教程前面有关介绍。

4.当PIX防火墙进入系统后即显示"pixfirewall&gt;"的提示符，这就证明防火墙已启动成功，所进入的是防火墙用户模式。可以进行进一步的配置了。

5.输入命令：enable,进入特权用户模式，此时系统提示为：pixfirewall#。

6.输入命令： configure terminal,进入全局配置模式，对系统进行初始化设置。
（1）.首先配置防火墙的网卡参数（以只有1个LAN和1个WAN接口的防火墙配置为例）

Interface ethernet0 auto　 # 0号网卡系统自动分配为WAN网卡，"auto"选项为系统自适应网卡类型
Interface ethernet1 auto

（2）.配置防火墙内、外部网卡的IP地址

IP address inside ip_address netmask　# Inside代表内部网卡

IP address outside ip_address netmask　# outside代表外部网卡

（3）.指定外部网卡的IP地址范围：

global 1 ip_address-ip_address

（4）.指定要进行转换的内部地址

nat 1 ip_address netmask

（5）.配置某些控制选项：

conduit global_ip port[-port] protocol foreign_ip [netmask]

其中，global_ip：指的是要控制的地址；port：指的是所作用的端口，0代表所有端口；protocol：指的是连接协议，比 如：TCP、UDP等；foreign_ip：表示可访问的global_ip外部IP地址；netmask：为可选项，代表要控制的子网掩码。

7.配置保存：wr mem

8.退出当前模式

此命令为exit，可以任何用户模式下执行，执行的方法也相当简单，只输入命令本身即可。它与Quit命令一样。下面三条语句表示了用户从配置模式退到特权模式，再退到普通模式下的操作步骤。

pixfirewall(config)# exit
pixfirewall# exit
pixfirewall&gt;

9.查看当前用户模式下的所有可用命令：show，在相应用户模式下键入这个命令后，即显示出当前所有可用的命令及简单功能描述。

10.查看端口状态：show interface，这个命令需在特权用户模式下执行，执行后即显示出防火墙所有接口配置情况。

11.查看静态地址映射:show static，这个命令也须在特权用户模式下执行，执行后显示防火墙的当前静态地址映射情况。

三、Cisco PIX防火墙的基本配置

1.同样是用一条串行电缆从电脑的COM口连到Cisco PIX 525防火墙的console口；

2.开启所连电脑和防火墙的电源，进入Windows系统自带的"超级终端"，通讯参数可按系统默然。进入防火墙初始化配置，在其中主要设置 有：Date(日期)、time(时间)、hostname(主机名称)、inside ip address(内部网卡IP地址)、domain(主域)等，完成后也就建立了一个初始化设置了。此时的提示符为：pix255&gt;。

3.输入enable命令，进入Pix 525特权用户模式,默然密码为空。

如果要修改此特权用户模式密码，则可用enable password命令，命令格式为：enable password password [encrypted]，这个密码必须大于16位。Encrypted选项是确定所加密码是否需要加密。

4、 定义以太端口：先必须用enable命令进入特权用户模式，然后输入configure terminal（可简称为config t）,进入全局配置模式模式。具体配置

`pix525&gt;enable
Password:
pix525#config t
pix525 (config)#interface ethernet0 auto
pix525 (config)#interface ethernet1 auto`

在默然情况下ethernet0是属外部网卡outside, ethernet1是属内部网卡inside, inside在初始化配置成功的情况下已经被激活生效了，但是outside必须命令配置激活。

5.clock

配置时钟，这也非常重要，这主要是为防火墙的日志记录而资金积累的，如果日志记录时间和日期都不准确，也就无法正确分析记录中的信息。这须在全局配置模式下进行。

时钟设置命令格式有两种，主要是日期格式不同，分别为：

clock set hh:mm:ss month day month year和clock set hh:mm:ss day month year

前一种格式为：小时：分钟：秒 月 日 年；而后一种格式为：小时：分钟：秒 日 月 年，主要在日、月份的前后顺序不同。在时间上如果为0，可以为一位，如：21:0:0。

6.指定接口的安全级别

指定接口安全级别的命令为nameif，分别为内、外部网络接 口指定一个适当的安全级别。在此要注意，防火墙是用来保护内部网络的，外部网络是通过外部接口对内部网络构成威胁的，所以要从根本上保障内部网络的安全， 需要对外部网络接口指定较高的安全级别，而内部网络接口的安全级别稍低，这主要是因为内部网络通信频繁、可信度高。在Cisco PIX系列防火墙中，安全级别的定义是由security（）这个参数决定的，数字越小安全级别越高，所以security0是最高的，随后通常是以10 的倍数递增，安全级别也相应降低。如下例：

`pix525(config)#nameif ethernet0 outside security0　 # outside是指外部接口
pix525(config)#nameif ethernet1 inside security100 # inside是指内部接口`

7.配置以太网接口IP地址

所用命令为：ip address，如要配置防火墙上的内部网接口IP地址为：192.168.1.0 255.255.255.0；外部网接口IP地址为：220.154.20.0 255.255.255.0。
配置方法如下：
`pix525(config)#ip address inside 192.168.1.0 255.255.255.0
pix525(config)#ip address outside 220.154.20.0 255.255.255.0`

8.access-group

这个命令是把访问控制列表绑定在特定的接口上。须在配置模式下进行配置。命令格式为：access-group acl_ID in interface interface_name，其中的"acl_ID"是指访问控制列表名称，interface_name为网络接口名称。如：

access-group acl_out in interface outside，在外部网络接口上绑定名称为"acl_out"的访问控制列表。

clear access-group：清除所有绑定的访问控制绑定设置。

no access-group acl_ID in interface interface_name：清除指定的访问控制绑定设置。

show access-group acl_ID in interface interface_name：显示指定的访问控制绑定设置。

9．配置访问列表

所用配置命令为：access-list，合格格式比较复杂，如下：

标准规则的创建命令：access-list [ normal | special ] listnumber1 { permit | deny } source-addr [ source-mask ]
扩 展规则的创建命令：access-list [ normal | special ] listnumber2 { permit | deny } protocol source-addr source-mask [ operator port1 [ port2 ] ] dest-addr dest-mask [ operator port1 [ port2 ] | icmp-type [ icmp-code ] ] [ log ]

它是防火墙的主要配置部分，上述格式中带"[]"部分是可选项，listnumber参数是规则号，标准规则号（listnumber1）是 1~99之间的整数，而扩展规则号（listnumber2）是100~199之间的整数。它主要是通过访问权限"permit"和"deny"来指定 的，网络协议一般有IP|TCP|UDP|ICMP等等。如只允许访问通过防火墙对主机:220.154.20.254进行www访问，则可按以下配置：

pix525(config)#access-list 100 permit 220.154.20.254 eq www

其中的100表示访问规则号，根据当前已配置的规则条数来确定，不能与原来规则的重复，也必须是正整数。关于这个命令还将在下面的高级配置命令中详细介绍。