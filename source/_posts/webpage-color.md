title: 网页颜色代码表
tags:
  - 网页
  - 计算机网络
  - 颜色
id: 622
categories:
  - 学习笔记
date: 2011-10-25 01:17:52
---

留着方便查阅..- -..<!--more-->

<span class="Apple-style-span" style="color: #ff8000; font-size: large;">颜色代码表：</span>

<div align="center"><center>
<table width="640" border="0" cellpadding="2">
<tbody>
<tr>
<td colspan="10" width="112%" height="19">

<span style="color: #ff8000; font-size: x-small;">红色和粉红色，以及它们的</span><span style="color: #ff8000; font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="color: #ff8000; font-size: x-small;">进制代码。</span>

</td>
</tr>
<tr>
<td align="middle" bgcolor="#990033" width="11%" height="25"><span style="color: #ffffff;">#990033</span></td>
<td align="middle" bgcolor="#cc6699" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC6699</span></span></td>
<td align="middle" bgcolor="#ff6699" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF6699</span></span></td>
<td align="middle" bgcolor="#ff3366" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF3366</span></span></td>
<td align="middle" bgcolor="#993366" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#993366</span></span></td>
<td align="middle" bgcolor="#cc0066" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC0066</span></span></td>
<td align="middle" bgcolor="#cc0033" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC0033</span></span></td>
<td align="middle" bgcolor="#ff0066" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF0066</span></span></td>
<td align="middle" bgcolor="#ff0033" width="12%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF0033</span></span></td>
<td align="middle" valign="center" bgcolor="#cc3399" width="12%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">..#CC3399..</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#ff3399" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF3399</span></span></td>
<td align="middle" bgcolor="#ff9999" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF9999</span></span></td>
<td align="middle" bgcolor="#ff99cc" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF99CC</span></span></td>
<td align="middle" bgcolor="#ff0099" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF0099</span></span></td>
<td align="middle" bgcolor="#cc3366" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC3366</span></span></td>
<td align="middle" bgcolor="#ff66cc" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF66CC</span></span></td>
<td align="middle" bgcolor="#ff33cc" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF33CC</span></span></td>
<td align="middle" bgcolor="#ffccff" width="11%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FFCCFF</span></span></td>
<td align="middle" bgcolor="#ff99ff" width="12%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF99FF</span></span></td>
<td align="middle" bgcolor="#ff00cc" width="12%" height="25"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF00CC</span></span></td>
</tr>
<tr>
<td colspan="10" align="middle" width="112%" height="19">

<span style="color: #ff8000; font-size: x-small;">紫红色，以及它们的</span><span style="color: #ff8000; font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="color: #ff8000; font-size: x-small;">进制代码。</span>

</td>
</tr>
<tr>
<td align="middle" bgcolor="#ff66ff" width="11%" height="19"><span style="color: #ffffff;">#FF66FF</span></td>
<td align="middle" bgcolor="#cc33cc" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC33CC</span></span></td>
<td align="middle" bgcolor="#cc00ff" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC00FF</span></span></td>
<td align="middle" bgcolor="#ff33ff" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF33FF</span></span></td>
<td align="middle" bgcolor="#cc99ff" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC99FF</span></span></td>
<td align="middle" bgcolor="#9900cc" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9900CC</span></span></td>
<td align="middle" bgcolor="#ff00ff" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF00FF</span></span></td>
<td align="middle" bgcolor="#cc66ff" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC66FF</span></span></td>
<td align="middle" bgcolor="#990099" width="12%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#990099</span></span></td>
<td align="middle" bgcolor="#cc0099" width="12%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC0099</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#cc33ff" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC33FF</span></span></td>
<td align="middle" bgcolor="#cc99cc" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC99CC</span></span></td>
<td align="middle" bgcolor="#990066" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#990066</span></span></td>
<td align="middle" bgcolor="#993399" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#993399</span></span></td>
<td align="middle" bgcolor="#cc66cc" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC66CC</span></span></td>
<td align="middle" bgcolor="#cc00cc" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC00CC</span></span></td>
<td align="middle" bgcolor="#663366" width="11%" height="19"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#663366</span></span></td>
<td align="middle" width="11%" height="19"></td>
<td align="middle" width="12%" height="19"></td>
<td align="middle" width="12%" height="19"></td>
</tr>
<tr>
<td colspan="10" align="middle" width="112%" height="16"><span style="color: #ff8000; font-size: x-small;">蓝色，以及它们的</span><span style="color: #ff8000;"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="font-size: x-small;">进制代码。</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#660099" width="11%" height="16"><span style="color: #ffffff;">#660099</span></td>
<td align="middle" bgcolor="#666ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#666FF</span></span></td>
<td align="middle" bgcolor="#000cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#000CC</span></span></td>
<td align="middle" bgcolor="#9933cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9933CC</span></span></td>
<td align="middle" bgcolor="#666699" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#666699</span></span></td>
<td align="middle" bgcolor="#660066" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#660066</span></span></td>
<td align="middle" bgcolor="#333366" width="11%" height="16">

<span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#333366</span></span>

</td>
<td align="middle" bgcolor="#0066cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#0066CC</span></span></td>
<td align="middle" bgcolor="#9900ff" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9900FF</span></span></td>
<td align="middle" bgcolor="#333399" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#333399</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#99ccff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#99CCFF</span></span></td>
<td align="middle" bgcolor="#9933ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9933FF</span></span></td>
<td align="middle" bgcolor="#330099" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#330099</span></span></td>
<td align="middle" bgcolor="#6699ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#6699FF</span></span></td>
<td align="middle" bgcolor="#9966cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9966CC</span></span></td>
<td align="middle" bgcolor="#3300cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3300CC</span></span></td>
<td align="middle" bgcolor="#003366" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#003366</span></span></td>
<td align="middle" bgcolor="#330033" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#330033</span></span></td>
<td align="middle" bgcolor="#3300ff" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3300FF</span></span></td>
<td align="middle" bgcolor="#6699cc" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#6699CC</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#663399" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#663399</span></span></td>
<td align="middle" bgcolor="#3333ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3333FF</span></span></td>
<td align="middle" bgcolor="#006699" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#006699</span></span></td>
<td align="middle" bgcolor="#6633cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#6633CC</span></span></td>
<td align="middle" bgcolor="#3333cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3333CC</span></span></td>
<td align="middle" bgcolor="#3399cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3399CC</span></span></td>
<td align="middle" bgcolor="#6600cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#6600CC</span></span></td>
<td align="middle" bgcolor="#0066ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#0066FF</span></span></td>
<td align="middle" bgcolor="#0099cc" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#0099CC</span></span></td>
<td align="middle" bgcolor="#9966ff" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9966FF</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#0033ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#0033FF</span></span></td>
<td align="middle" bgcolor="#66ccff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#66CCFF</span></span></td>
<td align="middle" bgcolor="#330066" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#330066</span></span></td>
<td align="middle" bgcolor="#3366ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3366FF</span></span></td>
<td align="middle" bgcolor="#3399ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3399FF</span></span></td>
<td align="middle" bgcolor="#6600ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#6600FF</span></span></td>
<td align="middle" bgcolor="#3366cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#3366CC</span></span></td>
<td align="middle" bgcolor="#003399" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#003399</span></span></td>
<td align="middle" bgcolor="#6633ff" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#6633FF</span></span></td>
<td align="middle" bgcolor="#000066" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#000066</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#0099ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#0099FF</span></span></td>
<td align="middle" bgcolor="#ccccff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CCCCFF</span></span></td>
<td align="middle" bgcolor="#000033" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#000033</span></span></td>
<td align="middle" bgcolor="#33ccff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#33CCFF</span></span></td>
<td align="middle" bgcolor="#9999ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9999FF</span></span></td>
<td align="middle" bgcolor="#0000ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#0000FF</span></span></td>
<td align="middle" bgcolor="#00ccff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#00CCFF</span></span></td>
<td align="middle" bgcolor="#9999cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#9999CC</span></span></td>
<td align="middle" bgcolor="#000099" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#000099</span></span></td>
<td align="middle" bgcolor="#6666cc" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#6666CC</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#0033cc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#0033CC</span></span></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="12%" height="16"></td>
<td align="middle" width="12%" height="16"></td>
</tr>
<tr>
<td colspan="10" align="middle" width="112%" height="14"><span style="color: #ff8000; font-size: x-small;">黄色、褐色、玫瑰色和橙色，以及它们的</span><span style="color: #ff8000;"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="font-size: x-small;">进制代码。</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#ffffcc" width="11%" height="16"><span style="color: #000000;">#FFFFCC</span></td>
<td align="middle" bgcolor="#ffcc00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FFCC00</span></span></td>
<td align="middle" bgcolor="#cc9000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC99090</span></span></td>
<td align="middle" bgcolor="#663300" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#663300</span></span></td>
<td align="middle" bgcolor="#ff6600" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF6600</span></span></td>
<td align="middle" bgcolor="#663333" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#663333</span></span></td>
<td align="middle" bgcolor="#cc6666" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC6666</span></span></td>
<td align="middle" bgcolor="#ff6666" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF6666</span></span></td>
<td align="middle" bgcolor="#ff0000" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF0000</span></span></td>
<td align="middle" bgcolor="#ffff99" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #000000;">#FFFF99</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#ffcc66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FFCC66</span></span></td>
<td align="middle" bgcolor="#ff9900" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF9900</span></span></td>
<td align="middle" bgcolor="#ff9966" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF9966</span></span></td>
<td align="middle" bgcolor="#cc3300" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC3300</span></span></td>
<td align="middle" bgcolor="#996666" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#996666</span></span></td>
<td align="middle" bgcolor="#ffcccc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FFCCCC</span></span></td>
<td align="middle" bgcolor="#660000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#660000</span></span></td>
<td align="middle" bgcolor="#ff3300" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF3300</span></span></td>
<td align="middle" bgcolor="#ff6666" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF6666</span></span></td>
<td align="middle" bgcolor="#ffcc33" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FFCC33</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#cc6600" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC6600</span></span></td>
<td align="middle" bgcolor="#ff6633" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF6633</span></span></td>
<td align="middle" bgcolor="#996633" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#996633</span></span></td>
<td align="middle" bgcolor="#cc9999" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC9999</span></span></td>
<td align="middle" bgcolor="#ff3333" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF3333</span></span></td>
<td align="middle" bgcolor="#990000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#990000</span></span></td>
<td align="middle" bgcolor="#cc9966" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC9966</span></span></td>
<td align="middle" bgcolor="#ffff33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #000000;">#FFFF33</span></span></td>
<td align="middle" bgcolor="#cc9933" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC9933</span></span></td>
<td align="middle" bgcolor="#993300" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#993300</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#ff9933" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FF9933</span></span></td>
<td align="middle" bgcolor="#330000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#330000</span></span></td>
<td align="middle" bgcolor="#993333" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#993333</span></span></td>
<td align="middle" bgcolor="#cc3333" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC3333</span></span></td>
<td align="middle" bgcolor="#cc0000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC0000</span></span></td>
<td align="middle" bgcolor="#ffcc99" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#FFCC99</span></span></td>
<td align="middle" bgcolor="#ffff00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #000000;">#FFFF00</span></span></td>
<td align="middle" bgcolor="#996600" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#996600</span></span></td>
<td align="middle" bgcolor="#cc6633" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#CC6633</span></span></td>
<td align="middle" width="12%" height="16"></td>
</tr>
<tr>
<td colspan="10" align="middle" width="112%" height="14"><span style="color: #ff8000; font-size: x-small;">绿色，以及它们的</span><span style="color: #ff8000;"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="font-size: x-small;">进制代码。</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#99ffff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99FFFF</span></td>
<td align="middle" bgcolor="#33cccc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33CCCC</span></td>
<td align="middle" bgcolor="#00cc99" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00CC99</span></td>
<td align="middle" bgcolor="#99ff99" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99FF99</span></td>
<td align="middle" bgcolor="#009966" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#009966</span></td>
<td align="middle" bgcolor="#33ff33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33FF33</span></td>
<td align="middle" bgcolor="#33ff00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33FF00</span></td>
<td align="middle" bgcolor="#99cc33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99CC33</span></td>
<td align="middle" bgcolor="#ccc33" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCC33</span></td>
<td align="middle" bgcolor="#66ffff" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66FFFF</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#66cccc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66CCCC</span></td>
<td align="middle" bgcolor="#66ffcc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66FFCC</span></td>
<td align="middle" bgcolor="#66ff66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66FF66</span></td>
<td align="middle" bgcolor="#009933" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#009933</span></td>
<td align="middle" bgcolor="#00cc33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00CC33</span></td>
<td align="middle" bgcolor="#66ff00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66FF00</span></td>
<td align="middle" bgcolor="#336600" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#336600</span></td>
<td align="middle" bgcolor="#33300" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#33300</span></span></td>
<td align="middle" bgcolor="#33ffff" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33FFFF</span></td>
<td align="middle" bgcolor="#339999" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#339999</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#99ffcc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99FFCC</span></td>
<td align="middle" bgcolor="#339933" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#339933</span></td>
<td align="middle" bgcolor="#33ff66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33FF66</span></td>
<td align="middle" bgcolor="#33cc33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33CC33</span></td>
<td align="middle" bgcolor="#99ff00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99FF00</span></td>
<td align="middle" bgcolor="#669900" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#669900</span></td>
<td align="middle" bgcolor="#666600" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#666600</span></td>
<td align="middle" bgcolor="#00ffff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00FFFF</span></td>
<td align="middle" bgcolor="#336666" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#336666</span></td>
<td align="middle" bgcolor="#00ff99" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00FF99</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#99cc99" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99CC99</span></td>
<td align="middle" bgcolor="#00ff66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00FF66</span></td>
<td align="middle" bgcolor="#66ff33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66FF33</span></td>
<td align="middle" bgcolor="#66cc00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66CC00</span></td>
<td align="middle" bgcolor="#99cc00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99CC00</span></td>
<td align="middle" bgcolor="#999933" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#999933</span></td>
<td align="middle" bgcolor="#00cccc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00CCCC</span></td>
<td align="middle" bgcolor="#006666" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#006666</span></td>
<td align="middle" bgcolor="#339966" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#339966</span></td>
<td align="middle" bgcolor="#66ff99" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66FF99</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#ccffcc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCFFCC</span></td>
<td align="middle" bgcolor="#00ff00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00FF00</span></td>
<td align="middle" bgcolor="#00cc00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00CC00</span></td>
<td align="middle" bgcolor="#ccff66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCFF66</span></td>
<td align="middle" bgcolor="#cccc66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCCC66</span></td>
<td align="middle" bgcolor="#009999" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#009999</span></td>
<td align="middle" bgcolor="#003333" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#003333</span></span></td>
<td align="middle" bgcolor="#006633" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#006633</span></td>
<td align="middle" bgcolor="#33ff99" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33FF99</span></td>
<td align="middle" bgcolor="#ccff99" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCFF99</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#66cc33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66CC33</span></td>
<td align="middle" bgcolor="#33cc00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33CC00</span></td>
<td align="middle" bgcolor="#ccff33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCFF33</span></td>
<td align="middle" bgcolor="#666633" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#666633</span></td>
<td align="middle" bgcolor="#669999" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#669999</span></td>
<td align="middle" bgcolor="#00ffcc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00FFCC</span></td>
<td align="middle" bgcolor="#336633" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#336633</span></td>
<td align="middle" bgcolor="#33cc66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33CC66</span></td>
<td align="middle" bgcolor="#99ff66" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99FF66</span></td>
<td align="middle" bgcolor="#006600" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#006600</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#339900" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#339900</span></td>
<td align="middle" bgcolor="#ccff00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCFF00</span></td>
<td align="middle" bgcolor="#999966" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#999966</span></td>
<td align="middle" bgcolor="#99cccc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99CCCC</span></td>
<td align="middle" bgcolor="#33ffcc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33FFCC</span></td>
<td align="middle" bgcolor="#669966" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#669966</span></td>
<td align="middle" bgcolor="#00cc66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00CC66</span></td>
<td align="middle" bgcolor="#99ff33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99FF33</span></td>
<td align="middle" bgcolor="#003300" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#003300</span></span></td>
<td align="middle" bgcolor="#99cc66" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#99CC66</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#999900" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#999900</span></td>
<td align="middle" bgcolor="#cccc99" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCCC99</span></td>
<td align="middle" bgcolor="#ccffff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCFFFF</span></td>
<td align="middle" bgcolor="#33cc99" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#33CC99</span></td>
<td align="middle" bgcolor="#66cc66" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66CC66</span></td>
<td align="middle" bgcolor="#66cc99" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#66CC99</span></td>
<td align="middle" bgcolor="#00ff33" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#00FF33</span></td>
<td align="middle" bgcolor="#009900" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#009900</span></td>
<td align="middle" bgcolor="#669900" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#669900</span></td>
<td align="middle" bgcolor="#669933" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#669933</span></td>
</tr>
<tr>
<td align="middle" bgcolor="#cccc00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCCC00</span></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="12%" height="16"></td>
<td align="middle" width="12%" height="16"></td>
</tr>
<tr>
<td colspan="10" align="middle" width="112%" height="16"><span style="color: #ff8000; font-size: x-small;">白色、灰色和黑色，以及它们的</span><span style="color: #ff8000;"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="font-size: x-small;">进制代码。</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#fffff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#FFFFF</span></td>
<td align="middle" bgcolor="#cccccc" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">#CCCCCC</span></td>
<td align="middle" bgcolor="#999999" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#999999</span></span></td>
<td align="middle" bgcolor="#666666" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#666666</span></span></td>
<td align="middle" bgcolor="#333333" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#333333</span></span></td>
<td align="middle" bgcolor="#000000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">#000000</span></span></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="11%" height="16"></td>
<td align="middle" width="12%" height="16"></td>
<td align="middle" width="12%" height="16"></td>
</tr>
<tr>
<td colspan="10" align="middle" width="112%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="color: #ff8000; font-size: x-small;">色和它们的</span><span style="color: #ff8000;"><span style="font-family: 'MS Sans Serif'; font-size: x-small;">16</span><span style="font-size: x-small;">进制代码。</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#00ffff" width="11%" height="16"><span style="color: #000000;">Aqua</span></td>
<td align="middle" bgcolor="#000000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Black</span></span></td>
<td align="middle" bgcolor="#ff00ff" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Fuchsia</span></span></td>
<td align="middle" bgcolor="#800000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Gray</span></span></td>
<td align="middle" bgcolor="#008000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Gree</span></span></td>
<td align="middle" bgcolor="#00ff00" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #000000;">Lime</span></span></td>
<td align="middle" bgcolor="#800000" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Maroon</span></span></td>
<td align="middle" bgcolor="#000080" width="11%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Navy</span></span></td>
<td align="middle" bgcolor="#808000" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Olive</span></span></td>
<td align="middle" bgcolor="#800080" width="12%" height="16"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Purple</span></span></td>
</tr>
<tr>
<td align="middle" bgcolor="#ff0000" width="11%" height="18"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Red</span></span></td>
<td align="middle" bgcolor="#c0c0c0" width="11%" height="18"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Silver</span></span></td>
<td align="middle" bgcolor="#008080" width="11%" height="18"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Teal</span></span></td>
<td align="middle" bgcolor="#ffffff" width="11%" height="18"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #000000;">White</span></span></td>
<td align="middle" bgcolor="#ffff00" width="11%" height="18"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #000000;">Yellow</span></span></td>
<td align="middle" bgcolor="#0000ff" width="11%" height="18"><span style="font-family: 'MS Sans Serif'; font-size: x-small;"><span style="color: #ffffff;">Blue</span></span></td>
<td align="middle" width="11%" height="18"></td>
<td align="middle" width="11%" height="18"></td>
<td align="middle" width="12%" height="18"></td>
</tr>
</tbody>
</table>
&nbsp;

&nbsp;

</center></div>