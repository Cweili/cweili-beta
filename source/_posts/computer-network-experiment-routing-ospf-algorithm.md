title: 计算机网络 路由选择算法实验 OSPF算法 的修改代码
tags:
  - C++
  - 实验
  - 程序设计
  - 算法
  - 编程
  - 计算机
  - 计算机网络
id: 740
categories:
  - 学习笔记
date: 2011-10-31 00:55:24
---

由于老师给的代码没有输出路由表最短路径, 而且输入重复数据过多, 故做了一些小修改, 尽量保持了代码原貌, 修改处我已经着重标出, 供大家参考~
<!--more-->
```cpp
#include <iostream>
#include <cstdlib>
#define MAXPOINT 21//定义最大的顶点数目
#define limit 32767   //设置没有路径的权代替无穷大

using namespace std;

struct record{		//没个顶点的数据结构设置为一个数组队列
	int number;		//顶点号
	int flag;		//标志是否从队列中被选过如果为1表示选过为0表示未选
	int allpath;	//从源点到这个点的当前最短距离（权最小）
} path[MAXPOINT];   

int cost[MAXPOINT][MAXPOINT];//用来表示图的邻接矩阵

int main() {
	//**********顶点数目改为每次运行时输入**********
	int pointnum;
	cout << "请输入顶点数目:";
	cin >> pointnum;

	//temp表示在数组队列中的当前下标 front表示队列首 rear表示队列尾 
	//N 表示待输入的源点号码 minnumber 表示选中的点的号码
	int rear = pointnum, min = 32768, i, j, temp, front = 1, N, minnumber;//设置一个初始值
	for(i = 1; i <= pointnum; ++i) {
		for(j = 1; j <= pointnum; ++j) {
			//**********增加判断减少输入次数**********
			if(i > j)
				cost[i][j] = cost[j][i];
			else if(i != j) {
				cout << "请输入从第" << i << "点到第" << j << "点的路径长度(权)如果没有路径的话请输入'32767'\n";
				cin >> cost[i][j];//初始化所有点之间的（权）路径值
			} else cost[i][j] = 0;
		}
	}

	//cout << "请输入源点号" << endl; //输入一个起点
	//cin >> N;

	for(N = pointnum; N >= 1; --N) {//把每一个点轮流地都设置为起点，可以求出任意一对顶点之间的最短路径

		for(i = front; i <= rear; ++i) {//初始化每个点的信息
			if(i == N)
				path[i].allpath = 0;
			else
				path[i].allpath = limit;
			path[i].flag = 0;
			path[i].number = i;
		}

		int p[MAXPOINT][MAXPOINT] = { 0 };//**********用于记录最短路径**********

		while(rear >= 1) {//控制循环次数
			for(temp = front; temp <= pointnum; ++temp) {
				if(path[temp].allpath < min && !path[temp].flag) {//选出一个具有最值的点
					minnumber = path[temp].number; 
					min = path[temp].allpath;
				}
			}
			min = 32768;
			path[minnumber].flag = 1;//把选中的点的标志变量置1表示已经被选过避免选中

			for(i = 1; i <= pointnum; ++i) {//进行类似广度优先搜索，更新最短路径
				if((i != minnumber) && (path[minnumber].allpath + cost[minnumber][i] < path[i].allpath)) {
					path[i].allpath = path[minnumber].allpath + cost[minnumber][i];
					p[i][++p[i][0]] = minnumber;//**********记录最短路径**********
				}
			}
			--rear;//次数减1
		}
		rear = pointnum; //恢复数组以便于下一点的循环
		for(j = 1; j <= pointnum; ++j) {
			cout << "第" << N << "点到第" << j << "点的最短路径长度（权）为 ";
			cout << path[j].allpath << ", 路径: \n";
			for(int k = 1; k <= p[j][0]; cout << p[j][k++] << " -> ");//**********输出最短路径**********
			cout << j << endl;
		}
	}
	getchar();
	getchar();
	return 0;
}
```