title: 校园春景
tags:
  - 相册
  - 摄影
  - 校园
id: 5
categories:
  - 小生活
date: 2011-05-11 07:02:25
---

其实我们的学校从某个角度看过去还是很漂亮的...你们说是吧~

猜不到这些照片是用偶那破烂的才两百多元钱淘来的数码相机拍的吧~~

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rj5qf0ej.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rj5qf0ej.jpg)
<!--more-->

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rjl9z4cj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rjl9z4cj.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rjztv0vj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rjztv0vj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rkdqcs5j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rkdqcs5j.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rkt7fwxj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rkt7fwxj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rl5q4bsj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rl5q4bsj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rld6nibj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rld6nibj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rlr4xbij.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rlr4xbij.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rm6woryj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rm6woryj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rmm5yilj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rmm5yilj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rmy7vkxj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rmy7vkxj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rna7iorj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rna7iorj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rnid3xaj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rnid3xaj.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rnq9z0vj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rnq9z0vj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rnvvceuj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rnvvceuj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3ro10j3jj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3ro10j3jj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3roegt96j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3roegt96j.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rpspjugj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rpspjugj.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rq62tb4j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rq62tb4j.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rqlkv18j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rqlkv18j.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rrax2euj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rrax2euj.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rrn8y5sj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rrn8y5sj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rui972sj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rui972sj.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rvi0egmj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rvi0egmj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rvuf7lcj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rvuf7lcj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rwbsltbj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rwbsltbj.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3rwppyhtj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3rwppyhtj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rx4tog2j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rx4tog2j.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3rxqxtsij.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3rxqxtsij.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3s64wsbfj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3s64wsbfj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3s7gkjfcj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3s7gkjfcj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3ry6x2qfj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3ry6x2qfj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3ryf0i8fj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3ryf0i8fj.jpg)

[![安徽工程大学 春景](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3s6dkq5cj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3s6dkq5cj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3rz2qxlaj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3rz2qxlaj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3s0v79arj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3s0v79arj.jpg)

[![安徽工程大学 春景](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3s146gt4j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3s146gt4j.jpg)

[![安徽工程大学 春景](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3s1g3qo7j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3s1g3qo7j.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3s1v52kxj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3s1v52kxj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3s2aq1c1j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3s2aq1c1j.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3s2qwwhej.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3s2qwwhej.jpg)

[![安徽工程大学 春景](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3s39nqr0j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3s39nqr0j.jpg)

[![安徽工程大学 春景](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1dr3s3mku4kj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1dr3s3mku4kj.jpg)

[![安徽工程大学 春景](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1dr3s3y9afsj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1dr3s3y9afsj.jpg)

[![安徽工程大学 春景](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1dr3s4amkk5j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1dr3s4amkk5j.jpg)

[![安徽工程大学 春景](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1dr3s4nk0wwj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1dr3s4nk0wwj.jpg)