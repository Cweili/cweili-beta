title: 建立与配置 Apache WEB 服务器
tags:
  - PHP
  - 计算机
  - 计算机网络
id: 911
categories:
  - 学习笔记
date: 2011-11-20 11:47:46
---

1.  ### Apache服务器
开放源代码的Apache服务器拥有牢靠可信的美誉，所以从1995年1月以来，Apache一直是Internet上最流行的Web服务器。

Apache的优点：首先，Apache能运行在UNIX、Linux和Windows等多种操作系统平台之上。其次，Apache借助开放源代码开发模式的优势，得到全世界许多程序员的支持，程序员们为Apache编写了能完成许多有用功能的模块，借助这些功能模块，Apache具有无限扩展功能的优点。同时，Apache的工作性能和稳定性远远领先于其他同类产品。<!--more-->

目前几乎所有的Linux发行版都捆绑了Apache，Red Hat Enterprise Linux 5.0版本也不例外，默认情况下Red Hat Enterprise Linux安装程序会将Apache安装在系统上。

由于目前Apache被重新命名为httpd，因此可使用下面命令检查系统是否已经安装了Apache或查看已经安装的版本。

1.  ### 测试Apache服务器
在Linux服务器上执行“/etc/init.d/httpd start”命令，确认Web服务已经启动后，在客户端（可以是另一个工作站）使用的Web浏览器中输入Linux服务器的IP地址或域名进行访问。如果出现Apache的测试页面，则表示Web服务安装正确并且运行正常。

从这个章节开始，启用实例3.8.2创建的域名服务器，并依据实例3.8.2预设置的网络环境的IP地址规划，分别建立相应的基于Linux的网络服务器。因此，本章节中，Web服务器的IP地址是192.168.1.251,域名是www.teach.com。

1.  ### Web服务的配置文件
配置Apache服务器的运行参数，是通过编辑Apache的主配置文件httpd.conf来实现的。该文件的位置随着安装方式的不同而不同，如果使用RPM的方式安装，该文件通常存放在/etc/httpd/conf目录下；如果使用编译源代码的方式安装，该文件通常存放在Apache安装目录的conf子目录下。由于httpd.conf是一个文本文件，因此可以使用任何文本编辑器(如vi)对其进行编辑。

1.  ### httpd.conf文件的格式
httpd.conf配置文件主要由全局环境(Section 1：Global Environment)、主服务器配置(Section 2：‘Main’server configuration)和虚拟主机(Section 3：Virtual Hosts)3个部分组成。每部分都有相应的配置语句，该文件所有配置语句的语法为“配置参数名称 参数值”的形式，配置语句可以放在文件中的任何地方，但为了增强文件的可读性，最好将配置语句放在相应的部分。

httpd.conf中每行包含一条语句，行末使用反斜杠“\”可以续行，但是反斜杠与下一行中间不能有任何其他字符(包括空白)。httpd.conf的配置语句除了选项的参数值以外，所有选项指令均不区分大小写，可以在每一行前用“#”号表示注释。

在默认的httpd.conf文件中，每个配置语句和参数都有详细的解释，建议先使用Apache默认的httpd.conf文件作为模板进行修改设置，而且在修改之前先作好备份，以便做了错误的修改后还原。

1.  ### Web服务的基本配置
现在通过一个实例，说明Web服务器的建立过程。

[实例3.9.1]建立一个Web服务器，能够正确提供基本中文网页服务。

1.  #### 设置主目录的路径
观察Apache服务器主配置文件httpd.conf内容，可以看到Apache服务器主目录的默认路径位于“/var/www/html”，可以将需要发布的网页放在这个目录下。不过如果需要，也可以将主目录的路径修改为其他目录，以方便管理和使用。如将Apache服务器主目录路径设为“/home/username/www”,则修改主配置文件httpd.conf中的语句：DocumentRoot"/var/www/html"更改为：DocumentRoot"/home/username/www"。

1.  #### 设置默认文档
默认文档是指在Web浏览器中键入Web站点的IP地址或域名即显示出来的Web页面(即在URL中没有指定要访问的页面)，也就是通常所说的主页。在缺省的情况下，Apache的默认文档名为index.html，默认文档名由Directorylndex语句进行定义，默认的语句是：DirectoryIndexindex.htmlindex.html.var，因此采用默认方式时，主页文档命名应为index.html。也可以将Directorylndex语句中的默认文档名修改为其他文件。例如，添加index.htm和index.php文件作为默认文档：DirectoryIndexindex.htmlindex.htmindex.phpindex.html.var，这样服务器将依次判断使用不同的文件名：index.htmlindex.htmindex.phpindex.html.var。

1.  #### 设置Apache监听的IP地址和端口号
Apache默认会在本机所有可用IP地址上的TCP80端口监听客户端的请求。但需要使用多个Listen语句，以便在多个地址和端口上监听请求。默认的语句是：Listen 80

假定要设置服务器监听IP地址为：172.16.1.100的80端口和172.16.2.100的8080端口请求，需要添加语句：

Listen 172.16.1.100:80

Listen 172.16.2.100:8080

1.  #### 设置相对根目录的路径
相对根目录，通常是Apache存放配置文件和日志文件的地方。在默认的情况下，相对根目录是/etc/httpd。配置文件中的语句是：ServerRoot"/etc/httpd"。

1.  #### 设置日志文件
通过日志文件可以监控Apache的运行情况、出错原因和安全等问题。

*   错误日志
错误日志记录了Apache在启动或运行时发生的错误，所以当Apache出错时，应该先检查这个日志。通常错误日志的文件名为error_log，错误日志存放的位置和文件名可以通过ErrorLog参数设置。默认的语句是：ErrorLoglogs/error_log。

这里需要提醒的是，如果日志文件存放的路径不是以“/”开头，则意味着该路径是相对于ServerRoot目录的相对路径。

*   访问日志
访问日志记录了客户端所有的访问信息，通过分析访问日志可以知道客户机什么时间访问了网站的什么文件等信息。通常访问日志的文件名为access_log，访问日志存放的位置和文件名可以通过CustomLog参数设置。默认的语句是：CustomLoglogs/access_logcombined。

上面语句最后的combined指明日志使用的格式，在这个位置可以使用common或combined。common是指使用Web服务器普遍采用的“普通标准”格式(Common Log Format)，这种格式可以被许多日志分析程序所识别。combined是指使用“组合记录”格式(Combined Log Format)，其实combined与common格式基本相同，只是多了“引用页”和“浏览器识别”信息而已。common和combined格式由LogFormat语句进行定义。

1.  #### 设置网络管理员的E-mail地址
当客户端访问服务器发生错误时，服务器通常会向客户端返回错误提示网页，为了方便解决错误，在这个网页中通常包含有管理员的E-mail地址。可以使用ServerAdmin语句来设置管理员的E-mail地址。例如ServerAdmincheneychen@teach.com。

1.  #### 设置服务器主机名称
为了方便Apache识别服务器自身的信息，可以使用ServerName语句来设置服务器的主机名称。在ServerName语句中，如果服务器有域名，则填入服务器的域名；如果没有域名，则填入服务器的IP地址。例如：ServerNamewww.teach.com。

1.  #### 设置默认字符集
AddDefaultCharset选项定义了服务器返回给客户机的默认字符集。由于西欧(UTF-8)是 Apache的默认字符集，因此当客户端访问服务器的中文网页时会出现乱码的现象，如图图3.9.2所示。

解决的办法是将语句“AddDefaultCharset UTF-8”改为“AddDefaultCharset GB2312”，然后重新启动Apache服务

从主目录以外的其他目录中发布信息，就必须创建虚拟目录。虚拟目录是一个位于Apache的主目录外的目录，它不包含在Apache的主目录中，但对于访问Web站点的用户而言，其与位于主目录中的子目录是一样的。

每个虚拟目录都有个别名，用户在Web浏览器中可以通过此别名来访问虚拟目录，如http://服务器域名/别名/文件名或http://服务器IP地址/别名/文件名，就可以访问虚拟目录下的任何文件了。

使用虚拟目录，不仅便于访问，灵活加大磁盘空间，更便于移动站点中的目录。只要别名不变，更改了虚拟目录的实际存放位置，无需更改目录的URL，不会影响用户的访问。同时，使用虚拟目录，安全性好，通过给每个虚拟目录设置不同的访问权限，非常适合于不同用户对不同目录拥有不同权限的要求。

在主配置文件中，Apache默认已经创建了两个虚拟目录：

Alias/icons/"/var/www/icons/" /icons/对应的物理路径是/var/www/icons/

Alias/error/"/var/www/error/" /error/对应的物理路径是/var/www/error/

使用Alias选项可以创建虚拟目录，例如创建名为/down的虚拟目录，它对应的物理路径是“/software/download”的语句是Alias/down/"/software/download/"。

可以使用&lt;Directory 目录路径&gt;和&lt;/Directory&gt;这对语句为主目录或虚拟目录设置权限。这是一对容器语句，必须成对出现，它们之间封装的是具体的设置目录权限语句，这些语句仅对被设置目录及其子目录起作用。下面是主配置文件中设置目录权限的例子。如果用户需要深入地设置目录权限，必须对设置目录特性选项进行调整。

&lt;Directory "/var/www/html"&gt;

Options Indexes FollowSymLinks

AllowOverride None

Order allow,deny

Allow from all

&lt;/Directory&gt;

用户认证是网络安全中非常重要的技术，是保护网络系统资源的第一道防线。用户认证的目标是仅让合法用户以合法的权限访问网络系统资源。

Apache的用户认证，能够实现当用户第一次访问了启用用户认证目录下的任何文件时，浏览器会显示一个对话框，要求输入正确的登录用户名和口令进行用户身份的确认。如果是合法用户，，此后访问该目录的每个文件时，浏览器会自动送出用户名和密码，不需再次输入了，直到关闭浏览器为止。下面通过一个实际的例子说明在Apache中启用用户认证功能的方法。

[例3.9.2]假设有一个名为teachsource的虚拟目录，对应的物理路径是/usr/local/teachsource，现需要对其启用用户认证功能，只允许用户名为teacher和student的用户访问。

1.  #### 建立口令文件
要实现用户认证，首先要建立保存用户名和口令的文件。使用Apache自带的htpasswd命令建立和更新存储用户名和密码的文本文件。这个文件应存放在不能被网络访问的位置，以免被下载。本例将口令文件放在/etc/httpd/目录下，文件名为mysecretpwd。使用以下的命令建立口令文件。

命令语句中 -c 选项表示无论口令文件是否已经存在，都会重新写入文件并删除原有内容。所有添加第2个用户到口令文件时，就不应该使用了。

1.  #### 建立虚拟目录并配置用户认证
在Apache的主配置文件httpd.conf中加入以下语句建立虚拟目录并配置用户认证。

1.  #### 测试用户认证
首先在服务器中必须创建相应的虚拟目录“/usr/local/teachsource”,并在目录中放置要访问的网页，如index.html；使用命令“/etc/init.d/httpd restart”重新启动Web服务。

在客户端的Web浏览器中访问这个虚拟目录，这时浏览器会弹出对话框，要求输入用户名和口令。

输入正确的用户名和口令，就可访问目录了。

如果用户名和口令不正确，则出现错误提示信息。

1.  ### 为何使用虚拟主机
虚拟主机的出现源于网站的迅猛发展，“每站一机”的传统方式无法满足需求时，虚拟主机技术就应运而生了。利用虚拟主机技术，可以把一台真正的主机分成许多“虚拟”的主机，从而实现多用户对硬件资源、网络资源共享，大幅度降低了用户的建站成本。

每一台虚拟主机都具有独立的域名或IP地址，具有完整的wleb服务器功能。虚拟主机各用户之间是完全独立的，从外界来看，虚拟主机和独立主机的表现是完全一样的。

1.  ### 基于IP地址的虚拟主机的配置
基于IP地址的虚拟主机是在服务器里绑定多个IP，然后配置Apache服务的主配置文件，把多个网站绑定在不同的IP上，访问服务器上不同的IP，就看到不同的网站。

[例3.9.3]假设服务器有192.168.1.249和192.168.1.248两个IP地址，现需使用这两个IP地址分别创建两台虚拟主机，每台虚拟主机都对应不同的主目录。

1.  #### 服务器网卡配置子接口IP
如果服务器只有一张网卡，可以配置子接口IP地址。方法如下：

创建或修改/etc/sysconfig/network-scripts/ifcfg-eth0:1文件的内容如下：

DEVICE=eth0:1

BOOTPROTO=none

ONBOOT=yes

HWADDR=00:0c:29:79:89:1c

TYPE=Ethernet

NETMASK=255.255.255.0

IPADDR=192.168.1.249

这样重新启动系统后就添加了一个子接口IP地址：192.168.1.249。采用同样的方法，创建或修改/etc/sysconfig/network-scripts/ifcfg-eth0:2文件，也可添加另一个子接口IP地址：192.168.1.248。

重启后，使用ifconfig命令验证结果如下：

[root@www network-scripts]# ifconfig

eth0 Link encap:Ethernet HWaddr 00:0C:29:79:89:1C

inet addr:192.168.1.251 Bcast:192.168.1.255 Mask:255.255.255.0

inet6 addr: fe80::20c:29ff:fe79:891c/64 Scope:Link

UP BROADCAST RUNNING MULTICAST MTU:1500 Metric:1

RX packets:375 errors:0 dropped:0 overruns:0 frame:0

TX packets:326 errors:0 dropped:0 overruns:0 carrier:0

collisions:0 txqueuelen:1000

RX bytes:35863 (35.0 KiB) TX bytes:39547 (38.6 KiB)

Interrupt:177 Base address:0x2000

eth0:1 Link encap:Ethernet HWaddr 00:0C:29:79:89:1C

inet addr:192.168.1.249 Bcast:192.168.1.255 Mask:255.255.255.0

UP BROADCAST RUNNING MULTICAST MTU:1500 Metric:1

Interrupt:177 Base address:0x2000

eth0:2 Link encap:Ethernet HWaddr 00:0C:29:79:89:1C

inet addr:192.168.1.248 Bcast:192.168.1.255 Mask:255.255.255.0

UP BROADCAST RUNNING MULTICAST MTU:1500 Metric:1

Interrupt:177 Base address:0x2000

lo Link encap:Local Loopback

inet addr:127.0.0.1 Mask:255.0.0.0

inet6 addr: ::1/128 Scope:Host

UP LOOPBACK RUNNING MTU:16436 Metric:1

RX packets:8 errors:0 dropped:0 overruns:0 frame:0

TX packets:8 errors:0 dropped:0 overruns:0 carrier:0

collisions:0 txqueuelen:0

RX bytes:560 (560.0 b) TX bytes:560 (560.0 b)

[root@www network-scripts]#

1.  #### 在主配置文件httpd.conf中添加以下语句实现基于IP地址的虚拟主机的配置
创建虚拟主机需要在Apache的主配置文件httpd.conf中使用&lt;VirtualHost&gt;和&lt;/VirtualHost&gt;这对语句进行设置，这对语句必须成对出现，它们之间封装了设置虚拟主机属性的语句。例子中的语句&lt;VirtualHost虚拟主机的IP&gt;是指明这台虚拟主机使用哪个IP地址。如果虚拟主机需要独立的日志文件，应保证日志文件的路径存在，否则Apache将不能启动。因此，要在/etc/httpd/logs目录下创建web1和web2目录。

1.  #### 基于域名的虚拟主机的配置
基于域名的虚拟主机只需服务器有一个IP地址即可创建多台虚拟主机，所有的虚拟主机共享同一个IP地址，各虚拟主机之间通过域名进行区分。因为HTTP协议访问请求里包含DNS域名信息，所以当Web服务器收到访问请求时，就可以根据不同的DNS域名来访问不同的网站。它的优势就是不需要更多的IP地址，容易配置。

[例3.9.4]假设服务器的IP地址为192.168.1.251，在DNS服务器中有class1.teach.com和class2.teach.com主机地址A资源记录映射到该IP地址，现需使用这两个域名分别创建两台虚拟主机，每台虚拟主机都对应不同的主目录。

1.  实现方式是在主配置文件httpd.conf中添加以下语句：
必须注意，应该将服务器主配置文档中的主目录路径的配置语句：DocumentRoot"/var/www/html"先进行注释。

1.  在同网段的工作站上测试验证
创建基于域名的虚拟主机时，必须先用NamevirtualHost指令指定哪个IP地址负责响应对虚拟主机的请求，然后&lt;ⅥnualH0st虚拟主机的域名&gt;来指明这台虚拟主机使用哪个域名。

没有必要为每个虚拟主机指定所有的配置语句，因为虚拟主机中没有指定的配置语句将使用服务器主配置文档中的配置。

1.  ### 启动Web服务
启动Web服务的命令为：/etc/init.d/httpdstart

1.  ### 停止Web服务
启动Web服务的命令为：/etc/init.d/httpdstop

1.  ### 重新启动Web服务
启动Web服务的命令为：/etc/init.d/httpdrestart

1.  ### 自动启动Web服务
如果需要让wleb服务随系统启动而自动加载，可以执行“ntsysv”命令启动服务配置程序，找到“httpd”服务，在其前面加上星号(*)，然后选择“确定’’即可

必须注意是web服务使用TCP协议的80端口，如果Linux服务器开启了防火墙功能，就需关闭防火墙功能或设置允许TCP协议的80端口通过。可以使用以下命令开放TCP协议的80端口。

Iptables-IINPUT-ptcp –dport80-jACCEPT

以上Web服务的测试采用的网页都是使用HTML语言编写的简单网页。众所周知，HTML语言的功能是比较贫乏的，难以完成诸如访问数据库等一类的操作，而实际的情况则是经常需要先对数据库进行操作(比如文件检索系统)，然后把访问的结果动态地显示在网页上。诸如此类的需求只用HTML是无法做到的。

因此，要运行动态网页，还需在服务器上配置CGI运行环境，或者配置PHP运行环境，或者配置JSP运行环境。

CGI是在Web服务器运行的一个可执行程序，由网页的一个超链接激活进行调用，并对该程序的返回结果进行处理，显示在客户端的Web浏览器上。用CGI程序可以实现处理网页的表单处理、数据库查询、发送电子邮件等工作。CGI使网页变得不再是静态的，而是交互式的。

PHP是超级文本预处理语言PHP Hypertext Preprocessor的嵌套缩写。PHP是一种HTML内嵌式的语言，PHP与微软公司的ASP颇有几分相似，都是一种在服务器端执行的“嵌入HTML文档的脚本语言”，语言的风格类似于C语言，现在被很多的网站编程人员广泛运用。PHP独特的语法混合了C、Java、Perl以及PHP自创新的语法。它可以比CGI或者Perl更快速地执行动态网页。

JSP(Java Server Pages)是由Sun Microsystems公司倡导、许多公司一起参与建立的一种基于Java技术的动态网页技术标准。在传统的网页HTML文件(*.htm、*.html)中嵌入了Java代码的一个脚本，由脚本完成查询数据库、重新定向网页和发送电子邮件等动态操作。所有程序操作都在服务器端执行，网络上传送给客户端的仅是得到的HTML结果。在这一点上，JSP与ASP和PHP等脚本语言一样。但JSP与其他脚本不同的是，ASP和PHP等传统脚本语言由服务器直接解释这个脚本，而JSP则由JSP容器(如Tomcat)首先将其转化为Servlet，然后再调用Javac编译器将Servlet编译为二进制的Class文件，服务器最终运行的是Class文件，所以运行效率要比传统解析性的脚本语言高。

Apache只是一个Web服务器，不能运行JSP程序。如果要运行JSP程序，还需要安装Tomcat服务器软件，通过整合Tomcat与Apache来运行JSP程序。Tomcat是由Apache-Jakarta子项目支持的开放源代码服务器软件，它得到Sun公司的全力支持，而且Tomcat的运行效率非常高，所以它得到了广泛的应用。

&nbsp;