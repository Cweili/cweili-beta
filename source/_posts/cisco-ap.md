title: Cisco AP配置手册
tags:
  - 计算机
  - 计算机网络
id: 942
categories:
  - 学习笔记
date: 2011-11-13 13:33:13
---

<div>Cisco AP配置手册</div>
<div>Cisco AP的配置方法主要有控制台端口登录、远程登录（如Telnet/SSH等）、Web浏览器等，后两种方式均需要获取或设置一个IP方可登录。一般情况 下，AP设置IP地址的方法有按默认方式获取，配置DHCP方式获取，使用IPSU（IP Setup Utility），使用控制台端口等。<!--more--></div>
<div>实际上对Cisco AP来讲，配置的最简单的方法就是使用Web浏览器方式即GUI方式。Cisco AP有两种基本GUI界面，一种是支持IOS GUI，如Arionet 1100系列；一种是VxWorks GUI，如Arionet 350的AP和网桥；而Arionet 1200系列可以支持这两种GUI界面。</div>
<div>如下图Cisco Aironet 1100系列AP GUI配置界面：</div>
<div>除了GUI和远程登录方式外，CLI（命令行）也是Cisco AP经常使用的配置方式。GUI的配置方法比较简单，因篇幅所限，不再作介绍。下面我们在CLI（命令行）模式下对Cisco AP进行配置。</div>
<div>     Cisco AP的IOS的CLI（命令行）的配置模式有：用户模式、特权模式、全局模式、接口模式、线路模式等等，它的命令提示符、基本配置命令与基于Cisco IOS系统的路由器和交换机CLI的模式也基本相同，如果您对各种模式还不熟悉，请您参照路由器和交换机的基本模式配置章节。</div>
<div>1、配置主机名</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>ap#configure terminal</div></td>
<td>
<div>进入全局配置模式</div></td>
</tr>
<tr>
<td>
<div>2</div></td>
<td>
<div>ap(config)#hostname name</div></td>
<td>
<div>配置主机名</div></td>
</tr>
<tr>
<td>
<div>3</div></td>
<td>
<div>ap(config)#end</div></td>
<td>
<div>返回到特权EXEC模式</div></td>
</tr>
<tr>
<td>
<div>4</div></td>
<td>
<div>ap#show running-config</div></td>
<td>
<div>查看配置信息</div></td>
</tr>
<tr>
<td>
<div>5</div></td>
<td>
<div>ap#copy running-config starup-config</div></td>
<td>
<div>保存配置信息</div></td>
</tr>
</tbody>
</table>
</div>
<div>注意：hostname name命令中的name名称必须符合ARPANET主机名的规则，最多为63个字符，并且必须以一个字母开头，结尾必须是一个字母或数字，中间只能是字母、数字或连接符。</div>
<div>2、配置IP地址</div>
<div>为了实现对设备的远程管理，我们通常需要对设备配置管理地址，对于AP来说，我们可以通过配置AP的BVI地址来实现。</div>
<div>BVI即网桥虚拟接口，它是由AP自动创建的，当AP连接到有线网络时，AP使用BVI将所有接口都聚合到一个IP地址下，然后通过AP的以太网口和无线端口并使用该BVI的地址对AP进行管理。</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>ap#configure terminal</div></td>
<td>
<div>进入全局配置模式</div></td>
</tr>
<tr>
<td>
<div>2</div></td>
<td>
<div>ap(config)#interface bvi1</div></td>
<td>
<div>配置BVI接口</div></td>
</tr>
<tr>
<td>
<div>3</div></td>
<td>
<div>ap(config-if)#ip address address mask submask</div></td>
<td>
<div>配置BVI接口的地址和子网掩码</div></td>
</tr>
<tr>
<td>
<div>4</div></td>
<td>
<div>ap(config-if)#end 或者Ctrl+Z</div></td>
<td>
<div>返回到特权EXEC模式</div></td>
</tr>
<tr>
<td>
<div>5</div></td>
<td>
<div>ap#show running-config</div></td>
<td>
<div>查看配置信息</div></td>
</tr>
<tr>
<td>
<div>6</div></td>
<td>
<div>ap#copy running-config starup-config</div></td>
<td>
<div>保存配置信息</div></td>
</tr>
</tbody>
</table>
</div>
<div>3、配置网络映射</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>ap#configure terminal</div></td>
<td>
<div>进入全局配置模式</div></td>
</tr>
<tr>
<td>
<div>2</div></td>
<td>
<div>ap(config)# dot11 network-map [collect-interval]</div></td>
<td>
<div>启用网络映射</div></td>
</tr>
<tr>
<td>
<div>3</div></td>
<td>
<div>ap(config-if)#end 或者Ctrl+Z</div></td>
<td>
<div>退出配置模式</div></td>
</tr>
<tr>
<td>
<div>4</div></td>
<td>
<div>ap#show dot11 network-map</div></td>
<td>
<div>查看无线网络映射信息</div></td>
</tr>
<tr>
<td>
<div>5</div></td>
<td>
<div>ap#show dot11 adjacent-ap</div></td>
<td>
<div>查看与本AP相邻的AP信息。</div></td>
</tr>
<tr>
<td>
<div>6</div></td>
<td>
<div>ap#copy running-config starup-config</div></td>
<td>
<div>保存配置信息</div></td>
</tr>
</tbody>
</table>
</div>
<div>如上表命令中，第二步dot11 network-map [collect-interval]命令参数[collect-interval]为指定IAPP请求报文的时间（1-60秒），默认为5秒。</div>
<div>Network Map即网络映射主要用于显示无线网络中所有设备的信息，当该功能被启用时，AP每隔一定时间间隔都会广播一个IAPP（接入点间协议GenIfo Request 报文，该报文从第2层域中的所有Cisco AP处收集信息。接收到GenIfo Request 报文后，AP向请求方发送IAPP GenIfo Response报文，以建立一个新的网络映射。</div>
<div>4、显示关联信息</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>ap#show dot11 associations</div>
<div>[client | repeater | statistics | H.H.H | bss-only | all-client]</div></td>
<td>
<div>显示无线关联表或者无线关联统计信息，或有选择地显示所有转发器、所有客户端、一个特定的客户端或基本服务客户端的关联信息。</div></td>
</tr>
</tbody>
</table>
</div>
<div>如上表第一步show dot11 associations [client | repeater | statistics | H.H.H | bss-only | all-client]命令，参数说明如下：</div>
<div>[client]-显示关联到AP的所有客户端设备。</div>
<div>[repeater]-显示关联到AP的所有转发器设备。</div>
<div>[statistics]-显示无线接口的AP关联统计信息。</div>
<div>[H.H.H]（mac-address）-显示具有指定mac地址的客户端设备的详细信息。</div>
<div>[bss-only]-显示直接关联到AP的基本服务客户集客户端。</div>
<div>[all-client]-显示关联到AP的所有客户端的状态。</div>
<div>举例说明：</div>
<div>显示无线关联表:</div>
<div>AP# show dot11 associations</div>
<div>显示与AP关联到所有客户端设备：</div>
<div>AP# show dot11 associations client</div>
<div>显示AP的统计信息：</div>
<div>AP# show dot11 associations statistics</div>
<div>另外，我们可以使用以下命令清除相关信息：</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>clear dot11 client {mac-address}</div></td>
<td>
<div>解除一个具有指定MAC地址的无线客户端与AP的关联（该客户端直接关联到AP，而不是转发器）。</div></td>
</tr>
<tr>
<td>
<div>2</div></td>
<td>
<div>clear dot11 statistics{interface | mac-address}</div></td>
<td>
<div>清除一个特定的无线接口或一个具有指定MAC地址的客户端的统计信息。</div></td>
</tr>
</tbody>
</table>
</div>
<div>举例说明：</div>
<div>清除接口radio0的统计信息</div>
<div>ap# clear dot11 statistics dot11radio 0</div>
<div>清除MAC地址为0040.9631.81cf客户端的统计信息。</div>
<div>ap# clear dot11 statistics 0040.9631.81cf</div>
<div>5、配置以太网接口</div>
<div>Cisco AP以太网接口的配置方式与交换机和路由器基本一样，如下命令格式：</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>ap#configure terminal</div></td>
<td>
<div>进入全局配置模式</div></td>
</tr>
<tr>
<td>
<div>2</div></td>
<td>
<div>ap(config)#interface type-number</div></td>
<td>
<div>进入某类型接口的配置模式</div></td>
</tr>
</tbody>
</table>
</div>
<div>在该接口模式下，可以配置它的全双工模式、端口速率、开启/关闭端口等，当然也可以配置它的IP地址（不推荐）。例如：</div>
<div>ap#configure terminal</div>
<div>ap(config)#interface fastethernet 0</div>
<div>ap(config-if)#speed 100</div>
<div>ap(config-if)#duplex full</div>
<div>ap(config-if)#no shutdown</div>
<div>ap(config-if)#end</div>
<div>配置完以上信息后，我们可以使用以下show命令显示接口的信息：</div>
<div>ap#show interfaces</div>
<div>ap#show ip interface brief</div>
<div>ap#show running-config</div>
<div>6、配置无线接口</div>
<div>主要配置AP接口的SSID、工作模式（角色）、发射功率、速度、信道及扩展性能及其他设置等。</div>
<div>（1）、设置SSID</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>ap#configure terminal</div></td>
<td>
<div>进入全局配置模式</div></td>
</tr>
<tr>
<td>
<div>2</div></td>
<td>
<div>ap(config)#interface dot11radio interface-number</div></td>
<td>
<div>进入无线接口的配置模式,默认的dot11radio号为0</div></td>
</tr>
<tr>
<td>
<div>3</div></td>
<td>
<div>ap(config-if)#ssid ssid-string</div></td>
<td>
<div>创建一个SSID，并进入新SSID的配置模式</div></td>
</tr>
<tr>
<td>
<div>4</div></td>
<td>
<div>ap(config-if-ssid)#authentication open</div></td>
<td>
<div>为该SSID设置认证类型为开放式认证即允许任何设备进行认证并与AP通信</div></td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
</div>
<div>上表中ssid ssid-string命令 ssid-string命令参数最多可以包括32个字母和数字，对大小写敏感，并且不能包含空格。</div>
<div>另外，除了上表中的开放式认证方式外，还经常使用如下认证方式：</div>
<div>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<div>步骤</div></td>
<td>
<div>配置命令</div></td>
<td>
<div>解释</div></td>
</tr>
<tr>
<td>
<div>1</div></td>
<td>
<div>ap(config-if-ssid)#authentication shared</div>
<div>[mac-address list-name]</div>
<div>[eap list-name]</div></td>
<td>
<div>为该SSID设置可指定MAC地址和EAP列表的预共享密钥的认证方式</div></td>
</tr>
<tr>
<td>
<div>2</div></td>
<td>
<div>ap(config-if-ssid)#authentication</div>
<div>network-eap list-name</div>
<div>[mac-address list-name]</div></td>
<td>
<div>为该SSID设置可指定MAC地址列表的network-eap认证方式</div></td>
</tr>
</tbody>
</table>
</div>
<div>如下例所示：</div>
<div>ap#configure terminal</div>
<div>ap(config)#interface dot11radio 0</div>
<div>ap(config-if)#ssid cxj_v122</div>
<div>ap(config-if-ssid)#authentication open</div>
<div>7、检查无线设备状态
常用命令如下表所示：
<table cellspacing="0">
<tbody>
<tr>
<td>            配置命令</td>
<td>            解释</td>
</tr>
<tr>
<td>            ap#show interface dot11radio interface-number</td>
<td>            显示无线接口配置和统计信息</td>
</tr>
<tr>
<td>            ap#show interface dot11radio summary</td>
<td>            显示无线接口汇总统计信息</td>
</tr>
<tr>
<td>            ap#debug dot11 events</td>
<td>            对所有无线事件进行调试分析</td>
</tr>
<tr>
<td>            ap#debug dot11 packets</td>
<td>            对无线数据包进行调试分析</td>
</tr>
<tr>
<td>            ap#debug dot11 syslog</td>
<td>            调试并分析无线的系统日志</td>
</tr>
<tr>
<td>            ap#no debug dot11 events</td>
<td>            停止对无线事件进行调试</td>
</tr>
<tr>
<td>            Ap#undebug all</td>
<td>            停止所有的调试过程</td>
</tr>
</tbody>
</table>
8、其他基本设置
AP 的 IOS 配置命令格式和语义跟路由器、交换机 IOS 基本类同,
本章节不作
阐解,以下为各配置示例,您可以参阅 Cisco 命令手册来理解。
Telnet 服务的配置示例:
ap(config)#line vty 0 4
ap(config-line)#login
ap(config-line)#password Cisco_Aironet
ap#end
CDP 服务的配置示例:
ap(config)#no cdp run
ap(config)#interface fastethernet 0
ap(config-if)#cdp enable
ap(config-if)#end
ap#show cdp
DNS 服务的配置示例:
ap(config)#ip domain-name www.eskystar.com
ap(config)#no ip domain-lookup
HTTP 服务的配置示例:
ap(config)#ip http authentication local
ap(config)#ip http port 8080
ap(config)#access-list 10 permit host 192.168.0.11
ap(config)#ip http access-class 1
ap(config)#ip http help-path file:///c:\wireless\help
ap(config)#end

</div>