title: 让 Gulp Connect 支持 POST、PUT、DELETE 请求
date: 2014-11-12 23:06:31
tags:
  - Gulp
  - Node.js
  - JavaScript
categories:
  - 学习笔记
---
同 `grunt-contrib-connect` 插件一样，`gulp-connect` 可以很方便得在 Gulp 任务中创建一个本地服务器，但默认情况下，只提供了 `GET` 请求的支持，`POST`、`PUT`、`DELETE` 请求则会返回以下错误：

    405 Method Not Allowed
    
我们可以通过自定义一个中间件来完成 `POST`、`PUT`、`DELETE` 请求。
<!-- more -->

首先，需要引入 `fs`、`path` 和 `connect`

```js
var fs = require('fs');
var path = require('path');
var connect = require('gulp-connect');

```

接着，在配置中增加我们自己的中间件。

```js
gulp.task('connect', function() {
  return connect.server({
    middleware: function(connect, options) {
      return [
        function(req, res, next) {
          var filepath = path.join(options.root, req.url);
          if ('POSTPUTDELETE'.indexOf(req.method.toUpperCase()) > -1
            && fs.existsSync(filepath) && fs.statSync(filepath).isFile()) {
            return res.end(fs.readFileSync(filepath));
          }
          return next();
        }
      ];
    }
  });
});
```
