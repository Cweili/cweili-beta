title: 柳絮纷飞烟雨西塘
tags:
  - 相册
  - 摄影
  - 浙江
  - 西塘
categories:
  - 小生活
date: 2014-09-21 01:05:40
---
又是一年秋高气爽之时，为了工作之余调整一番心情，我与家人出发浙江游玩水乡乌镇西塘，泛舟西子湖上，一游西溪湿地。

游玩照片集：

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjcmr534qj20xc0p014u.jpg)](http://cweili.gitcafe.io/wuzhen/#more)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/small/6f5b68c7gw1ekjdeowobuj20xc0p0124.jpg)](http://cweili.gitcafe.io/xitang/#more)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/small/6f5b68c7gw1ekjd19iw0lj20xc0p04e5.jpg)](http://cweili.gitcafe.io/xizihu/#more)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjgof4su1j20xc0p0175.jpg)](http://cweili.gitcafe.io/xixishidi/#more)
<!--more-->

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdd06pqej20xc0p0wn7.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdd06pqej20xc0p0wn7.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjdd2y5xhj20xc0p0amh.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjdd2y5xhj20xc0p0amh.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjdd4xa4mj20xc0p0jzr.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjdd4xa4mj20xc0p0jzr.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdd7fgb9j20xc0p0qht.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdd7fgb9j20xc0p0qht.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjdd9qp5lj20xc0p0nam.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjdd9qp5lj20xc0p0nam.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjddblq5dj20xc0p0460.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjddblq5dj20xc0p0460.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjdddg5qnj20xc0p010x.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjdddg5qnj20xc0p010x.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjddfq43qj20xc0p0do7.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjddfq43qj20xc0p0do7.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjddhfjcaj20xc0p0the.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjddhfjcaj20xc0p0the.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjddjnujej20xc0p049z.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjddjnujej20xc0p049z.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjddlige0j20xc0p07eb.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjddlige0j20xc0p07eb.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjddo3nh5j20xc0p0k47.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjddo3nh5j20xc0p0k47.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjddqfzxdj20xc0p0woa.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjddqfzxdj20xc0p0woa.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjddsnbafj20xc0p0dre.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjddsnbafj20xc0p0dre.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjddv5fe4j20xc0p0qg3.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjddv5fe4j20xc0p0qg3.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjde0623ej20xc0p0toe.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjde0623ej20xc0p0toe.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjde2kn73j20xc0p0k6c.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjde2kn73j20xc0p0k6c.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjde4d6g9j20xc0p0aj0.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjde4d6g9j20xc0p0aj0.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjde69e0sj20xc0p0al1.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjde69e0sj20xc0p0al1.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjde8i9e3j20xc0p0qdl.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjde8i9e3j20xc0p0qdl.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjdeap87hj20xc0p07d3.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjdeap87hj20xc0p07d3.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjdeckopdj20xc0p014l.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjdeckopdj20xc0p014l.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjdeeeyhgj20xc0p0k09.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjdeeeyhgj20xc0p0k09.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdegzr81j20xc0p07h5.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdegzr81j20xc0p07h5.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdeiqxjoj20xc0p0tkn.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdeiqxjoj20xc0p0tkn.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjdekr10kj20xc0p0133.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjdekr10kj20xc0p0133.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjden8qpej20xc0p0174.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjden8qpej20xc0p0174.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjdeowobuj20xc0p0124.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjdeowobuj20xc0p0124.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdeqwet8j20xc0p0tlo.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdeqwet8j20xc0p0tlo.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdet8f9ej20xc0p0ali.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdet8f9ej20xc0p0ali.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdevqhkzj20xc0p0n9j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdevqhkzj20xc0p0n9j.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjdexiatwj20xc0p07es.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjdexiatwj20xc0p07es.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjdezbr2jj20xc0p012u.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjdezbr2jj20xc0p012u.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjdf1qofnj20xc0p0468.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjdf1qofnj20xc0p0468.jpg)

[![柳絮纷飞烟雨西塘](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjdf47i29j20xc0p0180.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjdf47i29j20xc0p0180.jpg)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjdf6ggwmj20xc0p0wpi.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjdf6ggwmj20xc0p0wpi.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdf8ntomj20xc0p04ae.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdf8ntomj20xc0p04ae.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdfapvv6j20xc0p07gm.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdfapvv6j20xc0p07gm.jpg)

[![柳絮纷飞烟雨西塘](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjdfd40hoj20xc0p0n96.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjdfd40hoj20xc0p0n96.jpg)

[![柳絮纷飞烟雨西塘](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjdfft1r6j20xc0p04b9.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjdfft1r6j20xc0p04b9.jpg)
