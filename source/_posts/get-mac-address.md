title: 查看网卡MAC物理地址
tags:
  - 计算机
  - 计算机网络
id: 949
categories:
  - 学习笔记
date: 2011-10-30 13:54:29
---

#### Windows操作系统

Win+R打开运行，输入cmd，输入getmac。

#### UNIX/Linux操作系统

ifconfig命令。