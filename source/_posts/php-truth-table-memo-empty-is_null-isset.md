title: PHP真值表备忘录 empty is_null isSet == ===
tags:
  - PHP
  - 程序设计
  - 编程
  - 计算机
id: 811
categories:
  - 学习笔记
date: 2011-11-14 22:19:42
---

PHP里面，有众多的函数检查一个变量是否存在，或者是否为真，或者是否为空。例如： empty, is_null, isSet, == null等等，你知道他们之间的细节差异吗？表中的T就是True, F就是False<!--more-->

原文地址: [http://www.blueshoes.org/en/developer/php_cheat_sheet/](http://www.blueshoes.org/en/developer/php_cheat_sheet/)

## 检查变量的函数

<table border="1" cellspacing="0" cellpadding="4">
<tbody>
<tr bgcolor="#dde2e6">
<td id="cheatTd1_0_0"></td>
<td id="cheatTd1_1_0">gettype()</td>
<td id="cheatTd1_2_0">empty()</td>
<td id="cheatTd1_3_0">is_null()</td>
<td id="cheatTd1_4_0">isSet()</td>
<td id="cheatTd1_5_0">(bool)</td>
</tr>
<tr>
<td id="cheatTd1_0_1" bgcolor="#dde2e6">$x = “”;</td>
<td id="cheatTd1_1_1">string</td>
<td id="cheatTd1_2_1"><span style="color: green;">T</span></td>
<td id="cheatTd1_3_1"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_1"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_1"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd1_0_2" bgcolor="#dde2e6">$x = null;</td>
<td id="cheatTd1_1_2">NULL</td>
<td id="cheatTd1_2_2"><span style="color: green;">T</span></td>
<td id="cheatTd1_3_2"><span style="color: green;">T</span></td>
<td id="cheatTd1_4_2"><span style="color: red;">F</span></td>
<td id="cheatTd1_5_2"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd1_0_3" bgcolor="#dde2e6">var $x; (not set)</td>
<td id="cheatTd1_1_3">NULL</td>
<td id="cheatTd1_2_3"><span style="color: green;">T</span></td>
<td id="cheatTd1_3_3"><span style="color: green;">T</span></td>
<td id="cheatTd1_4_3"><span style="color: red;">F</span></td>
<td id="cheatTd1_5_3"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd1_0_4" bgcolor="#dde2e6">$x = array();</td>
<td id="cheatTd1_1_4">array</td>
<td id="cheatTd1_2_4"><span style="color: green;">T</span></td>
<td id="cheatTd1_3_4"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_4"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_4"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd1_0_5" bgcolor="#dde2e6">$x = false;</td>
<td id="cheatTd1_1_5">boolean</td>
<td id="cheatTd1_2_5"><span style="color: green;">T</span></td>
<td id="cheatTd1_3_5"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_5"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_5"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd1_0_6" bgcolor="#dde2e6">$x = 15;</td>
<td id="cheatTd1_1_6">integer</td>
<td id="cheatTd1_2_6"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_6"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_6"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_6"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_7" bgcolor="#dde2e6">$x = 1;</td>
<td id="cheatTd1_1_7">integer</td>
<td id="cheatTd1_2_7"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_7"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_7"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_7"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_8" bgcolor="#dde2e6">$x = 0;</td>
<td id="cheatTd1_1_8">integer</td>
<td id="cheatTd1_2_8"><span style="color: green;">T</span></td>
<td id="cheatTd1_3_8"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_8"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_8"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd1_0_9" bgcolor="#dde2e6">$x = -1;</td>
<td id="cheatTd1_1_9">integer</td>
<td id="cheatTd1_2_9"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_9"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_9"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_9"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_10" bgcolor="#dde2e6">$x = “15″;</td>
<td id="cheatTd1_1_10">string</td>
<td id="cheatTd1_2_10"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_10"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_10"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_10"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_11" bgcolor="#dde2e6">$x = “1″;</td>
<td id="cheatTd1_1_11">string</td>
<td id="cheatTd1_2_11"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_11"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_11"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_11"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_12" bgcolor="#dde2e6">$x = “0″;</td>
<td id="cheatTd1_1_12">string</td>
<td id="cheatTd1_2_12"><span style="color: green;">T</span></td>
<td id="cheatTd1_3_12"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_12"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_12"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd1_0_13" bgcolor="#dde2e6">$x = “-1″;</td>
<td id="cheatTd1_1_13">string</td>
<td id="cheatTd1_2_13"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_13"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_13"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_13"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_14" bgcolor="#dde2e6">$x = “foo”;</td>
<td id="cheatTd1_1_14">string</td>
<td id="cheatTd1_2_14"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_14"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_14"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_14"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_15" bgcolor="#dde2e6">$x = “true”;</td>
<td id="cheatTd1_1_15">string</td>
<td id="cheatTd1_2_15"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_15"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_15"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_15"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd1_0_16" bgcolor="#dde2e6">$x = “false”;</td>
<td id="cheatTd1_1_16">string</td>
<td id="cheatTd1_2_16"><span style="color: red;">F</span></td>
<td id="cheatTd1_3_16"><span style="color: red;">F</span></td>
<td id="cheatTd1_4_16"><span style="color: green;">T</span></td>
<td id="cheatTd1_5_16"><span style="color: green;">T</span></td>
</tr>
</tbody>
</table>
注意： empty() 和isSet() 在传入一个未定义变量的时候，报warning的。

## 用==比较

<table border="1" cellspacing="0" cellpadding="4">
<tbody>
<tr>
<td id="cheatTd2_0_0" bgcolor="#dde2e6"></td>
<td id="cheatTd2_1_0" bgcolor="#dde2e6">true</td>
<td id="cheatTd2_2_0" bgcolor="#dde2e6">false</td>
<td id="cheatTd2_3_0" bgcolor="#dde2e6">1</td>
<td id="cheatTd2_4_0" bgcolor="#dde2e6">0</td>
<td id="cheatTd2_5_0" bgcolor="#dde2e6">-1</td>
<td id="cheatTd2_6_0" bgcolor="#dde2e6">“1″</td>
<td id="cheatTd2_7_0" bgcolor="#dde2e6">“0″</td>
<td id="cheatTd2_8_0" bgcolor="#dde2e6">“-1″</td>
<td id="cheatTd2_9_0" bgcolor="#dde2e6">“1.3″</td>
<td id="cheatTd2_11_0" bgcolor="#dde2e6">1.3</td>
<td id="cheatTd2_13_0" bgcolor="#dde2e6">array()</td>
<td id="cheatTd2_14_0" bgcolor="#dde2e6">“foo”</td>
<td id="cheatTd2_15_0" bgcolor="#dde2e6">“”</td>
</tr>
<tr>
<td id="cheatTd2_0_1" bgcolor="#dde2e6">true</td>
<td id="cheatTd2_1_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_1"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_4_1"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_6_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_7_1"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_9_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_11_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_13_1"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_1"><span style="color: green;">T</span></td>
<td id="cheatTd2_15_1"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_2" bgcolor="#dde2e6">false</td>
<td id="cheatTd2_1_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_2_2"><span style="color: green;">T</span></td>
<td id="cheatTd2_3_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_2"><span style="color: green;">T</span></td>
<td id="cheatTd2_5_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_2"><span style="color: green;">T</span></td>
<td id="cheatTd2_8_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_2"><span style="color: green;">T</span></td>
<td id="cheatTd2_14_2"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_2"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd2_0_3" bgcolor="#dde2e6">1</td>
<td id="cheatTd2_1_3"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_3"><span style="color: green;">T</span></td>
<td id="cheatTd2_4_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_3"><span style="color: green;">T</span></td>
<td id="cheatTd2_7_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_3"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_3"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_4" bgcolor="#dde2e6">0</td>
<td id="cheatTd2_1_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_2_4"><span style="color: green;">T</span></td>
<td id="cheatTd2_3_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_4"><span style="color: green;">T</span></td>
<td id="cheatTd2_5_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_4"><span style="color: green;">T</span></td>
<td id="cheatTd2_8_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_4"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_4"><span style="color: green;">T</span></td>
<td id="cheatTd2_15_4"><span style="color: green;">T</span></td>
</tr>
<tr>
<td id="cheatTd2_0_5" bgcolor="#dde2e6">-1</td>
<td id="cheatTd2_1_5"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_5"><span style="color: green;">T</span></td>
<td id="cheatTd2_6_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_5"><span style="color: green;">T</span></td>
<td id="cheatTd2_9_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_5"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_5"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_6" bgcolor="#dde2e6">“1″</td>
<td id="cheatTd2_1_6"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_6"><span style="color: green;">T</span></td>
<td id="cheatTd2_4_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_6"><span style="color: green;">T</span></td>
<td id="cheatTd2_7_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_6"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_6"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_7" bgcolor="#dde2e6">“0″</td>
<td id="cheatTd2_1_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_2_7"><span style="color: green;">T</span></td>
<td id="cheatTd2_3_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_7"><span style="color: green;">T</span></td>
<td id="cheatTd2_5_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_7"><span style="color: green;">T</span></td>
<td id="cheatTd2_8_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_7"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_7"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_8" bgcolor="#dde2e6">“-1″</td>
<td id="cheatTd2_1_8"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_8"><span style="color: green;">T</span></td>
<td id="cheatTd2_6_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_8"><span style="color: green;">T</span></td>
<td id="cheatTd2_9_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_8"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_8"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_9" bgcolor="#dde2e6">“1.3″</td>
<td id="cheatTd2_1_9"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_9"><span style="color: green;">T</span></td>
<td id="cheatTd2_11_9"><span style="color: green;">T</span></td>
<td id="cheatTd2_13_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_9"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_9"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_11" bgcolor="#dde2e6">1.3</td>
<td id="cheatTd2_1_11"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_11"><span style="color: green;">T</span></td>
<td id="cheatTd2_11_11"><span style="color: green;">T</span></td>
<td id="cheatTd2_13_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_11"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_11"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_13" bgcolor="#dde2e6">array()</td>
<td id="cheatTd2_1_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_2_13"><span style="color: green;">T</span></td>
<td id="cheatTd2_3_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_5_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_13"><span style="color: green;">T</span></td>
<td id="cheatTd2_14_13"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_13"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_14" bgcolor="#dde2e6">“foo”</td>
<td id="cheatTd2_1_14"><span style="color: green;">T</span></td>
<td id="cheatTd2_2_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_3_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_14"><span style="color: green;">T</span></td>
<td id="cheatTd2_5_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_14"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_14"><span style="color: green;">T</span></td>
<td id="cheatTd2_15_14"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd2_0_15" bgcolor="#dde2e6">“”</td>
<td id="cheatTd2_1_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_2_15"><span style="color: green;">T</span></td>
<td id="cheatTd2_3_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_4_15"><span style="color: green;">T</span></td>
<td id="cheatTd2_5_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_6_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_7_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_8_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_9_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_11_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_13_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_14_15"><span style="color: red;">F</span></td>
<td id="cheatTd2_15_15"><span style="color: green;">T</span></td>
</tr>
</tbody>
</table>

## 用===比较

<table border="1" cellspacing="0" cellpadding="4">
<tbody>
<tr>
<td id="cheatTd3_0_0" bgcolor="#dde2e6"></td>
<td id="cheatTd3_1_0" bgcolor="#dde2e6">true</td>
<td id="cheatTd3_2_0" bgcolor="#dde2e6">false</td>
<td id="cheatTd3_3_0" bgcolor="#dde2e6">1</td>
<td id="cheatTd3_4_0" bgcolor="#dde2e6">0</td>
<td id="cheatTd3_5_0" bgcolor="#dde2e6">-1</td>
<td id="cheatTd3_6_0" bgcolor="#dde2e6">“1″</td>
<td id="cheatTd3_7_0" bgcolor="#dde2e6">“0″</td>
<td id="cheatTd3_8_0" bgcolor="#dde2e6">“-1″</td>
<td id="cheatTd3_9_0" bgcolor="#dde2e6">“1.3″</td>
<td id="cheatTd3_11_0" bgcolor="#dde2e6">1.3</td>
<td id="cheatTd3_13_0" bgcolor="#dde2e6">array()</td>
<td id="cheatTd3_14_0" bgcolor="#dde2e6">“foo”</td>
<td id="cheatTd3_15_0" bgcolor="#dde2e6">“”</td>
</tr>
<tr>
<td id="cheatTd3_0_1" bgcolor="#dde2e6">true</td>
<td id="cheatTd3_1_1"><span style="color: green;">T</span></td>
<td id="cheatTd3_2_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_1"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_1"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_2" bgcolor="#dde2e6">false</td>
<td id="cheatTd3_1_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_2"><span style="color: green;">T</span></td>
<td id="cheatTd3_3_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_2"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_2"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_3" bgcolor="#dde2e6">1</td>
<td id="cheatTd3_1_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_3"><span style="color: green;">T</span></td>
<td id="cheatTd3_4_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_3"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_3"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_4" bgcolor="#dde2e6">0</td>
<td id="cheatTd3_1_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_4"><span style="color: green;">T</span></td>
<td id="cheatTd3_5_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_4"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_4"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_5" bgcolor="#dde2e6">-1</td>
<td id="cheatTd3_1_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_5"><span style="color: green;">T</span></td>
<td id="cheatTd3_6_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_5"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_5"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_6" bgcolor="#dde2e6">“1″</td>
<td id="cheatTd3_1_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_6"><span style="color: green;">T</span></td>
<td id="cheatTd3_7_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_6"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_6"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_7" bgcolor="#dde2e6">“0″</td>
<td id="cheatTd3_1_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_7"><span style="color: green;">T</span></td>
<td id="cheatTd3_8_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_7"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_7"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_8" bgcolor="#dde2e6">“-1″</td>
<td id="cheatTd3_1_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_8"><span style="color: green;">T</span></td>
<td id="cheatTd3_9_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_8"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_8"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_9" bgcolor="#dde2e6">“1.3″</td>
<td id="cheatTd3_1_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_9"><span style="color: green;">T</span></td>
<td id="cheatTd3_11_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_9"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_9"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_11" bgcolor="#dde2e6">1.3</td>
<td id="cheatTd3_1_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_11"><span style="color: green;">T</span></td>
<td id="cheatTd3_13_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_11"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_11"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_13" bgcolor="#dde2e6">array()</td>
<td id="cheatTd3_1_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_13"><span style="color: green;">T</span></td>
<td id="cheatTd3_14_13"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_13"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_14" bgcolor="#dde2e6">“foo”</td>
<td id="cheatTd3_1_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_14"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_14"><span style="color: green;">T</span></td>
<td id="cheatTd3_15_14"><span style="color: red;">F</span></td>
</tr>
<tr>
<td id="cheatTd3_0_15" bgcolor="#dde2e6">“”</td>
<td id="cheatTd3_1_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_2_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_3_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_4_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_5_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_6_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_7_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_8_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_9_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_11_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_13_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_14_15"><span style="color: red;">F</span></td>
<td id="cheatTd3_15_15"><span style="color: green;">T</span></td>
</tr>
</tbody>
</table>