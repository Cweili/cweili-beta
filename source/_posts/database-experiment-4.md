title: 数据库实验 实验四 SQL命令
tags:
  - SQL
  - 实验
  - 数据库
  - 计算机
id: 615
categories:
  - 学习笔记
date: 2011-10-24 21:26:08
---

数据库实验四的代码记录<!--more-->
```sql
--------------------------------------------------------------------------------
--实验四
--1
--a
CREATE TABLE Avg (
	Cno char(1) NOT NULL,
	Grade decimal(4,1) NOT NULL
);
INSERT INTO Avg SELECT Cno, AVG(Grade) FROM Cj GROUP BY Cno;
--（所影响的行数为 6 行）

--b
UPDATE Cj SET Grade = Grade + 5 WHERE '01311' = (SELECT Clno FROM Student WHERE Student.Sno = Cj.Sno);
--（所影响的行数为 5 行）

--c
DELETE FROM Cj WHERE '01311' = (SELECT Clno FROM Student WHERE Student.Sno = Cj.Sno);
--（所影响的行数为 5 行）

--2
--i
SELECT Sname, Ssex, Clno FROM Student;
--（所影响的行数为 7 行）返回7条结果

--ii
SELECT * FROM Cj WHERE Grade IN (78, 88, 90);
--（所影响的行数为 3 行）返回3条结果

--iii
SELECT * FROM Student WHERE Clno = '00311' AND Ssex = '女';
--（所影响的行数为 1 行）返回1条结果

--iv
SELECT AVG(Grade) FROM Cj GROUP BY Cno HAVING '数据库' = (SELECT Cname FROM Course WHERE Course.Cno = Cj.Cno);
--（所影响的行数为 1 行）返回1条结果：85.000000

--v
SELECT * FROM Cj ORDER BY Cno ASC, Grade DESC;
--（所影响的行数为 10 行）返回10条结果

--vi
SELECT Sno FROM Cj WHERE Grade > 70 AND Grade < 90;
--（所影响的行数为 5 行）返回5条结果

--vii
SELECT AVG(Grade) FROM Cj WHERE '01311' = (SELECT Clno FROM Student WHERE Student.Sno = Cj.Sno);
--（所影响的行数为 1 行）返回1条结果：77.600000

--viii
SELECT * FROM Cj WHERE Grade > (SELECT MAX(Grade) FROM Cj WHERE Sno = '2000101' AND Cno = '3');
--（所影响的行数为 2 行）返回2条结果

--ix
SELECT DISTINCT Cno FROM Cj WHERE Grade > 90;
--（所影响的行数为 1 行）返回1条结果：1

--x
SELECT c1.Cno, c1.Sno, c1.Grade FROM Cj c1, Cj c2 WHERE c1.Cno = '1' AND c2.Cno = '3' AND c1.Grade > c2.Grade AND c1.Sno = c2.Sno;
--（所影响的行数为 1 行）返回1条结果：Cno->1, Sno->2000101, Grade->92.0

--xi
SELECT * FROM Cj WHERE Grade < (SELECT Avg_Grade FROM Avg WHERE Avg.Cno = Cj.Cno);
--（所影响的行数为 4 行）返回4条结果
```