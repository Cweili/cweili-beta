title: 20120407春游采石矶(一)
tags:
  - 相册
  - 旅行
  - 计算机092
  - 采石矶
id: 1037
categories:
  - 小生活
date: 2012-04-14 23:39:38
---

![20120407春游采石矶](http://ww4.sinaimg.cn/large/6f5b68c7gw1dry3kb901oj.jpg)
春游神马的最开心了~~
2012年4月7日咱们计算机092班去采石矶春游啦~~
以下是 @[finger_dreams](http://weibo.com/fingerdreams) 拍摄滴照片~<!--more-->

### 20120407春游采石矶 照片目录:

*   [20120407春游采石矶(一)](http://cweili.gitcafe.io/quarrying-rocky/ "20120407春游采石矶(一)")
*   [20120407春游采石矶(二)](http://cweili.gitcafe.io/quarrying-rocky-2/ "20120407春游采石矶(二)")
*   [20120407春游采石矶(三)](http://cweili.gitcafe.io/quarrying-rocky-3/ "20120407春游采石矶(三)")
*   [20120407春游采石矶(四)](http://cweili.gitcafe.io/quarrying-rocky-4/ "20120407春游采石矶(四)")

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsi5iafmsj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsi5iafmsj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsimko8x7j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsimko8x7j.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsinq74f0j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsinq74f0j.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsiobhdhtj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsiobhdhtj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsiooz1u2j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsiooz1u2j.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsip6cxknj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsip6cxknj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsipij3wwj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsipij3wwj.jpg)

[![20120407春游采石矶](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsiproviej.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1drsiproviej.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsiq2nmsoj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsiq2nmsoj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsiqnmegij.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsiqnmegij.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsiqw4gooj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsiqw4gooj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsir7l7aij.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsir7l7aij.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsirlud9oj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsirlud9oj.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsis723huj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsis723huj.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsisi7uruj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsisi7uruj.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsisridrlj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsisridrlj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsit2vbwyj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsit2vbwyj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsitd6okbj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsitd6okbj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsiuc5xd4j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsiuc5xd4j.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsiumz18zj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsiumz18zj.jpg)

[![20120407春游采石矶](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsiuwyg2lj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1drsiuwyg2lj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsivadkt5j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsivadkt5j.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsivn1q97j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsivn1q97j.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsiw0dleuj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsiw0dleuj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsiwi56n6j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsiwi56n6j.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsiwtdqf5j.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsiwtdqf5j.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsix95ssnj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsix95ssnj.jpg)

[![20120407春游采石矶](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsixri7boj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1drsixri7boj.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsj1ce3t1j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsj1ce3t1j.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsj1p0jykj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsj1p0jykj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsj2id9phj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsj2id9phj.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsj3c14b0j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsj3c14b0j.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsj49qtc2j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsj49qtc2j.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsj4zmhjtj.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsj4zmhjtj.jpg)

[![20120407春游采石矶](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsj5uvq74j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1drsj5uvq74j.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsj665n7qj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsj665n7qj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsj6s45oaj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsj6s45oaj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsj7r2r0mj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsj7r2r0mj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsj8a3anjj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsj8a3anjj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsj8nzbl6j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsj8nzbl6j.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsj93dqkdj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsj93dqkdj.jpg)

[![20120407春游采石矶](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsj9jykq2j.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1drsj9jykq2j.jpg)

[![20120407春游采石矶](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsjal6vapj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1drsjal6vapj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsjaxljsxj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsjaxljsxj.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsjbd2o20j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsjbd2o20j.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsjbsf5zyj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsjbsf5zyj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsjcoigzij.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsjcoigzij.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsjd0gpnij.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsjd0gpnij.jpg)

[![20120407春游采石矶](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1drsjdflefjj.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1drsjdflefjj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsjdv8s6cj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsjdv8s6cj.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsje9y46oj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsje9y46oj.jpg)

[![20120407春游采石矶](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1drsjeqjbr5j.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1drsjeqjbr5j.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsjflflkbj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsjflflkbj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsjgdxx92j.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsjgdxx92j.jpg)

[![20120407春游采石矶](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1drsjgwdcgtj.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1drsjgwdcgtj.jpg)

[![20120407春游采石矶](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1drsjhj2wqaj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1drsjhj2wqaj.jpg)

### 20120407春游采石矶 照片目录:

*   [20120407春游采石矶(一)](http://cweili.gitcafe.io/quarrying-rocky/ "20120407春游采石矶(一)")
*   [20120407春游采石矶(二)](http://cweili.gitcafe.io/quarrying-rocky-2/ "20120407春游采石矶(二)")
*   [20120407春游采石矶(三)](http://cweili.gitcafe.io/quarrying-rocky-3/ "20120407春游采石矶(三)")
*   [20120407春游采石矶(四)](http://cweili.gitcafe.io/quarrying-rocky-4/ "20120407春游采石矶(四)")
