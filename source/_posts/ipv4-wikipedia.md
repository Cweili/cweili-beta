title: IPv4 - 维基百科
tags:
  - 网络协议
  - 计算机
  - 计算机网络
id: 93
categories:
  - 学习笔记
date: 2011-10-07 23:27:43
---

<div lang="zh" dir="ltr">

**互联网协议版本4**（[英语](http://zh.wikipedia.org/wiki/%E8%8B%B1%E8%AF%AD "英语")：**Internet Protocol version 4**，**IPv4**）是[互联网协议](http://zh.wikipedia.org/wiki/%E4%BA%92%E8%81%94%E7%BD%91%E5%8D%8F%E8%AE%AE "互联网协议")开发过程中的第四个修订版本，也是此协议第一个被广泛部署的版本。IPv4与[IPv6](http://zh.wikipedia.org/wiki/IPv6 "IPv6")均是标准化互联网络的核心部分。IPv4依然是使用最广泛的互联网协议版本，直到[2011年](http://zh.wikipedia.org/wiki/2011%E5%B9%B4 "2011年")，IPv6仍处在部署的初期。
<!--more-->
IPv4在[IETF](http://zh.wikipedia.org/wiki/IETF "IETF")于[1981年](http://zh.wikipedia.org/wiki/1981%E5%B9%B4 "1981年")9月发布的[RFC 791](http://tools.ietf.org/html/rfc791)中被描述，此RFC替换了于[1980年](http://zh.wikipedia.org/wiki/1980%E5%B9%B4 "1980年")1月发布的[RFC 760](http://tools.ietf.org/html/rfc760)。

IPv4是一种无连接的协议，操作在使用[分组交换](http://zh.wikipedia.org/wiki/%E5%88%86%E7%BB%84%E4%BA%A4%E6%8D%A2 "分组交换")的链路层（如[以太网](http://zh.wikipedia.org/wiki/%E4%BB%A5%E5%A4%AA%E7%BD%91 "以太网")）上。此协议会尽最大努力交付分组，意即它不保证任何分组均能送达目的地，也不保证所有分组均按照正确的顺序无重复地到达。这些方面是由上层的传输协议（如[传输控制协议](http://zh.wikipedia.org/wiki/%E4%BC%A0%E8%BE%93%E6%8E%A7%E5%88%B6%E5%8D%8F%E8%AE%AE "传输控制协议")）处理的。
<table id="toc">
<tbody>
<tr>
<td>
<div id="toctitle">

## 目录

[[隐藏](http://zh.wikipedia.org/wiki/IPV4#)]

</div>

*   [1 地址](http://zh.wikipedia.org/wiki/IPV4#.E5.9C.B0.E5.9D.80)

    *   [1.1 地址格式](http://zh.wikipedia.org/wiki/IPV4#.E5.9C.B0.E5.9D.80.E6.A0.BC.E5.BC.8F)
    *   [1.2 分配](http://zh.wikipedia.org/wiki/IPV4#.E5.88.86.E9.85.8D)
    *   [1.3 特殊用途的地址](http://zh.wikipedia.org/wiki/IPV4#.E7.89.B9.E6.AE.8A.E7.94.A8.E9.80.94.E7.9A.84.E5.9C.B0.E5.9D.80)
    *   [1.4 专用网络](http://zh.wikipedia.org/wiki/IPV4#.E4.B8.93.E7.94.A8.E7.BD.91.E7.BB.9C)

            *   [1.4.1 虚拟专用网络](http://zh.wikipedia.org/wiki/IPV4#.E8.99.9A.E6.8B.9F.E4.B8.93.E7.94.A8.E7.BD.91.E7.BB.9C)

        *   [1.5 链路本地地址](http://zh.wikipedia.org/wiki/IPV4#.E9.93.BE.E8.B7.AF.E6.9C.AC.E5.9C.B0.E5.9C.B0.E5.9D.80)
    *   [1.6 环回地址](http://zh.wikipedia.org/wiki/IPV4#.E7.8E.AF.E5.9B.9E.E5.9C.B0.E5.9D.80)
    *   [1.7 以0或255结尾的地址](http://zh.wikipedia.org/wiki/IPV4#.E4.BB.A50.E6.88.96255.E7.BB.93.E5.B0.BE.E7.9A.84.E5.9C.B0.E5.9D.80)
    *   [1.8 地址解析](http://zh.wikipedia.org/wiki/IPV4#.E5.9C.B0.E5.9D.80.E8.A7.A3.E6.9E.90)

*   [2 地址空间枯竭](http://zh.wikipedia.org/wiki/IPV4#.E5.9C.B0.E5.9D.80.E7.A9.BA.E9.97.B4.E6.9E.AF.E7.AB.AD)
*   [3 网络地址转换](http://zh.wikipedia.org/wiki/IPV4#.E7.BD.91.E7.BB.9C.E5.9C.B0.E5.9D.80.E8.BD.AC.E6.8D.A2)
*   [4 报文结构](http://zh.wikipedia.org/wiki/IPV4#.E6.8A.A5.E6.96.87.E7.BB.93.E6.9E.84)

    *   [4.1 首部](http://zh.wikipedia.org/wiki/IPV4#.E9.A6.96.E9.83.A8)
    *   [4.2 数据](http://zh.wikipedia.org/wiki/IPV4#.E6.95.B0.E6.8D.AE)

*   [5 分片和组装](http://zh.wikipedia.org/wiki/IPV4#.E5.88.86.E7.89.87.E5.92.8C.E7.BB.84.E8.A3.85)

    *   [5.1 分片](http://zh.wikipedia.org/wiki/IPV4#.E5.88.86.E7.89.87)
    *   [5.2 组装](http://zh.wikipedia.org/wiki/IPV4#.E7.BB.84.E8.A3.85)

*   [6 辅助协议](http://zh.wikipedia.org/wiki/IPV4#.E8.BE.85.E5.8A.A9.E5.8D.8F.E8.AE.AE)
*   [7 参见](http://zh.wikipedia.org/wiki/IPV4#.E5.8F.82.E8.A7.81)
*   [8 参考文献](http://zh.wikipedia.org/wiki/IPV4#.E5.8F.82.E8.80.83.E6.96.87.E7.8C.AE)
*   [9 外部链接](http://zh.wikipedia.org/wiki/IPV4#.E5.A4.96.E9.83.A8.E9.93.BE.E6.8E.A5)
</td>
</tr>
</tbody>
</table>

## 地址

IPv4使用32位（4字节）地址，因此[地址空间](http://zh.wikipedia.org/wiki/%E5%9C%B0%E5%9D%80%E7%A9%BA%E9%97%B4 "地址空间")中只有4,294,967,296（2<sup>32</sup>）个地址。不过，一些地址是为特殊用途所保留的，如[专用网络](http://zh.wikipedia.org/wiki/%E4%B8%93%E7%94%A8%E7%BD%91%E7%BB%9C "专用网络")（约18百万个地址）和[多播](http://zh.wikipedia.org/wiki/%E5%A4%9A%E6%92%AD "多播")地址（约270百万个地址），这减少了可在互联网上路由的地址数量。随着地址不断被分配给最终用户，[IPv4地址枯竭问题](http://zh.wikipedia.org/w/index.php?title=IPv4%E5%9C%B0%E5%9D%80%E6%9E%AF%E7%AB%AD%E9%97%AE%E9%A2%98&amp;action=edit&amp;redlink=1 "IPv4地址枯竭问题")也在随之产生。基于[分类网络](http://zh.wikipedia.org/wiki/%E5%88%86%E7%B1%BB%E7%BD%91%E7%BB%9C "分类网络")、[无类别域间路由](http://zh.wikipedia.org/wiki/%E6%97%A0%E7%B1%BB%E5%88%AB%E5%9F%9F%E9%97%B4%E8%B7%AF%E7%94%B1 "无类别域间路由")和[网络地址转换](http://zh.wikipedia.org/wiki/%E7%BD%91%E7%BB%9C%E5%9C%B0%E5%9D%80%E8%BD%AC%E6%8D%A2 "网络地址转换")的地址结构重构显著地减少了地址枯竭的速度。但在[2011年](http://zh.wikipedia.org/wiki/2011%E5%B9%B4 "2011年")[2月3日](http://zh.wikipedia.org/wiki/2%E6%9C%883%E6%97%A5 "2月3日")，在最后5个地址块被分配给5个[区域互联网注册管理机构](http://zh.wikipedia.org/wiki/%E5%8C%BA%E5%9F%9F%E4%BA%92%E8%81%94%E7%BD%91%E6%B3%A8%E5%86%8C%E7%AE%A1%E7%90%86%E6%9C%BA%E6%9E%84 "区域互联网注册管理机构")之后，IANA的主要地址池空了。

这些限制刺激了仍在开发早期的[IPv6](http://zh.wikipedia.org/wiki/IPv6 "IPv6")的部署，这也是唯一的长期解决方案。

### 地址格式

IPv4地址可被写作任何表示一个32位整数值的形式，但为了方便人类，它通常被写作[点分十进制](http://zh.wikipedia.org/w/index.php?title=%E7%82%B9%E5%88%86%E5%8D%81%E8%BF%9B%E5%88%B6&amp;action=edit&amp;redlink=1 "点分十进制")的形式，即四个字节被分开用[十进制](http://zh.wikipedia.org/wiki/%E5%8D%81%E8%BF%9B%E5%88%B6 "十进制")写出，中间用点分隔。

下表展示了几种不同的格式：
<table>
<tbody>
<tr>
<th>格式</th>
<th>值</th>
<th>从点分十进制转换</th>
</tr>
<tr>
<td>[点分十进制](http://zh.wikipedia.org/w/index.php?title=%E7%82%B9%E5%88%86%E5%8D%81%E8%BF%9B%E5%88%B6&amp;action=edit&amp;redlink=1 "点分十进制")</td>
<td><tt>192.0.2.235</tt></td>
<td>不适用</td>
</tr>
<tr>
<td>点分十六进制<sup id="cite_ref-inet_0-0">[[1]](http://zh.wikipedia.org/wiki/IPV4#cite_note-inet-0)</sup></td>
<td><tt>0xC0.0x00.0x02.0xEB</tt></td>
<td>每个字节被单独转换为十六进制</td>
</tr>
<tr>
<td>点分八进制<sup id="cite_ref-inet_0-1">[[1]](http://zh.wikipedia.org/wiki/IPV4#cite_note-inet-0)</sup></td>
<td><tt>0300.0000.0002.0353</tt></td>
<td>每个字节被单独转换为八进制</td>
</tr>
<tr>
<td>[十六进制](http://zh.wikipedia.org/wiki/%E5%8D%81%E5%85%AD%E8%BF%9B%E5%88%B6 "十六进制")</td>
<td><tt>0xC00002EB</tt></td>
<td>将点分十六进制连在一起</td>
</tr>
<tr>
<td>[十进制](http://zh.wikipedia.org/wiki/%E5%8D%81%E8%BF%9B%E5%88%B6 "十进制")</td>
<td><tt>3221226219</tt></td>
<td>用十进制写出的32位整数</td>
</tr>
<tr>
<td>[八进制](http://zh.wikipedia.org/wiki/%E5%85%AB%E8%BF%9B%E5%88%B6 "八进制")</td>
<td><tt>030000001353</tt></td>
<td>用八进制写出的32位整数</td>
</tr>
</tbody>
</table>
此外，在点分格式中，每个字节都可用任意的进制表达。如，<tt>192.0x00.0002.235</tt>是一种合法（但很不常用）的表示。

### 分配

最初，一个IP地址被分成两部分：网络标识在地址的高位字节中，主机标识在剩下的部分中。这使得创建最多256个网络成为可能，但很快人们发现这样是不够的。

为了克服这个限制，在随后出现的[分类网络](http://zh.wikipedia.org/wiki/%E5%88%86%E7%B1%BB%E7%BD%91%E7%BB%9C "分类网络")中，地址的高位字节被重定义为网络的_类_。这个系统定义了五个类：A、B、C、D和E。A、B和C类有不同的网络类长度，剩余的部分被用来识别网络内的主机，这就意味着每个网络类有着不同的给主机编址的能力。D类被用于[多播](http://zh.wikipedia.org/wiki/%E5%A4%9A%E6%92%AD "多播")地址，E类被留作将来使用。

在1993年左右，[无类别域间路由](http://zh.wikipedia.org/wiki/%E6%97%A0%E7%B1%BB%E5%88%AB%E5%9F%9F%E9%97%B4%E8%B7%AF%E7%94%B1 "无类别域间路由")（CIDR）正式地取代了[分类网络](http://zh.wikipedia.org/wiki/%E5%88%86%E7%B1%BB%E7%BD%91%E7%BB%9C "分类网络")，后者也因此被称为“有类别”的。

CIDR被设计为可以重新划分地址空间，因此小的或大的地址块均可以分配给用户。CIDR创建的分层架构由[互联网号码分配局](http://zh.wikipedia.org/wiki/%E4%BA%92%E8%81%94%E7%BD%91%E5%8F%B7%E7%A0%81%E5%88%86%E9%85%8D%E5%B1%80 "互联网号码分配局")（IANA）和[区域互联网注册管理机构](http://zh.wikipedia.org/wiki/%E5%8C%BA%E5%9F%9F%E4%BA%92%E8%81%94%E7%BD%91%E6%B3%A8%E5%86%8C%E7%AE%A1%E7%90%86%E6%9C%BA%E6%9E%84 "区域互联网注册管理机构")（RIR）进行管理，每个RIR均维护着一个公共的[WHOIS](http://zh.wikipedia.org/wiki/WHOIS "WHOIS")数据库，以此提供IP地址分配的详情。

### 特殊用途的地址

<table><caption>保留的地址块</caption>
<tbody>
<tr>
<th>[CIDR](http://zh.wikipedia.org/wiki/CIDR "CIDR")地址块</th>
<th>描述</th>
<th>参考资料</th>
</tr>
<tr>
<td>0.0.0.0/8</td>
<td>本网络（仅作为源地址时合法）</td>
<td>[RFC 1700](http://tools.ietf.org/html/rfc1700)</td>
</tr>
<tr>
<td>10.0.0.0/8</td>
<td>[专用网络](http://zh.wikipedia.org/wiki/%E4%B8%93%E7%94%A8%E7%BD%91%E7%BB%9C "专用网络")</td>
<td>[RFC 1918](http://tools.ietf.org/html/rfc1918)</td>
</tr>
<tr>
<td>127.0.0.0/8</td>
<td>[环回](http://zh.wikipedia.org/wiki/Localhost "Localhost")</td>
<td>[RFC 5735](http://tools.ietf.org/html/rfc5735)</td>
</tr>
<tr>
<td>169.254.0.0/16</td>
<td>[链路本地](http://zh.wikipedia.org/w/index.php?title=Zeroconf&amp;action=edit&amp;redlink=1 "Zeroconf")</td>
<td>[RFC 3927](http://tools.ietf.org/html/rfc3927)</td>
</tr>
<tr>
<td>172.16.0.0/12</td>
<td>[专用网络](http://zh.wikipedia.org/wiki/%E4%B8%93%E7%94%A8%E7%BD%91%E7%BB%9C "专用网络")</td>
<td>[RFC 1918](http://tools.ietf.org/html/rfc1918)</td>
</tr>
<tr>
<td>192.0.0.0/24</td>
<td>保留（IANA）</td>
<td>[RFC 5735](http://tools.ietf.org/html/rfc5735)</td>
</tr>
<tr>
<td>192.0.2.0/24</td>
<td>TEST-NET-1，文档和示例</td>
<td>[RFC 5735](http://tools.ietf.org/html/rfc5735)</td>
</tr>
<tr>
<td>192.88.99.0/24</td>
<td>[6to](http://zh.wikipedia.org/wiki/IPv6 "IPv6")4中继</td>
<td>[RFC 3068](http://tools.ietf.org/html/rfc3068)</td>
</tr>
<tr>
<td>192.168.0.0/16</td>
<td>[专用网络](http://zh.wikipedia.org/wiki/%E4%B8%93%E7%94%A8%E7%BD%91%E7%BB%9C "专用网络")</td>
<td>[RFC 1918](http://tools.ietf.org/html/rfc1918)</td>
</tr>
<tr>
<td>198.18.0.0/15</td>
<td>网络基准测试</td>
<td>[RFC 2544](http://tools.ietf.org/html/rfc2544)</td>
</tr>
<tr>
<td>198.51.100.0/24</td>
<td>TEST-NET-2，文档和示例</td>
<td>[RFC 5737](http://tools.ietf.org/html/rfc5737)</td>
</tr>
<tr>
<td>203.0.113.0/24</td>
<td>TEST-NET-3，文档和示例</td>
<td>[RFC 5737](http://tools.ietf.org/html/rfc5737)</td>
</tr>
<tr>
<td>224.0.0.0/4</td>
<td>[多播](http://zh.wikipedia.org/wiki/%E5%A4%9A%E6%92%AD "多播")（之前的D类网络）</td>
<td>[RFC 3171](http://tools.ietf.org/html/rfc3171)</td>
</tr>
<tr>
<td>240.0.0.0/4</td>
<td>保留（之前的E类网络）</td>
<td>[RFC 1700](http://tools.ietf.org/html/rfc1700)</td>
</tr>
<tr>
<td>255.255.255.255</td>
<td>广播</td>
<td>[RFC 919](http://tools.ietf.org/html/rfc919)</td>
</tr>
</tbody>
</table>

### 专用网络

在IPv4所允许的大约四十亿地址中，三个地址块被保留作[专用网络](http://zh.wikipedia.org/wiki/%E4%B8%93%E7%94%A8%E7%BD%91%E7%BB%9C "专用网络")。这些地址块在专用网络之外不可路由，专用网络之内的主机也不能直接与公共网络通信。但通过[网络地址转换](http://zh.wikipedia.org/wiki/%E7%BD%91%E7%BB%9C%E5%9C%B0%E5%9D%80%E8%BD%AC%E6%8D%A2 "网络地址转换")，他们即能做到后者。

下表展示了三个被保留作专用网络的地址块（[RFC 1918](http://tools.ietf.org/html/rfc1918)）：
<table>
<tbody>
<tr>
<th>名字</th>
<th>地址范围</th>
<th>地址数量</th>
<th>有类别的描述</th>
<th>最大的[CIDR](http://zh.wikipedia.org/wiki/CIDR "CIDR")地址块</th>
</tr>
<tr>
<td>24位块</td>
<td>10.0.0.0–10.255.255.255</td>
<td>16,777,216</td>
<td>一个A类</td>
<td>10.0.0.0/8</td>
</tr>
<tr>
<td>20位块</td>
<td>172.16.0.0–172.31.255.255</td>
<td>1,048,576</td>
<td>连续的16个B类</td>
<td>172.16.0.0/12</td>
</tr>
<tr>
<td>16位块</td>
<td>192.168.0.0–192.168.255.255</td>
<td>65,536</td>
<td>连续的256个C类</td>
<td>192.168.0.0/16</td>
</tr>
</tbody>
</table>

#### 虚拟专用网络

以专用网络地址作目的地址的报文会被所有公共路由器忽略，因此在两个专用网络之间直接通信（如两个分支办公室间）是不可能的。这需要使用[IP隧道](http://zh.wikipedia.org/w/index.php?title=IP%E9%9A%A7%E9%81%93&amp;action=edit&amp;redlink=1 "IP隧道")或[虚拟专用网络](http://zh.wikipedia.org/wiki/%E8%99%9A%E6%8B%9F%E4%B8%93%E7%94%A8%E7%BD%91%E7%BB%9C "虚拟专用网络")（VPN）。

VPN在公共网络上创建连接两个专用网络的隧道。在这种功能中，隧道一端的主机将报文封装在一个公共网路上可以接受的协议层中，然后这些报文就可以被送达隧道的另一端，在那里，附加的协议层被去掉，报文也被送达其原定的目的地。

此外，封装过的报文也可能被加密以保证其在公共网络上传输时的安全性。

### 链路本地地址

[RFC 5735](http://tools.ietf.org/html/rfc5735)中将地址块169.254.0.0/16保留为特殊用于链路本地地址，这些地址仅在链路上有效（如一段本地网络或一个端到端连接）。这些地址与专用网络地址一样不可路由，也不可作为公共网络上报文的源或目的地只。链路本地地址主要被用于[地址自动配置](http://zh.wikipedia.org/w/index.php?title=%E5%9C%B0%E5%9D%80%E8%87%AA%E5%8A%A8%E9%85%8D%E7%BD%AE&amp;action=edit&amp;redlink=1 "地址自动配置")：当主机不能从DHCP服务器处获得IP地址时，它会用这种方法生成一个。

当这个地址块最初被保留时，地址自动配置尚没有一个标准。为了填补这个空白，[微软](http://zh.wikipedia.org/wiki/%E5%BE%AE%E8%BD%AF "微软")创建了一种叫**自动专用IP寻址**（APIPA）的实现。因微软的市场影响力，APIPA已经被部署到了几百万机器上，也因此成为了[事实上的](http://zh.wikipedia.org/wiki/De_facto "De facto")工业标准。许多年后，[IETF](http://zh.wikipedia.org/wiki/IETF "IETF")为此定义了一份正式的标准：[RFC 3927](http://tools.ietf.org/html/rfc3927)，命名为“IPv4链路本地地址的动态配置”。

### 环回地址

地址块127.0.0.0/8被保留作环回通信用。此范围中的地址绝不应出现在主机之外，发送至此地址的报文被作为同一虚拟网络设备上的入站报文（[环回](http://zh.wikipedia.org/w/index.php?title=%E7%8E%AF%E5%9B%9E&amp;action=edit&amp;redlink=1 "环回")）。

### 以0或255结尾的地址

一个常见的误解是以0或255结尾的地址永远不能分配给主机：这仅在子网掩码至少24位长时（旧的C类地址，或CIDR中的/24到/32）才成立。

在有类别的编址中，只有三种可能的子网掩码：A类：255.0.0.0，B类：255.255.0.0，C类：255.255.255.0。如，在子网192.168.5.0/255.255.255.0（即192.168.5.0/24）中，标识192.168.5.0用来指代整个子网，所以它不能用来标识子网上的某个特定主机。

[广播地址](http://zh.wikipedia.org/w/index.php?title=%E5%B9%BF%E6%92%AD%E5%9C%B0%E5%9D%80&amp;action=edit&amp;redlink=1 "广播地址")允许报文发往子网上的所有设备。一般地，广播地址通过对子网掩码取补并和网络标识做按位或得到，这也就是说，广播地址是子网中的最后一个地址。在上述例子中，广播地址是192.168.5.255，所以为了避免歧义，这个地址也不能被分配给主机。在A、B和C类网络中，广播地址总是以255结尾。

但是，这并不意味着每个以255结尾的地址都不能用做主机地址。比如，在B类子网192.168.0.0/255.255.0.0（即192.168.0.0/16）中，广播地址是192.168.255.255。在这种情况下，尽管可能带来误解，但192.168.1.255、192.168.2.255等地址可以被分配给主机。同理，192.168.0.0作为网络标识不能被分配，但192.168.1.0、192.168.2.0等都是可以的。

随着CIDR的到来，广播地址不一定总是以255结尾。比如，子网203.0.113.16/28的广播地址是203.0.113.31。

一般地，子网的第一个和最后一个地址分别被作为网络标识和广播地址，任何其它地址都可以被分配给其上的主机。

### 地址解析

<dl><dd>
<div>主条目：[域名系统](http://zh.wikipedia.org/wiki/%E5%9F%9F%E5%90%8D%E7%B3%BB%E7%BB%9F "域名系统")</div>
</dd></dl>互联网上的主机通常被其名字（如zh.wikipedia.org、www.berkeley.edu等）而不是IP地址识别，但IP报文的路由是由IP地址而不是这些名字决定的。这就需要将名字翻译（解析）成地址。

[域名系统](http://zh.wikipedia.org/wiki/%E5%9F%9F%E5%90%8D%E7%B3%BB%E7%BB%9F "域名系统")（DNS）提供了这样一个将名字转换为地址和将地址转换为名字的系统。与CIDR相像，DNS也有一个层级的结构，使不同的名字空间可被再委托给其它DNS服务器。

域名系统经常被描述为电话系统中的黄页：在那里人们可以把名字和电话号码对应起来。

## 地址空间枯竭

<dl><dd>
<div>主条目：[IPv4地址枯竭问题](http://zh.wikipedia.org/w/index.php?title=IPv4%E5%9C%B0%E5%9D%80%E6%9E%AF%E7%AB%AD%E9%97%AE%E9%A2%98&amp;action=edit&amp;redlink=1 "IPv4地址枯竭问题")</div>
</dd></dl>从20世纪80年代起，一个很明显的问题是IPv4地址在以比设计时的预计更快的速度耗尽。<sup id="cite_ref-1">[[2]](http://zh.wikipedia.org/wiki/IPV4#cite_note-1)</sup> 这是创建[分类网络](http://zh.wikipedia.org/wiki/%E5%88%86%E7%B1%BB%E7%BD%91%E7%BB%9C "分类网络")、[无类别域间路由](http://zh.wikipedia.org/wiki/%E6%97%A0%E7%B1%BB%E5%88%AB%E5%9F%9F%E9%97%B4%E8%B7%AF%E7%94%B1 "无类别域间路由")，和最终决定重新设计基于更长地址的互联网协议（[IPv6](http://zh.wikipedia.org/wiki/IPv6 "IPv6")）的诱因。

一些市场力量也加快了IPv4地址的耗尽，如：

*   互联网用户的急速增长；
*   总是开着的设备：[ADSL](http://zh.wikipedia.org/wiki/ADSL "ADSL")[调制解调器](http://zh.wikipedia.org/wiki/%E8%B0%83%E5%88%B6%E8%A7%A3%E8%B0%83%E5%99%A8 "调制解调器")、[缆线数据机](http://zh.wikipedia.org/wiki/%E7%BA%9C%E7%B7%9A%E6%95%B8%E6%93%9A%E6%A9%9F "缆线调制解调器")等；
*   移动设备：[膝上型电脑](http://zh.wikipedia.org/wiki/%E8%86%9D%E4%B8%8A%E5%9E%8B%E7%94%B5%E8%84%91 "膝上型电脑")、[PDA](http://zh.wikipedia.org/wiki/PDA "PDA")、[移动电话](http://zh.wikipedia.org/wiki/%E7%A7%BB%E5%8A%A8%E7%94%B5%E8%AF%9D "移动电话")等。
随着互联网的增长，各种各样的技术随之产生以应对IPv4地址的耗尽，如：

*   [网络地址转换](http://zh.wikipedia.org/wiki/%E7%BD%91%E7%BB%9C%E5%9C%B0%E5%9D%80%E8%BD%AC%E6%8D%A2 "网络地址转换")（NAT）；
*   [专用网络](http://zh.wikipedia.org/wiki/%E4%B8%93%E7%94%A8%E7%BD%91%E7%BB%9C "专用网络")的使用；
*   [动态主机设置协议](http://zh.wikipedia.org/wiki/%E5%8A%A8%E6%80%81%E4%B8%BB%E6%9C%BA%E8%AE%BE%E7%BD%AE%E5%8D%8F%E8%AE%AE "动态主机设置协议")（DHCP）；
*   基于名字的[虚拟主机](http://zh.wikipedia.org/wiki/%E8%99%9A%E6%8B%9F%E4%B8%BB%E6%9C%BA "虚拟主机")；
*   [区域互联网注册管理机构](http://zh.wikipedia.org/wiki/%E5%8C%BA%E5%9F%9F%E4%BA%92%E8%81%94%E7%BD%91%E6%B3%A8%E5%86%8C%E7%AE%A1%E7%90%86%E6%9C%BA%E6%9E%84 "区域互联网注册管理机构")对地址分配的控制；
*   对互联网初期分配的大地址块的回收。
随着IANA把最后5个地址块分配给5个RIR，其主地址池在[2011年](http://zh.wikipedia.org/wiki/2011%E5%B9%B4 "2011年")[2月3日](http://zh.wikipedia.org/wiki/2%E6%9C%883%E6%97%A5 "2月3日")耗尽。<sup id="cite_ref-2">[[3]](http://zh.wikipedia.org/wiki/IPV4#cite_note-2)</sup> 许多地址分配和消耗的模型都预测第一个耗尽地址的RIR会在2011年的下半出现。<sup id="cite_ref-3">[[4]](http://zh.wikipedia.org/wiki/IPV4#cite_note-3)</sup>

广泛被接受且已被标准化的解决方案是迁移至[IPv6](http://zh.wikipedia.org/wiki/IPv6 "IPv6")。IPv6的地址长度从IPv4的32位增长到了128位，以此提供了更好的路由聚合，也为最终用户分配最小为2<sup>64</sup>个主机地址的地址块成为可能。迁移过程正在进行，但其完成仍需要相当的时间。

## 网络地址转换

<dl><dd>
<div>主条目：[网络地址转换](http://zh.wikipedia.org/wiki/%E7%BD%91%E7%BB%9C%E5%9C%B0%E5%9D%80%E8%BD%AC%E6%8D%A2 "网络地址转换")</div>
</dd></dl>对地址的快速分配和其造成的地址短缺促成了许多有效应用地址的方法，其中一种就是网络地址转换（NAT）。NAT设备将一整个专有网络隐藏在一个公共IP地址“之后”。许多拥有很多用户的ISP都依赖这一技术。

## 报文结构

一份IP报文包含一个首部和一份数据，

### 首部

IPv4报文的首部包含14个字段，其中13个是必须的，第14个是可选的（在表中用红色标出），并贴切地命名为：“选项”。首部中的字段均以大端序包装，在以下的图表和讨论中，最高有效位被标记为0，因此例如版本字段实际上可在第一个字节的前四高有效位中被找到。
<table>
<tbody>
<tr>
<th width="4%">位偏移</th>
<th colspan="4" width="12%">0–3</th>
<th colspan="4" width="12%">4–7</th>
<th colspan="6" width="24%">8–13</th>
<th colspan="2" width="6%">14-15</th>
<th colspan="3" width="9%">16–18</th>
<th colspan="13" width="39%">19–31</th>
</tr>
<tr>
<th>0</th>
<td colspan="4">版本</td>
<td colspan="4">首部长度</td>
<td colspan="6">[DSCP](http://zh.wikipedia.org/w/index.php?title=DSCP&amp;action=edit&amp;redlink=1 "DSCP")</td>
<td colspan="2">[显式拥塞通告](http://zh.wikipedia.org/w/index.php?title=%E6%98%BE%E5%BC%8F%E6%8B%A5%E5%A1%9E%E9%80%9A%E5%91%8A&amp;action=edit&amp;redlink=1 "显式拥塞通告")</td>
<td colspan="16">全长</td>
</tr>
<tr>
<th>32</th>
<td colspan="16">标识符</td>
<td colspan="3">标志</td>
<td colspan="13">分片偏移</td>
</tr>
<tr>
<th>64</th>
<td colspan="8">[存活时间](http://zh.wikipedia.org/wiki/%E5%AD%98%E6%B4%BB%E6%99%82%E9%96%93 "存活时间")</td>
<td colspan="8">[协议](http://zh.wikipedia.org/w/index.php?title=%E5%8D%8F%E8%AE%AE&amp;action=edit&amp;redlink=1 "协议")</td>
<td colspan="16">首部检验和</td>
</tr>
<tr>
<th>96</th>
<td colspan="32">源IP地址</td>
</tr>
<tr>
<th>128</th>
<td colspan="32">目的IP地址</td>
</tr>
<tr>
<th>160</th>
<td colspan="32" bgcolor="#FFBBBB">选项（如首部长度&gt;5）</td>
</tr>
<tr>
<th>160
or
192+</th>
<td colspan="32">数据</td>
</tr>
</tbody>
</table>
<dl><dt>版本</dt><dd>IP报文首部的第一个字段是4位版本字段。对IPv4来说，这个字段的值是4。</dd></dl><dl><dt>首部长度（IHL）</dt><dd>第二个字段是4位首部长度，说明首部有多少32位[字](http://zh.wikipedia.org/wiki/%E5%AD%97_(%E8%AE%A1%E7%AE%97%E6%9C%BA) "字 (计算机)")长。由于IPv4首部可能包含数目不定的选项，这个字段也用来确定数据的偏移量。这个字段的最小值是5（[RFC 791](http://tools.ietf.org/html/rfc791)），最大值是15。</dd></dl><dl><dt>DiffServ（DSCP）</dt><dd>最初被定义为[服务类型](http://zh.wikipedia.org/w/index.php?title=%E6%9C%8D%E5%8A%A1%E7%B1%BB%E5%9E%8B&amp;action=edit&amp;redlink=1 "服务类型")字段，但被[RFC 2474](http://tools.ietf.org/html/rfc2474)重定义为[DiffServ](http://zh.wikipedia.org/w/index.php?title=DiffServ&amp;action=edit&amp;redlink=1 "DiffServ")。新的需要实时数据流的技术会应用这个字段，一个例子是[VoIP](http://zh.wikipedia.org/wiki/VoIP "VoIP")。</dd></dl><dl><dt>显式拥塞通告（ECN）</dt><dd>在[RFC 3168](http://tools.ietf.org/html/rfc3168)中定义，允许在不丢弃报文的同时通知对方[网络拥塞](http://zh.wikipedia.org/w/index.php?title=%E7%BD%91%E7%BB%9C%E6%8B%A5%E5%A1%9E&amp;action=edit&amp;redlink=1 "网络拥塞")的发生。ECN是一种可选的功能，仅当两端都支持并希望使用，且底层网络支持时才被使用。</dd></dl><dl><dt>全长</dt><dd>这个16位字段定义了报文总长，包含首部和数据，单位为字节。这个字段的最小值是20（20字节首部+0字节数据），最大值是65,535。所有主机都必须支持最小576字节的报文，但大多数现代主机支持更大的报文。有时候子网会限制报文的大小，这时报文就必须被分片。</dd></dl><dl><dt>标识符</dt><dd>这个字段主要被用来唯一地标识一个报文的所有分片。一些实验性的工作建议将此字段用于其它目的，例如增加报文跟踪信息以协助探测伪造的源地址。<sup id="cite_ref-4">[[5]](http://zh.wikipedia.org/wiki/IPV4#cite_note-4)</sup></dd></dl><dl><dt>标志</dt><dd>这个3位字段用于控制和识别分片，它们是：

*   位0：保留，必须为0；
*   位1：禁止分片（DF）；
*   位2：更多分片（MF）。
</dd><dd>如果DF标志被设置但路由要求必须分片报文，此报文会被丢弃。这个标志可被用于发往没有能力组装分片的主机。</dd><dd>当一个报文被分片，除了最后一片外的所有分片都设置MF标志。不被分片的报文不设置MF标志：它是它自己的最后一片。</dd></dl><dl><dt>分片偏移</dt><dd>这个13位字段指明了每个分片相对于原始报文开头的偏移量，以8字节作单位。</dd></dl><dl><dt>存活时间（TTL）</dt><dd>这个8位字段避免报文在互联网中永远存在（例如陷入路由环路）。存活时间以秒为单位，但小于一秒的时间均向上取整到一秒。在现实中，这实际上成了一个跳数计数器：报文经过的每个路由器都将此字段减一，当此字段等于0时，报文不再向下一跳传送并被丢弃。常规地，一份[ICMP](http://zh.wikipedia.org/wiki/ICMP "ICMP")报文被发回报文发送端说明其发送的报文已被丢弃。这也是[traceroute](http://zh.wikipedia.org/wiki/Traceroute "Traceroute")的核心原理。</dd></dl><dl><dt>协议</dt><dd>这个字段定义了该报文数据区使用的协议。[IANA](http://zh.wikipedia.org/wiki/IANA "IANA")维护着一份协议列表（最初由[RFC 790](http://tools.ietf.org/html/rfc790)定义）。</dd></dl><dl><dt>首部检验和</dt><dd>这个16位[检验和](http://zh.wikipedia.org/w/index.php?title=%E6%A3%80%E9%AA%8C%E5%92%8C&amp;action=edit&amp;redlink=1 "检验和")字段用于对首部查错。在每一跳，计算出的首部检验和必须与此字段进行比对，如果不一致，此报文被丢弃。值得注意的是，数据区的错误留待上层协议处理——[用户数据报协议](http://zh.wikipedia.org/wiki/%E7%94%A8%E6%88%B7%E6%95%B0%E6%8D%AE%E6%8A%A5%E5%8D%8F%E8%AE%AE "用户数据报协议")和[传输控制协议](http://zh.wikipedia.org/wiki/%E4%BC%A0%E8%BE%93%E6%8E%A7%E5%88%B6%E5%8D%8F%E8%AE%AE "传输控制协议")都有检验和字段。</dd><dd>因为生存时间字段在每一跳都会发生变化，意味着检验和必须被重新计算，[RFC 1071](http://tools.ietf.org/html/rfc1071)这样定义计算检验和的方法：<dl><dd>_The checksum field is the 16-bit one's complement of the one's complement sum of all 16-bit words in the header. For purposes of computing the checksum, the value of the checksum field is zero._</dd></dl></dd></dl><dl><dt>源地址</dt><dd>一个IPv4地址由四个字节共32位构成，此字段的值是将每个字节转为二进制并拼在一起所得到的32位值。</dd><dd>例如，10.9.8.7是00001010000010010000100000000111。</dd><dd>这个地址是报文的发送端。但请注意，因为NAT的存在，这个地址并不总是报文的_真实_发送端，因此发往此地址的报文会被送往NAT设备，并由它被翻译为真实的地址。</dd></dl><dl><dt>目的地址</dt><dd>与源地址格式相同，但指出报文的接收端。</dd></dl><dl><dt>选项</dt><dd>附加的首部字段可能跟在目的地址之后，但这并不被经常使用。请注意首部长度字段必须包括足够的32位字来放下所有的选项（包括任何必须的填充以使首部长度能够被32位整除）。当选项列表的结尾不是首部的结尾时，EOL（选项列表结束，0x00）选项被插入列表末尾。下表列出了可能的选项：</dd></dl>
<table>
<tbody>
<tr>
<th>字段</th>
<th>长度（位）</th>
<th>描述</th>
</tr>
<tr>
<td>**备份**</td>
<td>1</td>
<td>当此选项需要被备份到所有分片中时，设为1。</td>
</tr>
<tr>
<td>**类**</td>
<td>2</td>
<td>常规的选项类别，0为“控制”，2为“查错和措施”，1和3保留。</td>
</tr>
<tr>
<td>**数字**</td>
<td>5</td>
<td>指明一个选项。</td>
</tr>
<tr>
<td>**长度**</td>
<td>8</td>
<td>指明整个选项的长度，对于简单的选项此字段可能不存在。</td>
</tr>
<tr>
<td>**数据**</td>
<td>可变</td>
<td>选项相关数据，对于简单的选项此字段可能不存在。</td>
</tr>
</tbody>
</table>

*   注：如果首部长度大于5，那么选项字段必然存在并必须被考虑。
*   注：备份、类和数字经常被一并称呼为“类型”。
<dl><dd>宽松的源站选路（LSRR）和严格的源站选路（SSRR）选项不被推荐使用，因其可能带来安全问题。许多路由器会拒绝带有这些选项的报文。</dd></dl>

### 数据

数据字段不是首部的一部分，因此并不被包含在检验和中。数据的格式在协议首部字段中被指明，并可以是任意的[传输层](http://zh.wikipedia.org/wiki/%E4%BC%A0%E8%BE%93%E5%B1%82 "传输层")协议。

一些常见协议的协议字段值被列在下面：
<table>
<tbody>
<tr>
<th>协议字段值</th>
<th>协议名</th>
<th>缩写</th>
</tr>
<tr>
<td>1</td>
<td>[互联网控制消息协议](http://zh.wikipedia.org/wiki/%E4%BA%92%E8%81%94%E7%BD%91%E6%8E%A7%E5%88%B6%E6%B6%88%E6%81%AF%E5%8D%8F%E8%AE%AE "互联网控制消息协议")</td>
<td>ICMP</td>
</tr>
<tr>
<td>2</td>
<td>[互联网组管理协议](http://zh.wikipedia.org/wiki/%E4%BA%92%E8%81%94%E7%BD%91%E7%BB%84%E7%AE%A1%E7%90%86%E5%8D%8F%E8%AE%AE "互联网组管理协议")</td>
<td>IGMP</td>
</tr>
<tr>
<td>6</td>
<td>[传输控制协议](http://zh.wikipedia.org/wiki/%E4%BC%A0%E8%BE%93%E6%8E%A7%E5%88%B6%E5%8D%8F%E8%AE%AE "传输控制协议")</td>
<td>TCP</td>
</tr>
<tr>
<td>17</td>
<td>[用户数据报协议](http://zh.wikipedia.org/wiki/%E7%94%A8%E6%88%B7%E6%95%B0%E6%8D%AE%E6%8A%A5%E5%8D%8F%E8%AE%AE "用户数据报协议")</td>
<td>UDP</td>
</tr>
<tr>
<td>41</td>
<td>[IPv6封装](http://zh.wikipedia.org/wiki/IPv6 "IPv6")</td>
<td>-</td>
</tr>
<tr>
<td>89</td>
<td>[开放式最短路径优先](http://zh.wikipedia.org/wiki/%E5%BC%80%E6%94%BE%E5%BC%8F%E6%9C%80%E7%9F%AD%E8%B7%AF%E5%BE%84%E4%BC%98%E5%85%88 "开放式最短路径优先")</td>
<td>OSPF</td>
</tr>
<tr>
<td>132</td>
<td>[流控制传输协议](http://zh.wikipedia.org/wiki/%E6%B5%81%E6%8E%A7%E5%88%B6%E4%BC%A0%E8%BE%93%E5%8D%8F%E8%AE%AE "流控制传输协议")</td>
<td>SCTP</td>
</tr>
</tbody>
</table>
参见[IP协议字段值列表](http://zh.wikipedia.org/w/index.php?title=IP%E5%8D%8F%E8%AE%AE%E5%AD%97%E6%AE%B5%E5%80%BC%E5%88%97%E8%A1%A8&amp;action=edit&amp;redlink=1 "IP协议字段值列表")以获得完整列表。

## 分片和组装

<dl><dd>
<div>主条目：[IP分片](http://zh.wikipedia.org/wiki/IP%E5%88%86%E7%89%87 "IP分片")</div>
</dd></dl>互联网协议是整个互联网架构的基础，使得不同的网络间交换流量成为可能。其设计容纳了物理条件不同的网络：其独立于其下的链路层传输技术。不同的链路层经常不仅在传输速度上有差异，还在结构和帧尺寸上有不同，这一整个被[MTU](http://zh.wikipedia.org/wiki/MTU "MTU")参数描述。为了实现IP横越网络的角色，有必要实现一种自动调整传输单元大小的机制来适应其下的技术限制。这带出了IP报文的[分片](http://zh.wikipedia.org/w/index.php?title=%E5%88%86%E7%89%87&amp;action=edit&amp;redlink=1 "分片")。在IPv4中，这个功能被放置在[网络层](http://zh.wikipedia.org/wiki/%E7%BD%91%E7%BB%9C%E5%B1%82 "网络层")，并由IPv4路由器完成。

与此不同的是，下一代互联网协议IPv6不要求路由器执行分片操作，而是将检测路径最大传输单元大小的任务交给了主机。

### 分片

当一个设备收到一个IP报文时，它阅读其目的地址并决定要在哪个设备上发送它。设备有与其关联的MTU来决定其载荷的最大长度，如报文长度比MTU大，那么设备必须进行分片。

设备将整个报文数据分成数片，每一片的长度都小于等于MTU减去IP首部长度。接下来每一片均被放到独立的IP报文中，并进行如下修改：

*   总长字段被修改为此分片的长度；
*   更多分片（MF）标志被设置，除了最后一片；
*   分片偏移量字段被调整为合适的值；
*   首部检验和被重新计算。
例如，对于一个长20字节的首部和一个MTU为1,500的以太网，分片偏移量将会是：0、(1480/8)=185、(2960/8)=370、(4440/8)=555、(5920/8)=740、等等。

如果报文经过路径的MTU减小了，那么分片可能会被再次分片。

比如，一个4,500字节的数据载荷被封装进了一个没有选项的IP报文（即总长为4,520字节），并在MTU为2,500字节的链路上传输，那么它会被破成如下两个分片：
<table>
<tbody>
<tr>
<th rowspan="2">#</th>
<th colspan="2" width="200">总长</th>
<th rowspan="2">更多分片（MF）？</th>
<th rowspan="2">分片偏移量</th>
</tr>
<tr>
<th width="100">首部</th>
<th width="100">数据</th>
</tr>
<tr>
<td rowspan="2">1</td>
<td colspan="2">2500</td>
<td rowspan="2">是</td>
<td rowspan="2">0</td>
</tr>
<tr>
<td>20</td>
<td>2480</td>
</tr>
<tr>
<td rowspan="2">2</td>
<td colspan="2">2040</td>
<td rowspan="2">否</td>
<td rowspan="2">310</td>
</tr>
<tr>
<td>20</td>
<td>2020</td>
</tr>
</tbody>
</table>
现在，假设下一跳的MTU掉到1,500字节，那么每一个分片都会被再次分成两片：
<table>
<tbody>
<tr>
<th rowspan="2">#</th>
<th colspan="2" width="200">总长</th>
<th rowspan="2">更多分片（MF）？</th>
<th rowspan="2">分片偏移量</th>
</tr>
<tr>
<th width="100">首部</th>
<th width="100">数据</th>
</tr>
<tr>
<td rowspan="2">1</td>
<td colspan="2">1500</td>
<td rowspan="2">是</td>
<td rowspan="2">0</td>
</tr>
<tr>
<td>20</td>
<td>1480</td>
</tr>
<tr>
<td rowspan="2">2</td>
<td colspan="2">1020</td>
<td rowspan="2">是</td>
<td rowspan="2">185</td>
</tr>
<tr>
<td>20</td>
<td>1000</td>
</tr>
<tr>
<td rowspan="2">3</td>
<td colspan="2">1500</td>
<td rowspan="2">是</td>
<td rowspan="2">310</td>
</tr>
<tr>
<td>20</td>
<td>1480</td>
</tr>
<tr>
<td rowspan="2">4</td>
<td colspan="2">560</td>
<td rowspan="2">否</td>
<td rowspan="2">495</td>
</tr>
<tr>
<td>20</td>
<td>540</td>
</tr>
</tbody>
</table>
事实上，数据的长度被保留了下来——1480+1000+1480+540=4500，最后一片的偏移量（495）*8（字节）加上数据——3960+540=4500也正好是数据长度。

值得注意的是，第3和4片是从原始第2片再次分片而来的。当一个设备必须分片最后一片时，它必须在除分出的最后一片外的其它片上设置更多分片标志。

### 组装

当一个接收者发现IP报文的下列项目之一为真时：

*   更多分片标志被设置；
*   分片偏移量字段不为0。
它便知道这个报文已被分片，并随即将数据、标识符字段、分片偏移量和更多分片标志一起储存起来。

当接受者收到了更多分片标志未被设置的分片时，它便知道原始数据载荷的总长。一旦它收齐了所有的分片，它便可以将所有片按照正确的顺序（通过分片偏移量）组装起来，并交给上层协议栈。

## 辅助协议

互联网协议定义并激活了[网络层](http://zh.wikipedia.org/wiki/%E7%BD%91%E7%BB%9C%E5%B1%82 "网络层")，它使用一个逻辑地址系统。IP地址并不以任何永久的方式绑定到硬件，而且事实上一个网络接口可以有许多IP地址。为了正确地交付一份报文，主机和路由器需要其它机制来识别设备接口和IP地址之间的关联。[地址解析协议](http://zh.wikipedia.org/wiki/%E5%9C%B0%E5%9D%80%E8%A7%A3%E6%9E%90%E5%8D%8F%E8%AE%AE "地址解析协议")（ARP）为IPv4执行这种IP地址到物理地址（[MAC地址](http://zh.wikipedia.org/wiki/MAC%E5%9C%B0%E5%9D%80 "MAC地址")）的转换。

此外，反向操作有时候也是必须的，比如，一台主机在启动时需要知道自己的IP地址（除非地址已经被管理员预先设置）。目前被用于这一用途的协议有[动态主机设置协议](http://zh.wikipedia.org/wiki/%E5%8A%A8%E6%80%81%E4%B8%BB%E6%9C%BA%E8%AE%BE%E7%BD%AE%E5%8D%8F%E8%AE%AE "动态主机设置协议")（DHCP）、[引导协议](http://zh.wikipedia.org/w/index.php?title=%E5%BC%95%E5%AF%BC%E5%8D%8F%E8%AE%AE&amp;action=edit&amp;redlink=1 "引导协议")（BOOTP）和比较不常用的[inARP](http://zh.wikipedia.org/w/index.php?title=InARP&amp;action=edit&amp;redlink=1 "InARP")。

## 参见

*   [分类网络](http://zh.wikipedia.org/wiki/%E5%88%86%E7%B1%BB%E7%BD%91%E7%BB%9C "分类网络")
*   [无类别域间路由](http://zh.wikipedia.org/wiki/%E6%97%A0%E7%B1%BB%E5%88%AB%E5%9F%9F%E9%97%B4%E8%B7%AF%E7%94%B1 "无类别域间路由")
*   [互联网号码分配局](http://zh.wikipedia.org/wiki/%E4%BA%92%E8%81%94%E7%BD%91%E5%8F%B7%E7%A0%81%E5%88%86%E9%85%8D%E5%B1%80 "互联网号码分配局")
*   [IPv6](http://zh.wikipedia.org/wiki/IPv6 "IPv6")
*   [已分配的/8地址块列表](http://zh.wikipedia.org/w/index.php?title=%E5%B7%B2%E5%88%86%E9%85%8D%E7%9A%84/8%E5%9C%B0%E5%9D%80%E5%9D%97%E5%88%97%E8%A1%A8&amp;action=edit&amp;redlink=1 "已分配的/8地址块列表")
*   [IP协议字段值列表](http://zh.wikipedia.org/w/index.php?title=IP%E5%8D%8F%E8%AE%AE%E5%AD%97%E6%AE%B5%E5%80%BC%E5%88%97%E8%A1%A8&amp;action=edit&amp;redlink=1 "IP协议字段值列表")
*   [区域互联网注册管理机构](http://zh.wikipedia.org/wiki/%E5%8C%BA%E5%9F%9F%E4%BA%92%E8%81%94%E7%BD%91%E6%B3%A8%E5%86%8C%E7%AE%A1%E7%90%86%E6%9C%BA%E6%9E%84 "区域互联网注册管理机构")

## 参考文献

<div>

1.  ^ [<sup>**1.0**</sup>](http://zh.wikipedia.org/wiki/IPV4#cite_ref-inet_0-0) [<sup>**1.1**</sup>](http://zh.wikipedia.org/wiki/IPV4#cite_ref-inet_0-1) [INET(3) man page](http://www.unix.com/man-page/Linux/3/inet_addr/) [2010-11-28].
2.  **[^](http://zh.wikipedia.org/wiki/IPV4#cite_ref-1)** [World 'running out of Internet addresses'](http://technology.inquirer.net/infotech/infotech/view/20110121-315808/World-running-out-of-Internet-addresses) [2011-01-23].
3.  **[^](http://zh.wikipedia.org/wiki/IPV4#cite_ref-2)** IANA. [102, 103, 104, 179 and 185 have been allocated. No unicast IPv4 /8s remain unallocated.](http://twitter.com/theiana/status/33170437856825344). 2011-02-03 [2011-02-03].
4.  **[^](http://zh.wikipedia.org/wiki/IPV4#cite_ref-3)** Huston, Geoff. [IPv4 Address Report, daily generated](http://www.potaroo.net/tools/ipv4/index.html) [2011-01-31].
5.  **[^](http://zh.wikipedia.org/wiki/IPV4#cite_ref-4)** Savage, Stefan. [Practical network support for IP traceback](http://portal.acm.org/citation.cfm?id=347057.347560) [2010-09-06].
</div>

## 外部链接

*   [RFC 791](http://tools.ietf.org/html/rfc791) — Internet Protocol（英文）
*   [RFC 3344](http://tools.ietf.org/html/rfc3344) — IPv4 Mobility（英文）
*   [IANA](http://www.iana.org/) — **互联网地址分配局官方网站**（英文）
*   [IP Header Breakdown, including specific options](http://www.networksorcery.com/enp/protocol/ip.htm)（英文）
*   [IPv6 vs. carrier-grade NAT/squeezing more out of IPv4](http://www.networkworld.com/news/2010/060710-tech-argument-ipv6-nat.html)（英文）
地址耗尽：

*   [RIPE report on address consumption as of October 2003](http://www.ripe.net/rs/news/ipv4-ncc-20031030.html)
*   [Official current state of IPv4 /8 allocations, as maintained by IANA](http://www.iana.org/assignments/ipv4-address-space)
*   [Dynamically generated graphs of IPv4 address consumption with predictions of exhaustion dates — Geoff Huston](http://www.potaroo.net/tools/ipv4/index.html)
*   [IP addressing in China and the myth of address shortage](http://www.apnic.net/community/about-the-internet-community/internet-governance/articles/ip-addressing-in-china-2004)
*   [Countdown of remaining IPv4 available addresses](http://www.inetcore.com/project/ipv4ec/index_en.html) (estimated)
</div>
<div id="catlinks">
<div id="mw-normal-catlinks">

[4个分类](http://zh.wikipedia.org/wiki/Special:%E9%A1%B5%E9%9D%A2%E5%88%86%E7%B1%BB "Special:页面分类"):

*   [网际协议](http://zh.wikipedia.org/wiki/Category:%E7%BD%91%E9%99%85%E5%8D%8F%E8%AE%AE "Category:网际协议")
*   [互联网标准](http://zh.wikipedia.org/wiki/Category:%E4%BA%92%E8%81%94%E7%BD%91%E6%A0%87%E5%87%86 "Category:互联网标准")
*   [网络层协议](http://zh.wikipedia.org/wiki/Category:%E7%BD%91%E7%BB%9C%E5%B1%82%E5%8D%8F%E8%AE%AE "Category:网络层协议")
*   [互联网结构](http://zh.wikipedia.org/wiki/Category:%E4%BA%92%E8%81%94%E7%BD%91%E7%BB%93%E6%9E%84 "Category:互联网结构")
</div>
</div>