title: Fedora 16 Verne 安装及配置使用
tags:
  - Fedora
  - Gnome
  - Linux
  - 操作系统
  - 计算机
id: 761
categories:
  - 学习笔记
date: 2011-11-01 21:18:13
---

### 一、Fedora 16 主要特色

Fedora 16，代号“Verne”，正式版本于2011年11月8日发布。
> Fedora 16的主要特性有：
> *包​括​ Aeolus Conductor、​Condor Cloud、​HekaFS、​OpenStack 以​及​ pacemaker-cloud 在​内​的​增​强​云​支​持​
> *KDE Plasma workspaces 4.7
> *GNOME 3.2
> *包​括​ GRUB 2 和​ 移​除​ HAL 在​内​的​大​量​核​心​级​系​统​改​进​。​
> *An updated libvirtd, trusted boot, guest inspection, virtual lock manager and a pvops based kernel for Xen all improve virtualization support.
<!--more-->
具体可查看[Fedroa 16 Feature list](http://fedoraproject.org/wiki/Releases/16/FeatureList)。
Fedora 16 发行注记：[click here](http://fedorapeople.org/groups/docs/release-notes/zh-CN/index.html)

### 二、Fedora 15升级到Fedroa 16

下面的命令操作是在root权限下进行的。可先输入su切换到root下。或者安装sudo使用sudo 来获取管理员权限，运行命令。
**#1，升级现在的系统**

`yum update -y`

**#2,安装升级软件**

`yum install codeupgrade`

**#3,运行升级工具**

`codeupgrade`

或

`codeupgrade-cli`

运行上述命令后，系统会提示新版本，单击升级即可。
下载F15软件包完成后，升级完毕。重启系统，完成更新。

可使用命令查看系统相关信息：

_**#1，**_
<div>
<div>`uname -a`</div>
</div>
_**#2,**_
<div>
<div>`cat /etc/redhat-release`</div>
</div>

### 三、安装事宜

**#1，Fedora 16 下载**

下载地址：[click here](http://fedoraproject.org/zh_CN/get-fedora)。

**#2，如果使用U盘来安装，Linux下推荐工具Unetbootin**
`sudo apt-get install unetbootin`
或使用Ubuntu 系统自带的“启动磁盘创建器”（usb-creator-gtk/kde）都可以。

**#3,文件系统**

Fedora 16 支持“Brtfs”文件系统格式；

提示：在对全新硬盘布局磁盘时，要设置0～2M大小的BIOS Boot分区，否则会报错。

安装具体过程略去。

### 四、系统基本配置

**#1，su **

刚刚安装完毕的系统无法在命令直接添加 sudo 执行。可以使用 su 或 su -c 'cmd' 来执行需要管理员权限的命令。
`su #切换到root权限下`
或
`su -c 'cmd'`
**#2,启用sudo**

在终端下输入vi /etc/sudoers 或visudo命令：
`# vi /etc/sudoers`
或
`# visudo`
搜索文件，找到下面而2行内容：
`## Allow root to run any commands anywhere
root ALL=(ALL) ALL`
然后，在第二行下面添加如下内容
`username ALL=(ALL) ALL`
提示：将 username 换成你的用户名即可。

**#3，检查、安装系统更新**

检查系统更新：
`sudo yum check-update`
安装系统更新：
`sudo yum update`
**#4,设置中文输入法**

在应用里面查找“Input Method selector”(输入法切换器)，打开后选择“Show all”（显示所有输入法），选择“Chinese”，添加“Pinyin输入法”即可。

中文输入法还有Fcitx小企鹅输入法、ibus-sunpinyin、ibus-googlepinyin、fcitx-googlepinyin、小小输入法等。

Fcitx 小企鹅输入法：
`sudo yum install fcitx`
ibus-sunpinyin:
`sudo yum install ibus-sunpinyin`
ibus-googlepinyin &amp; Fcitx-googlepinyin编译：[点击查看](http://code.google.com/p/libgooglepinyin/wiki/INSTALL)

**#5，多媒体编码 &amp; Flash 插件**

Multi-media-codec:为了正常播放音频、视频文件格式，需安装多媒体编码
`yum localinstall –nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-rawhide.noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-rawhide.noarch.rpm`

yum install ffmpeg ffmpeg-libs gstreamer-ffmpeg xvidcore libdvdread libdvdnav lsdvd

yum install gstreamer-plugins-good gstreamer-plugins-bad gstreamer-plugins-ugly
Flash 插件：

点击[Adobe Flash Player官网](http://get.adobe.com/cn/flashplayer/)下载.rpm包，下载完成，点击安装即可。

### 五：GNOME 3.2 配置

**#1，安装Gnome-tweak-tool**
`sudo yum intall gnome-tweak-tool`
**#2,GNOME Shell Extension**

大部分GNOME Shell Extension基本可以在源中获得。

扩展列表：
`gnome-shell-extension-alternate-tab.noarch
gnome-shell-extension-apps-menu.noarch
gnome-shell-extension-auto-move-windows.noarch
gnome-shell-extension-common.noarch
gnome-shell-extension-cpu-temperature.noarch
gnome-shell-extension-dock.noarch
gnome-shell-extension-drive-menu.noarch
gnome-shell-extension-gpaste.noarch
gnome-shell-extension-icon-manager.noarch
gnome-shell-extension-mediaplayers.noarch
gnome-shell-extension-noim.noarch
gnome-shell-extension-noripple.noarch
gnome-shell-extension-pidgin.i686
gnome-shell-extension-places-menu.noarch
gnome-shell-extension-pomodoro.noarch
gnome-shell-extension-codesentation-mode.noarch
gnome-shell-extension-remove-volume-icon.noarch
gnome-shell-extension-righthotcorner.noarch
gnome-shell-extension-systemMonitor.noarch
gnome-shell-extension-theme-selector.noarch
gnome-shell-extension-user-theme.noarch
gnome-shell-extension-windowsNavigator.noarch
gnome-shell-extension-workspace-indicator.noarch
gnome-shell-extension-workspacesmenu.noarch
gnome-shell-extension-xrandr-indicator.noarch`
各个扩展的作用通过名称基本可以辨别，也可以参考此处：[gnome3-ppa安装gnome扩展 ](http://www.bentutu.com/2011/10/ubuntu-11-10-ppa-install-gnome-3-e/)

GNOME Shell 主题：
`gnome-shell-theme-atolm.noarch
gnome-shell-theme-dark-glass.noarch
gnome-shell-theme-gaia.noarch
gnome-shell-theme-orta.noarch
gnome-shell-theme-smooth-inset.noarch`
安装举例：
`sudo yum install gnome-shell-extension-theme-selector.noarch`
扩展安装完毕后，需重启GNOME Shell（按住Alt+F2,输入 r） 。打开gnome-tweak-tool，激活安装的扩展即可。

**#3，推荐阅读**

[GNOME 3.2 发行注记](http://www.bentutu.com/2011/09/gnome-3-2-%e5%8f%91%e8%a1%8c%e6%b3%a8%e8%ae%b0%ef%bc%88%e5%ae%98%e6%96%b9%e6%96%87%e6%a1%a3%ef%bc%89/)

### 六、集成工具与常用软件推荐

**#1,优秀集成工具推荐：**

Ailurus小熊猫；Autoplus；Fedora Utils；Easy Life

好工具，就要广而告之才够意思！

Ailurus小熊猫:著名的系统设置、软件工具
`su -c 'wget http://homerxing.fedorapeople.org/ailurus.repo -O /etc/yum.repos.d/ailurus.repo'

su -c 'yum makecache'

su -c 'yum install ailurus'`
安装完毕，从“系统工具”里面启动即可。不再赘述。

Autoplus

简介：可用Autoplus进行安装/卸载 Adobe Flash, codecs, Google Earth, Skype, Sun Java, VirtualBox, WinFF, Imagination, Cinelerra, Hugin Panorama Creator, Lightscribe, Dropbox等操作。
`su -c 'yum -y –nogpgcheck install http://dnmouse.org/autoplus-1.2-5.noarch.rpm'`
如果有必要，可导入签名（可选项）：
`su -c 'rpm –import http://dnmouse.org/RPM-GPG-KEY-dnmouse'`
目前，vbox 和 dropbox 还不支持Fedora 16。具体查看主页：[dnmouse.org/autoten/](http://www.dnmouse.org/autoten/)。

Fedora Utils

应用安装与系统设置脚本，可安装Adobe Flash, codecs, Sun Java, Adobe Air, Wine, Google Earth, GTalk plugin, MS Truetype Fonts 等其他常用应用。

主页 | 下载：[click here](http://fedorautils.sourceforge.net/)

easyLife

功能与上述几款工具类似。

主页：[easylife project](http://easylifeproject.org/)

**#2,常用应用**

兔兔将常用应用分为邮件、文件分享、IM、图形图像、播放器、音视频编辑、系统工具、IDE、办公应用

邮件

Thunderbird邮件客户端(Fedora 16 默认为Evolution)
`sudo yum install thunderbird`
文件分享

Transmission(BT客户端)
`sudo yum install transmission`
Filezilla(FTP客户端)：
`sudo yum install filezilla`
即时通讯IM

Pidgin（同Empathy）：
`sudo yum install pidgin`
Emesene（MSN客户端）：
`sudo yum install emesene`
Gwibber（微博客户端）：
`sudo yum install gwibber`
Pino（Twitter 客户端）:
`sudo yum install pino`
Hotot(傲兔,Twitter客户端)：
`yum install hotot`
图形图像

shutter(截图、截屏)：
`sudo yum install shutter`
Gimp：
`sudo yum install gimp`
Dia（流程图）:
`sudo yum install dia`
Inkscape（矢量做图）：
`sudo yum install inkscape`
Gthumb（图像浏览器）：
`sudo yum install gthumb`
Pinta（小Gimp）：
`sudo yum install pinta`
播放器

vlc：
`sudo yum install vlc`
Banshee(音乐播放器)：
`sudo yum install banshee`
音视频编辑

Pitivi（视频编辑）：
`sudo yum install pitivi`
Audacity（音频编辑）：
`sudo yum install audacity`
系统类工具

Gnome Do（快速文件搜索）：
`sudo yum install gnome-do`
Compiz（特性设置器）:
`sudo yum install compiz-manager`
`sudo yum install ccsm`
p7zip （解压缩工具）：
`sudo yum install p7zip`
Virtualbox（虚拟机）：
`sudo yum install virtualbox-ose`
Terminator（优秀命令终端）：
`sudo yum install terminator`
办公

Stardict（星际词典）：
`sudo yum install stardict`
Chmsee（CHM文件阅读器）：
`sudo yum install chmsee`
LibreOffice（Office套件）：
`sudo yum install libreoffice`
Foxit（福晰PDF阅读器）：

点击下载：[官网](http://www.foxitsoftware.com/downloads/)

IDE

Anjuta：
`sudo yum install anjuta`
Code::Blocks：
`sudo yum install codeblocks`
Eclipse:
`sudo yum install eclipse-platform`
版本控制
`sudo yum install subversion git bzr`
网络应用

PPStream（PPS 网络电视）：

点击下载：[官网](http://dl.pps.tv/pps_linux_download.html)

OpenFetion（飞信客户端）：

据作者[@levin博客](http://basiccoder.com/new-opensource-project-hybrid-dev-note.html)介绍，openfetion已停止更新，建立新项目[Hybrid](https://github.com/levin108/hybrid)，支持Fetion和XMPP协议,可下载帮助测试。

Google Chrome（网页浏览器）：

点击官网下载：[click here](http://www.google.com/chrome/index.html)

其他软件，可以从Fedora 16 的“添加/删除软件”应用搜索即可。

**还可以以以下方式方便得安装软件:**

#1，Ailurus小熊猫
大家已经很熟悉了，不再赘述。

#2，Autoplus
下载地址@ [here](http://www.dnmouse.org/autoten/)

#3，Fedora Utils
下载地址@ [here](http://fedorautils.sourceforge.net/)

#4，Easy Life
下载地址@ [here](http://easylifeproject.org/)

&nbsp;

转载自 [笨兔兔](http://www.bentutu.com/2011/11/fedora-16-desktop-configure-sheet/)