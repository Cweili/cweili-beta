title: 用微博图片做外链
tags:
  - 微博
id: 641
categories:
  - 杂物
date: 2011-10-29 01:26:02
---

**很多孩纸在寻找能免费提供图片外链的空间, 我突然突发奇想, 目前微博这么火爆, 另外发现微博里上传的图片有能够外链的迹象, 希望做个测试~~搜寻相关资料, 发现有一达人已做了统计测试~故转载之供大家参考~**

**我个人的意见是这些大的门户网站的微博由于第三方应用众多..引用图片是常事..应该不会这么轻易限制外链的..再说这部分的流量在他们看来也不算什么…所以应该还是靠谱的吧…大家目前还是可以放心得使用一些大网站的微博来做图片外链的~~<!--more-->**
> ### 原文如下:
如今，微博在国内火爆异常，其中图片分享功能也是其成功的一个原因。反观国外某著名微博刚刚支持上传图片，真是后知后觉呀！而国内微博图片大多都可以做外链，今天特意来一个个测试一下。

&nbsp;

<span style="color: #008000; font-size: x-small;">**腾讯微博**</span>

![](http://t3.qpic.cn/mblogpic/e97fc643d24715e05974/460)

一次可以上传八张图片，应该也没有图片大小限制（没说明），直接拖拽即可在新窗口打开图片真是外链地址。

<span style="color: #008000; font-size: x-small;">**新浪微博**</span>

![](http://ww1.sinaimg.cn/large/586c28f1gw1dk60whs1ktj.jpg)

图片上传格式和大小都有标明，点击“查看大图”在新窗口中打开图片外链地址。

<span style="color: #008000; font-size: x-small;">**网易微博**</span>

![](http://oimagea3.ydstatic.com/image?w=120&amp;h=120&amp;url=http://126.fm/1JspHQ)

图片上传后网易给自动缩小到120*90格式，太不厚道了吧！图片也没水印。

<span style="color: #008000; font-size: x-small;">**搜狐微博**</span>

![](http://s2.t.itc.cn/mblog/pic/201108_15_10/13133760919153.JPG)

图片上传过程中有上传进度条，并且可以左右旋转，也是在新窗口打开图片外链地址，图片无水印。

国内微博已泛滥，是个网站都想搞个微博，网站要是没有微博你都不好意思和别人说！最后能存活下来也就腾讯微博和新浪微博了，相对来说，更看好腾讯一点。腾讯善于不断学习，并悄悄的超越对手，新浪的自我审核有点过了，不过这也是国内形势所迫，可以理解。

<span style="color: #008000; font-size: x-small;">**follow5**</span>

![](http://pic1.follow5.com/imgs/note/2011/08/15/20/49/19/12693290815204919_l.jpg)

图片大小有限制，我发了2M的没成功，follow5我主要做和其它微博同步用的。

<span style="color: #008000; font-size: x-small;">**人民微博**</span>

![](http://t.people.com.cn/msgimage/201108/b_145614_1313408583.jpg)

人民微博也很给力呀！没有图片格式、大小的限制。

<span style="color: #008000; font-size: x-small;">**凤凰微博**</span>

![](http://img.t.ifeng.com/201108/15/20/128x160_729573b0e210.jpg)

和网易一样，图片也是被自动缩小了，图片过大无法上传，不过可以管理自己上传的图片。

<span style="color: #008000; font-size: x-small;">**139移动微博**</span>

![](http://fs0.139js.com/file/8fcq8.jpg)

不支持firfox上传图片，原图即是外链地址，和12580一样也有水印。

<span style="color: #008000; font-size: x-small;">**和讯微博**</span>

![](http://photo20.hexun.com/p/2011/0815/451575/t150_564F0A67B6B3B25F26421E526B43ACAE.jpg)

很郁闷，图片也别缩小了。

<span style="color: #008000; font-size: x-small;">**天涯微博**</span>

![](http://img3.laibafile.cn/getimgXXX/3/2/photo3/2011/8/15/small/76511483_14793765_small.jpg)

同样的被缩小，还不支持firefox传图片！

<span style="color: #008000; font-size: x-small;">**做啥**</span>

![](http://zuosa.com/photo/mms/00/25/9D/6242455646.jpg)

有水印，多点一下才能找到图片地址。

<span style="color: #008000; font-size: x-small;">**嘀咕**</span>

![](http://pic.digu.com/file/10/01/79/64/201108/c918f68e39c71a10dab660e67db48ed4_640x480.JPEG)

也不能传大照片！

<span style="color: #008000; font-size: x-small;">**鲜果联播**</span>

![](http://xgres.com/lianbo/upload/120/4e491de1c4a07-4gnW)

也是缩小，其外链地址很有意思，不是图片形式结尾。

<span style="color: #008000; font-size: x-small;">**盛大推他**</span>

![](http://img2.tuita.cc/a1/t/2a/b3/1326231545-309390-528.jpg)

推他有点像是轻博客的意思，上传图片很快。

<span style="color: #008000; font-size: x-small;">**点点**</span>

![](http://m1.img.libdd.com/farm2/83/D1B6C5E7AD27160AD9E6F4415B188753_501_375.jpg)

点点作为国内复制的第一个轻博客，当然少不了图片了，现在也可以外链。

<span style="color: #008000; font-size: x-small;">**新浪轻博客**</span>

![](http://ww4.sinaimg.cn/mw600/586c28f1jw1dk6hs6cjovj.jpg)

轻博客主要还是以图片为主，新浪轻博客倒是没有打水印。

&nbsp;

<span style="color: #008000; font-size: x-small;">**结果**</span>

几乎所有的微博和轻博客都支持图片外链，只是在打水印和图片大小方面有所区别而已。轻博客是一个良好的存储图片的地方，既可以管理，并且易于分享，还不会有容量、流量什么限制什么的。

&nbsp;

经过几天的测试，天涯微博有时候不能外链，网易微博和和讯微博有时候显示有问题。可见，微博图片外链真的是不靠谱！

各微博平台对图片是否可以外链没做说明，做个测试或者应急还可以，当图床当然不行了。

&nbsp;

转载自:[用微博图片做外链](http://yuhaitao.com/weibo-picture/)