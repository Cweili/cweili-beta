title: 水光潋滟西子湖畔
tags:
  - 相册
  - 摄影
  - 浙江
  - 杭州
  - 西溪湿地
categories:
  - 小生活
date: 2014-09-21 01:05:47
---
又是一年秋高气爽之时，为了工作之余调整一番心情，我与家人出发浙江游玩水乡乌镇西塘，泛舟西子湖上，一游西溪湿地。

游玩照片集：

[![灯火阑珊水映乌镇](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjcmr534qj20xc0p014u.jpg)](http://cweili.gitcafe.io/wuzhen/#more)

[![柳絮纷飞烟雨西塘](http://ww4.sinaimg.cn/small/6f5b68c7gw1ekjdeowobuj20xc0p0124.jpg)](http://cweili.gitcafe.io/xitang/#more)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/small/6f5b68c7gw1ekjd19iw0lj20xc0p04e5.jpg)](http://cweili.gitcafe.io/xizihu/#more)

[![河塘飞鸟西溪湿地](http://ww2.sinaimg.cn/small/6f5b68c7gw1ekjgof4su1j20xc0p0175.jpg)](http://cweili.gitcafe.io/xixishidi/#more)
<!--more-->

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd0aq8fij20xc0p0qit.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd0aq8fij20xc0p0qit.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd0cosh6j20xc0p0wo3.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd0cosh6j20xc0p0wo3.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd0fhoydj20xc0p0gst.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd0fhoydj20xc0p0gst.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd0hp5l8j20xc0p0drb.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd0hp5l8j20xc0p0drb.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd0ji8efj20xc0p07av.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd0ji8efj20xc0p07av.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd0l8kh8j20xc0p0464.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd0l8kh8j20xc0p0464.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd0ncay7j20xc0p0tg2.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd0ncay7j20xc0p0tg2.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd0p4iu9j20xc0p00zw.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd0p4iu9j20xc0p00zw.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd0qgs6cj20xc0p0dmy.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd0qgs6cj20xc0p0dmy.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd0s2anaj20xc0p0gsb.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd0s2anaj20xc0p0gsb.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd0u719nj20xc0p0thr.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd0u719nj20xc0p0thr.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd0wjx73j20xc0p0n6m.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd0wjx73j20xc0p0n6m.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd0yixxqj20xc0p0qcc.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd0yixxqj20xc0p0qcc.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd11dulhj20xc0p0k6y.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd11dulhj20xc0p0k6y.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd13uwyxj20xc0p0wu4.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd13uwyxj20xc0p0wu4.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd172n9dj20xc0p0h1s.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd172n9dj20xc0p0h1s.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd19iw0lj20xc0p04e5.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd19iw0lj20xc0p04e5.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd1c3hlsj20xc0p0to5.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd1c3hlsj20xc0p0to5.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd1eudmyj20xc0p0qiu.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd1eudmyj20xc0p0qiu.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd1hd004j20xc0p0h11.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd1hd004j20xc0p0h11.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd1k44jtj20xc0p0k6a.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd1k44jtj20xc0p0k6a.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd1nwlz8j20xc0p0avv.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd1nwlz8j20xc0p0avv.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd1q7gaxj20xc0p07jv.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd1q7gaxj20xc0p07jv.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd1sxnxsj20xc0p0wul.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd1sxnxsj20xc0p0wul.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd1vr4hyj20xc0p0qiv.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd1vr4hyj20xc0p0qiv.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd1yva9qj20xc0p0neq.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd1yva9qj20xc0p0neq.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd20tqdcj20xc0p0n7n.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd20tqdcj20xc0p0n7n.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd23dw1rj20xc0p07h1.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd23dw1rj20xc0p07h1.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd25lu5oj20xc0p0wr4.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd25lu5oj20xc0p0wr4.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd28lx47j20xc0p016f.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd28lx47j20xc0p016f.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd2aj671j20xc0p0gy5.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd2aj671j20xc0p0gy5.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd2d5xj6j20xc0p04d9.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd2d5xj6j20xc0p04d9.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd2evvhpj20xc0p0gxl.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd2evvhpj20xc0p0gxl.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd2ht8xjj20xc0p019i.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd2ht8xjj20xc0p019i.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd2kog5uj20xc0p0tqj.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd2kog5uj20xc0p0tqj.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd2ma82oj20xc0p0k1y.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd2ma82oj20xc0p0k1y.jpg)

[![水光潋滟西子湖畔](http://ww2.sinaimg.cn/mw690/6f5b68c7gw1ekjd2ol7msj20xc0p0gx7.jpg)](http://ww2.sinaimg.cn/large/6f5b68c7gw1ekjd2ol7msj20xc0p0gx7.jpg)

[![水光潋滟西子湖畔](http://ww1.sinaimg.cn/mw690/6f5b68c7gw1ekjd2r8zt7j20p00xch0d.jpg)](http://ww1.sinaimg.cn/large/6f5b68c7gw1ekjd2r8zt7j20p00xch0d.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd2sr2gqj20xc0p0wl2.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd2sr2gqj20xc0p0wl2.jpg)

[![水光潋滟西子湖畔](http://ww3.sinaimg.cn/mw690/6f5b68c7gw1ekjd2v85rhj20xc0p0tip.jpg)](http://ww3.sinaimg.cn/large/6f5b68c7gw1ekjd2v85rhj20xc0p0tip.jpg)

[![水光潋滟西子湖畔](http://ww4.sinaimg.cn/mw690/6f5b68c7gw1ekjd2xioiij20xc0p019g.jpg)](http://ww4.sinaimg.cn/large/6f5b68c7gw1ekjd2xioiij20xc0p019g.jpg)
