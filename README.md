# [Cweili Beta](http://cweili.coding.me/)

* [个人博客](http://cweili.coding.me/)。

* [源代码](http://git.oschina.net/Cweili/cweili-beta)

* 由以下产品强力驱动：

	![Hexo](http://hexo.io/logo.svg)

	基于 Node.js 的静态博客程序

	![GitCafe](https://coding.net/static/7a51352fa766f4176d7c4543339e0e98.png)

	基于代码托管服务打造的技术协作与分享平台
